-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 10, 2015 at 11:18 AM
-- Server version: 5.5.37-0ubuntu0.13.10.1
-- PHP Version: 5.5.3-1ubuntu2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--

--

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__blockedip`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__blockedip` (
  `id_block` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) NOT NULL DEFAULT '',
  `username` varchar(30) DEFAULT NULL,
  `time_fail` datetime DEFAULT '0000-00-00 00:00:00',
  `reason` text,
  `time_upto` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `added_by` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_block`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__category`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT '0',
  `id_origin` int(11) DEFAULT '0',
  `level` tinyint(2) DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Table structure for table `flexyrelease__cmscategory`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__cmscategory` (
  `id_cmscategory` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `code` varchar(55) DEFAULT NULL,
  `description` text,
  `status` int(1) NOT NULL DEFAULT '1',
  `ip` varchar(20) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cmscategory`),
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__cmscategorymap`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__cmscategorymap` (
  `id_content` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cmscode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cmscategory` varchar(255) DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `h1tag` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `view_description` text,
  `language` varchar(50) DEFAULT NULL,
  `id_seq` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_content`),
  KEY `cmscategory` (`cmscategory`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__config`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__config` (
  `id_config` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `ckey` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `f_type` varchar(20) NOT NULL DEFAULT '',
  `f_key` text,
  `f_value` text,
  `is_editable` tinyint(1) NOT NULL DEFAULT '1',
  `comment` text,
  `id_seq` int(11) DEFAULT '0',
  PRIMARY KEY (`id_config`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=367 ;

--
-- Dumping data for table `flexyrelease__config`
--

INSERT INTO `flexyrelease__config` (`id_config`, `name`, `ckey`, `value`, `f_type`, `f_key`, `f_value`, `is_editable`, `comment`, `id_seq`) VALUES
(86, 'LANGUAGE', 'Chinese', 'ch', 'text', '', '', 0, '', 0),
(85, 'LANGUAGE', 'French', 'fr', 'text', '', '', 0, '', 0),
(84, 'LANGUAGE', 'English', 'en', 'text', '', '', 0, '', 0),
(235, 'IMAGE', 'carousel_width', '400', 'text', NULL, NULL, 0, '', 0),
(101, 'SITE_ADMIN', 'email', 'aa@afixi.com', 'text', '', '', 1, '', 2),
(100, 'IPINFO', 'blocktimeinterval', '00:15:00', 'text', '', '', 0, 'Interval time to block user.Format is hh:mm:ss', 1),
(75, 'IMAGE', 'preview_thumb', 'image/preview/thumb/', 'text', '', '', 0, '', 0),
(74, 'IMAGE', 'preview_orig', 'image/preview/orig/', 'text', '', '', 0, '', 0),
(72, 'IMAGE', 'image_thumb', 'image/thumb/', 'text', '', '', 0, '', 0),
(71, 'IMAGE', 'image_orig', 'image/orig/', 'text', '', '', 0, '', 0),
(231, 'IPINFO', 'failure_attempt', '3', 'text', '', '', 0, 'Number to show block IP after this much failure.', 0),
(186, 'MYSQL_INFO', 'version', 'mysql5', 'radio', 'mysql4,mysql5,pdosql', 'mysql4,mysql5,pdosql', 0, 'Mysql version info', 0),
(184, 'GENDER', 'F', 'Female', 'text', '', '', 0, '', 0),
(91, 'IMAGE', 'thumb_height', '100', 'text', '', '', 0, '', 5),
(90, 'IMAGE', 'thumb_width', '100', 'text', '', '', 0, '', 4),
(183, 'GENDER', 'M', 'Male', 'text', '', '', 0, '', 0),
(117, 'USER_TYPE', '2', 'Director', 'text', '', '', 0, '', 0),
(116, 'USER_TYPE', '1', 'Admin', 'text', '', '', 0, '', 0),
(96, 'IPINFO', 'applyblock', '0', 'radio', '0,1', 'No,Yes', 0, 'Apply ip blocking for user', 2),
(345, 'LOGIN', 'autologin', '0', 'radio', '0,1', 'disabled,enabled', 0, 'Set this flag to auto log in user without confirmation mail being sent.', 0),
(346, 'LINKEDIN', 'is_linkedin', '0', 'radio', '0,1', 'Disabled,Enabled', 0, '', 0),
(233, 'LOGIN', 'register', '0', 'radio', '0,1', 'Plain,Ajax', 0, 'Choose either register is plain or ajax type.', 0),
(93, 'AUDIT', 'status', '1', 'radio', '1,0', 'Enabled,Disabled', 0, '', 1),
(102, 'SITE_ADMIN', 'admin_email', 'Afixi Technologies Pvt. Ltd.', 'text', '', '', 1, '', 1),
(103, 'CATEGORY', 'level', '3', 'text', '', '', 0, 'Change how many levels you want in the category.', 0),
(232, 'LOGIN', 'type', '0', 'radio', '0,1', 'Plain,Ajax', 0, 'Choose your login type either plain or ajax type.', 0),
(230, 'IPINFO', 'loginlogs', '1', 'radio', '0,1', 'No,Yes', 0, 'Want to store login details of user', 0),
(238, 'PAGINATE_USER', 'rec_per_page', '8', 'text', NULL, NULL, 0, '', 0),
(237, 'PAGINATE_USER', 'show_page', '2', 'text', NULL, NULL, 0, '', 0),
(114, 'PAGINATE_ADMIN', 'rec_per_page', '50', '', '', '', 0, '', 2),
(115, 'PAGINATE_ADMIN', 'show_page', '5', '', '', '', 0, '', 1),
(118, 'USER_TYPE', '3', 'Account Specifier', 'text', '', '', 0, '', 0),
(119, 'USER_TYPE', '4', 'Account', 'text', '', '', 0, '', 0),
(120, 'USER_TYPE', '99', 'Developer', 'text', '', '', 0, '', 0),
(265, 'RATE', 'max_rate', '10', 'text', NULL, NULL, 0, 'maximum rating (rating up to)', 0),
(125, 'MULTI_LANG', 'islang', '1', 'radio', '0,1', 'No,Yes', 0, 'Choose Yes to enable multi language\r\n', 0),
(138, 'IMAGE_HANDLER', 'library', 'gd', 'text', NULL, NULL, 0, 'im: for image magickrngd : to use GD library', 1),
(133, 'PRODUCT', 'image_upload_type', '1', 'radio', '1,2', 'Add more,Uploadify', 0, '1', 1),
(180, 'IMAGE', 'number', '5', 'text', '', '', 0, 'maximum no. image upload number more than  1..', 6),
(264, 'RATE', 'services', 'rating,review', 'checkbox', 'rating,review,like,like_and_dislike', 'rating,review,like,like and dislike', 0, 'which service do u want provide', 0),
(142, 'RECAPTCHA_KEY', 'publickey', '6LeftNgSAAAAAA4nlp-no7HkYwu1eVXXSOstUcQD ', 'text', '', '', 0, 'This is the public key for the Recaptcha code.\r\nThis API key need to be created for each site and it is different for each site.\r\nTo generate a key login to:-  http://recaptcha.net/api/getkey\r\n//6LcBL94SAAAAAK1M4O7j7PotjDhCkxhB4U79iG9C', 0),
(143, 'RECAPTCHA_KEY', 'privatekey', '6LeftNgSAAAAAJUcstBLpoFC5wIN5u5TiudEYDNy-tE ', 'text', NULL, NULL, 0, 'This is the private key for recaptcha.\r\nIt is also an API key it need to be created for each site and it is different for each site.\r\nTo generate a key login to:-  http://recaptcha.net/api/getkey\r\n6LcBL94SAAAAAL3b_C-wEQpOU8cJYUPWM6dUVUrs ', 0),
(229, 'LOGIN', 'name', '1', 'radio', '1,2', 'username,email', 0, 'Check for username or email to login', 0),
(344, 'HOBBIES', 'status', 'true', 'radio', 'false,true', 'Disabled,Enabled', 0, 'This is used to show hobbies options in register or edit profile page.', 0),
(274, 'FOOTER', 'copyright', 'Copyright 2013', 'text', '', '', 1, 'Footer copyright.', 0),
(234, 'IMAGE', 'image_carousel_path', 'product/carousel/', 'text', NULL, NULL, 0, 'dgdgf', 0),
(179, 'CATEGORY', 'category_status', '1', 'dropdown', '1,0', 'Active,Deactive', 0, 'Here we can set category module as active or deactivate  ', 0),
(227, 'PRODUCT', 'cropping_no', '0', 'text', NULL, NULL, 0, 'How many number of cropping of product', 0),
(223, 'CAPTCHA', 'login', '0', 'radio', '0,1', 'No,Yes', 0, 'Captcha used for login form.', 0),
(221, 'CAPTCHA', 'contact', '0', 'radio', '0,1', 'No,Yes', 0, 'Captcha used for contact form.', 0),
(222, 'CAPTCHA', 'register', '0', 'radio', '0,1', 'No,Yes', 0, 'Captcha used for register form.', 0),
(236, 'IMAGE', 'carousel_height', '400', 'text', NULL, NULL, 0, '', 0),
(270, 'ALL_CAPTCHA', 'select', '1', 'dropdown', '0,1,2,3,4', 'nocaptcha,recaptcha,audio_captcha,text_captcha,math_captcha', 0, 'select use captcha or not.', 0),
(269, 'ALL_CAPTCHA', 'register', '1', 'radio', '0,1', 'No,Yes', 0, 'audio capcha used for register', 0),
(268, 'ALL_CAPTCHA', 'contact', '1', 'radio', '0,1', 'No,Yes', 0, 'audio captcha for contact..', 0),
(242, 'IMAGE', 'new_crop_thumb_path', 'image/newCrop/', 'text', NULL, NULL, 0, 'sfvsv', 0),
(243, 'CHECK_LIST', 'ch', 'Changes', 'text', '', '', 0, '', 0),
(244, 'CHECK_LIST', 'cr', 'Creation', 'text', '', '', 0, '', 0),
(245, 'CHECK_LIST', 'pr', 'Permission', 'text', '', '', 0, '', 0),
(246, 'MESSAGE_DISPLAY', 'type', '1', 'radio', '0,1,2,3', 'no style,style 1,style 2,style 3', 0, '', 0),
(271, 'ALL_CAPTCHA', 'select_file', '1', 'dropdown', '0,1,2,3,4', 'no_file,recaptcha/recaptchalib.php,captcha/audio_captcha/audio_captcha.php,captcha/text_captcha/text_captcha.php,captcha/math_captcha/math_captcha.php', 0, 'chose file include for captcha', 0),
(249, 'PASSWORD', 'type', '1', 'radio', '1,md5,sha1', 'Plain,MD5,SHA1', 0, 'Choose your password type.', 0),
(267, 'ALL_CAPTCHA', 'login', '0', 'radio', '0,1', 'No,Yes', 0, 'audio captcha for user login', 0),
(258, 'RATING', 'access', 'v', 'text', NULL, NULL, 0, NULL, 0),
(259, 'RATING', 'type', 's', 'text', NULL, NULL, 0, NULL, 0),
(260, 'RATING', 'services', 'r,c,l', 'text', NULL, NULL, 0, NULL, 0),
(261, 'RATING', 'review', 'f', 'text', NULL, NULL, 0, NULL, 0),
(262, 'RATE', 'access', 'user,viewer', 'checkbox', 'user,viewer', 'user,viewer', 0, 'which one can rate login user or all', 0),
(263, 'RATE', 'comment_type', 'simple', 'radio', 'facebook,simple', 'facebook,simple', 0, 'choice your commenting system', 0),
(281, 'RATE', 'comment_level', '5', 'text', NULL, NULL, 0, 'level of comments user can add .', 0),
(285, 'COLOR_CODE', 'css', '#3E90D6', 'text', NULL, NULL, 0, 'give the color of the theme with the # sign', 0),
(287, 'NEWS_TYPE', '1', 'entertainment', 'text', NULL, NULL, 0, 'News category', 0),
(288, 'NEWS_TYPE', '2', 'cricket', 'text', NULL, NULL, 0, 'News category', 0),
(289, 'NEWS_TYPE', '3', 'finance', 'text', NULL, NULL, 0, 'News category', 0),
(290, 'IS_PUBLISH', 'news', '0', 'radio', '0,1', 'No,Yes', 0, '', 0),
(291, 'IS_PUBLISH', 'events', '1', 'radio', '0,1', 'No,Yes', 0, '', 0),
(292, 'IS_PUBLISH', 'testimonial', '1', 'radio', '0,1', 'No,Yes', 0, '', 0),
(293, 'FACEBOOK', 'fconnect', '0', 'radio', '0,1', 'Disabled,Enabled', 0, 'Facebook connect', 0),
(294, 'PAYPAL', 'business', 'afixi._1216113978_per@gmail.com', 'text', NULL, NULL, 0, 'Set your paypal business account.', 0),
(295, 'PAYPAL', 'paypal_mode', 'sandbox', 'radio', 'sandbox,live', 'Sandbox,Live', 0, 'You can set your paypal mode.', 0),
(296, 'PAYPALPRO', 'API_USERNAME', 'afixi._1256540322_biz_api1.gmail.com', 'text', NULL, NULL, 0, 'manjar_1358935428_biz_api1.afixi.com \r\nAPI USERNAME: The user that is identified as making the call. you can also use your own API username that you created on PayPal’s sandbox or the PayPal live site.', 0),
(297, 'PAYPALPRO', 'API_PASSWORD', '1256540338', 'text', NULL, NULL, 0, '1358935461\r\nAPI_PASSWORD: The password associated with the API username.If you are using your own API username, enter the API password that was generated by PayPal below.', 0),
(298, 'PAYPALPRO', 'API_SIGNATURE', 'ATPAEmQLmRPGbcn0t.2OBraTOO9TA0XwyMsL05WZBeZBr2hGdA0Pi9Zs', 'text', NULL, NULL, 0, 'AzJxh6as6OF4e1Pdzf02IaWHKRF6A2Fr7J0E7SH4j95Zg.R57Pjk99KS \r\nAPI_SIGNATURE:The Signature associated with the API user. which is generated by paypal.', 0),
(307, 'AUTHORIZENET', 'MODE', 'test', 'radio', 'test,live', 'Test,Live', 0, 'Test or live mode of Authorize.net Payment API', 0),
(308, 'AUTHORIZENET', 'API_LOGIN_ID', '7G7X5wBpv', 'text', NULL, NULL, 0, 'API LOGIN ID for authorization payment api', 0),
(309, 'AUTHORIZENET', 'TRANSACTION_KEY', '5ZE49u69X24FYhbG', 'text', NULL, NULL, 0, 'TRANSACTION KEY required for authorize.net api', 0),
(301, 'PAYPALPRO', 'PROXY_HOST', '127.0.0.1', 'text', NULL, NULL, 0, 'PROXY_HOST: Set the host name or the IP address of proxy server.', 0),
(302, 'PAYPALPRO', 'PROXY_PORT', '808', 'text', NULL, NULL, 0, 'PROXY_PORT: Set proxy port.NOTE : PROXY_HOST and PROXY_PORT will be read only if USE_PROXY is set to TRUE.', 0),
(303, 'PAYPALPRO', 'USE_PROXY', 'FALSE', 'text', '', '', 0, 'USE_PROXY: Set this variable to TRUE to route all the API requests through proxy.', 0),
(305, 'PAYPALPRO', 'MODE', 'sandbox', 'radio', 'sandbox,live', 'Sandbox,Live', 0, 'Mode of payment.For development choose sanbox and for production choose live.', 0),
(304, 'PAYPALPRO', 'VERSION', '65.1', 'text', NULL, NULL, 0, 'VERSION: this is the API version in the request.It is a mandatory parameter for each API request.', 0),
(310, 'MENU', 'level', '2', 'text', NULL, NULL, 0, NULL, 0),
(311, 'GOOGLECHECKOUT', 'merchant_id', '333699160616384', 'text', NULL, NULL, 0, 'Merchant id of google checkout seeler account', 0),
(312, 'GOOGLECHECKOUT', 'merchant_key', 'dRRWk--oE8KkY7xT4_Rmog', 'text', NULL, NULL, 0, 'Merchant key of google checkout seller account.', 0),
(313, 'GOOGLECHECKOUT', 'mode', 'sandbox', 'radio', 'sandbox,live', 'Sandbox,Live', 0, 'Mode of payment', 0),
(341, 'SENDGRID', 'username', 'afixi', 'text', NULL, NULL, 0, NULL, 0),
(314, 'DEFAULT', 'API_KEY', 'a4ffd445afd32ea116c24b607c68fa01-us2', 'text', NULL, NULL, 0, NULL, 0),
(315, 'DEFAULT', 'ID_LIST', '705cfe6c6e', 'text', NULL, NULL, 0, NULL, 0),
(318, 'MAIL', 'MAIL_SEND', '1', 'radio', '0,1', 'No,Yes', 0, '', 0),
(319, 'USPS_SHIPPING', 'SHIPPING_METHOD', 'Priority', 'radio', 'Express,Express SH,Express Commercial,Express SH Commercial,First Class,Priority,Priority Commercial,Parcel,Library,BPM,Media,ALL', 'Express,Express SH,Express Commercial,Express SH Commercial,First Class,Priority,Priority Commercial,Parcel,Library,BPM,Media,ALL', 0, 'set  shipping method for USPS shipping', 0),
(320, 'UPS_SHIPPING', 'SERVICE_CODE', '03', 'radio', '01,02,03,12,13,14,59', 'Next Day Air,2nd Day Air,Ground,3 Day Select,Next Day Air Saver,Next Day Air Early AM,2nd Day Air AM', 0, NULL, 0),
(321, 'UPS_SHIPPING', 'ORIGIN_POSTAL_CODE', '48010', 'text', NULL, NULL, 0, 'Origin Postal Code for UPS shipping', 0),
(322, 'SHIPPING', 'method', 'UPS', 'radio', 'UPS,USPS', 'UPS,USPS', 0, '', 0),
(323, 'SHIPPING', 'shipping_mode', 'testing', 'radio', 'testing,live', 'testing,live', 0, 'You can set your shipping mode.', 0),
(324, 'USPS_SHIPPING', 'ORIGIN_POSTAL_CODE', '10022', 'text', NULL, NULL, 0, 'Origin Postal Code for USPS shipping', 0),
(325, 'USPS_SHIPPING', 'USERNAME', '975SURYA7479', 'text', '', '', 0, '', 0),
(326, 'USPS_SHIPPING', 'PASSWORD', '865KP57VW894', 'text', NULL, NULL, 0, '', 0),
(327, 'UPS_SHIPPING', 'USERNAME', 'ezekielh', 'text', NULL, NULL, 0, NULL, 0),
(328, 'UPS_SHIPPING', 'PASSWORD', 'vibing12', 'text', NULL, NULL, 0, NULL, 0),
(336, 'PRODUCT', 'image_type', '2', 'radio', '1,2', 'Single,Mulitple', 0, 'single /multiple image', 0),
(330, 'USPS_SHIPPING', 'container_type', 'Flat Rate Box', 'radio', 'Flat Rate Box', 'Flat Rate Box', 0, 'Set container type', 0),
(331, 'USPS_SHIPPING', 'size', 'Regular', 'radio', 'Regular,Large', 'Regular,Large', 0, 'Set size here .', 0),
(332, 'USPS_SHIPPING', 'height', '15', 'text', NULL, NULL, 0, 'Set height', 0),
(333, 'USPS_SHIPPING', 'width', '15', 'text', NULL, NULL, 0, 'Set width', 0),
(334, 'USPS_SHIPPING', 'length', '30', 'text', NULL, NULL, 0, 'Set length', 0),
(335, 'USPS_SHIPPING', 'girth', '55', 'text', NULL, NULL, 0, 'Set girth', 0),
(337, 'UPS_SHIPPING', 'height', '15', 'text', NULL, NULL, 0, 'set height', 0),
(338, 'UPS_SHIPPING', 'width', '15', 'text', NULL, NULL, 0, 'set width', 0),
(339, 'UPS_SHIPPING', 'length', '15', 'text', NULL, NULL, 0, 'set length', 0),
(340, 'FACEBOOK', 'app_id', '416033341885933', 'text', '', '', 1, 'Prev Facebook application id - 1398620427097451', 0),
(342, 'SENDGRID', 'password', 'p455w0rd', 'text', NULL, NULL, 0, NULL, 0),
(343, 'ADDR_GMAP', 'gmap_status', 'true', 'radio', 'false,true', 'Disabled,Enabled', 0, 'This is used to show google map in register or edit profile page. A user can fetch his/her address from map (drag and drop ) using marker. .', 0),
(347, 'LINKEDIN', 'api_key', 'tk0kuzm1nde6', 'text', NULL, NULL, 0, '', 0),
(348, 'LINKEDIN', 'secret_key', 'Sb5LjwkQSrxRlI9o', 'text', NULL, NULL, 0, '', 0),
(349, 'TWITTER', 'is_twitter', '0', 'radio', '0,1', 'Disabled,Enabled', 0, '', 0),
(350, 'TWITTER', 'consumerKey', 'EfQ90vcTsQmO1Ji8yRyxDNYEE', 'text', '', '', 1, '', 0),
(351, 'TWITTER', 'consumerSecret', 'Le6YIqmKjjSj97kCzg8VtEmFWwU0QnuBEuG3oMJRSymxpRdwi5', 'text', '', '', 1, '', 0),
(352, 'STRIPE', 'secret_key', 'sk_test_Geec7nlx5DOgbPYGdROroGKh', 'text', NULL, NULL, 0, 'This is the secret key of this application', 0),
(353, 'STRIPE', 'publishable_key', 'pk_test_RqbyhfhBofSEVdoEyHvNN464', 'text', NULL, NULL, 0, 'This is the Publishable Key', 0),
(354, 'ALL_PAYMENT_APIS', 'services', 'paypal,paypalpro,googlecheckout,authorizenet,stripe', 'checkbox', 'paypal,paypalpro,googlecheckout,authorizenet,stripe', 'paypal,paypalpro,googlecheckout,authorizenet,stripe', 0, 'Choose what you want to use', 0),
(355, 'PING', 'google', 'http://www.google.com/webmasters/tools/ping?sitemap=', 'text', NULL, NULL, 0, NULL, 0),
(356, 'PING', 'bing', 'http://www.bing.com/webmaster/ping.aspx?siteMap=', 'text', NULL, NULL, 0, NULL, 0),
(357, 'PING', 'ask', 'http://submissions.ask.com/ping?sitemap=', 'text', NULL, NULL, 0, NULL, 0),
(358, 'PING', 'yahoo', 'http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid=YahooDemo&url=', 'text', NULL, NULL, 0, NULL, 0),
(359, 'FACEBOOK', 'app_secret', 'b1cada0fae37b59f66e925fbacd82010', 'text', '', '', 1, '6165c6a64d09b5d1477f978156d41f41', 0),
(361, 'GOOGLE', 'is_google', '1', 'radio', '0,1', 'Disable,Enable', 0, '', 0),
(362, 'GOOGLE', 'client_id', '489994811212-2o8r35d4nu7pea818epv5lb144opntq9.apps.googleusercontent.com', 'text', NULL, NULL, 0, '', 0),
(363, 'GOOGLE', 'client_secret', 'scGJNbBxddsawcm_rBaYkueV', 'text', NULL, NULL, 0, '', 0),
(364, 'GOOGLE', 'developer_key', '0', 'text', NULL, NULL, 0, '', 0),
(365, 'REGISTRATION_MODE', 'registration', '2', 'radio', '1,2,3,4', 'Register by Admin,Registration redirect,Allow auto registration,Allow with additional field', 0, 'Set different mode registration if it is from social sites', 0),
(366, 'SOCIAL_REGD_TYPE', 'regd_type', '3', 'radio', '1,2,3,4', 'Register Basic,Register Extended,No Registration(only admin can add),Direct Login', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__content`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__content` (
  `id_content` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cmscode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `h1tag` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `language` varchar(50) DEFAULT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_content`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__content_archieve`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__content_archieve` (
  `id_content_archieve` int(11) NOT NULL AUTO_INCREMENT,
  `id_content` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cmscode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `h1tag` varchar(255) DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `language` varchar(50) DEFAULT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update_time` datetime DEFAULT '0000-00-00 00:00:00',
  `archieve_time` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_content_archieve`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__hobbies`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__hobbies` (
  `id_hobbies` int(11) NOT NULL AUTO_INCREMENT,
  `hobbies_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bflag` int(11) NOT NULL,
  PRIMARY KEY (`id_hobbies`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `flexyrelease__hobbies`
--

INSERT INTO `flexyrelease__hobbies` (`id_hobbies`, `hobbies_name`, `Description`, `bflag`) VALUES
(1, 'music', 'Listening', 1),
(2, 'dance', 'modern', 2);

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__image`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__image` (
  `id_img` int(11) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) DEFAULT NULL,
  `cr_date` datetime DEFAULT NULL,
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_img`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__jsmsg`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__jsmsg` (
  `id_jsmsg` int(12) NOT NULL AUTO_INCREMENT,
  `level` varchar(35) DEFAULT NULL,
  `message` text,
  `description` text,
  `language_code` varchar(30) DEFAULT NULL,
  `is_editable` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id_jsmsg`),
  UNIQUE KEY `level` (`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `flexyrelease__jsmsg`
--

INSERT INTO `flexyrelease__jsmsg` (`id_jsmsg`, `level`, `message`, `description`, `language_code`, `is_editable`) VALUES
(1, 'required', 'This field is required.', '', NULL, 0),
(2, 'remote', '', '', NULL, 0),
(3, 'email', '', '', NULL, 0),
(4, 'url', '', '', NULL, 0),
(27, 'date', '', 'for date validation.', NULL, 0),
(6, 'dateISO', '', '', NULL, 0),
(7, 'number', '', '', NULL, 0),
(8, 'digits', '', '', NULL, 0),
(9, 'creditcard', '', 'for credit card validation', NULL, 0),
(10, 'equalTo', '', '', NULL, 0),
(57, 'accept', 'ppp', '', NULL, 0),
(75, 'catUpdSucc', 'Catgory updated successfully', '', NULL, 0),
(12, 'maxlength', '', '', NULL, 0),
(13, 'minlength', '', '', NULL, 0),
(14, 'rangelength', '', '', NULL, 0),
(73, 'cvxcv', 'xcvcv', 'xcvxc', NULL, 0),
(15, 'range', '', '', NULL, 0),
(16, 'maxValue', '', '', NULL, 0),
(17, 'minValue', '', '', NULL, 0),
(74, 'subCatUpdSucc', 'Sub category updated successfully', '', NULL, 0),
(18, 'nodefind', '', '', NULL, 0),
(19, 'novalue', '', '', NULL, 0),
(20, 'customarray', '', '', NULL, 0),
(21, 'customfcheck', '', '', NULL, 0),
(72, 'gfg', '', '', NULL, 0),
(76, 'subCatInsSucc', 'Sub category inserted successfully', '', NULL, 0),
(77, 'catInsSucc', 'Category inserted successfully', '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__login`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__login` (
  `id_login` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `date_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL DEFAULT '',
  `msg` text,
  `status` int(1) NOT NULL DEFAULT '0',
  `failure_attempt` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__message`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__message` (
  `id_message` int(10) NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(50) NOT NULL DEFAULT '',
  `key_name` varchar(50) NOT NULL DEFAULT '',
  `key_value` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_message`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=199 ;

--
-- Dumping data for table `flexyrelease__message`
--

INSERT INTO `flexyrelease__message` (`id_message`, `lang_code`, `key_name`, `key_value`) VALUES
(114, 'en', 'SETTING_CONFIRM_DEL', 'Are you sure to delete this this record.'),
(88, 'en', 'USER_IS_VALID_ZIP', 'Is not a valid zip file. Try another.'),
(51, 'en', 'SETTING_DUP_KEY', 'This message key is already exist.'),
(35, 'en', 'SETTING_DELETE_SUC', 'Message key with all its entries deleted successfully.'),
(54, 'en', 'USER_CONFIRM_DEL', 'Are u sure to delete ?'),
(55, 'en', 'SETTING_INSERT', 'Record inserted successfully.'),
(56, 'en', 'SETTING_UPDATE', 'Record updated successfully.'),
(57, 'en', 'USER_INSERT', 'User information inserted successfully.'),
(58, 'en', 'USER_UPDATE', 'User updated successfully.'),
(59, 'en', 'USER_DELETE', 'User information deleted  successfully.'),
(60, 'en', 'CMS_INSERT', 'Content inserted successfully.'),
(61, 'en', 'CMS_UPDATE', 'Content updated successfully.'),
(62, 'en', 'CMS_DELETE', 'Content deleted successfully'),
(63, 'en', 'PRODUCT_INSERT', 'Product inserted successfully'),
(64, 'en', 'PRODUCT_UPDATE', 'Product updated successfully.'),
(65, 'en', 'PRODUCT_DELETE', 'Product deleted successfully.'),
(66, 'en', 'CATEGORY_INSERT', 'Category inserted successfully.'),
(67, 'en', 'CATEGORY_UPDATE', 'Category updated successfully.'),
(68, 'en', 'CATEGORY_DELETE', 'Category deleted successfully.'),
(69, 'en', 'IPBLOCKING_INSERT', 'Ip blocked successfully.'),
(70, 'en', 'IPBLOCKING_UPDATE', 'IP update successfully.'),
(71, 'en', 'IPBLOCKING_DELETE', 'Blocked  ip deleted successfully.'),
(72, 'en', 'CATEGORY_IS_EXIST', 'This category already exist.'),
(73, 'en', 'CMS_LANG_1', 'Content  title with.'),
(74, 'en', 'CMS_LANG_2', 'Language  already exist.'),
(75, 'en', 'CONTACTUS_MAIL_SUCC', 'Mail Sent Successfully.'),
(76, 'en', 'IPBLOCKING_FAIL', 'Check one checkbox.'),
(77, 'en', 'MODULE_CREATE', 'Module is created.'),
(78, 'en', 'MODULE_DELETE', 'Module is deleted.'),
(79, 'en', 'MODULE_EXISTS', 'This module is already exist in this project folder.'),
(80, 'en', 'MODULE_REQZP', 'Please provide only zip file.'),
(81, 'en', 'MODULE_SUCC', 'Module integrated successfully.'),
(82, 'en', 'PRODUCT_INS_FAIL', 'Problem in insert product.'),
(83, 'en', 'PRODUCT_NEW_INS', 'New product inserted successfully.'),
(84, 'en', 'PRODUCT_UPDATE_FAIL', 'Failure in product updation.'),
(85, 'en', 'PRODUCT_DELETE_FAIL', 'Product deletion failure.'),
(89, 'en', 'USER_FILE_FOUND', 'File not found in the server.'),
(90, 'en', 'USER_IS_VALID_TAR', ' Is not a valid tar file. Try another.'),
(91, 'en', 'USER_LIST_TAR', 'List of all tar files.'),
(92, 'en', 'USER_LIST_ZIP', 'Here is list of all zip files.'),
(93, 'en', 'USER_LOGIN_SUCC', 'Successfully logged in'),
(94, 'en', 'USER_UNAME_PASS', 'Enter  Username  and  password'),
(95, 'en', 'USER_BLOCK_MSG', 'You are blocked.<br>Please contact admin.'),
(96, 'en', 'USER_VALID_UNAME_PASS', 'Please enter correct username and password'),
(134, 'en', 'USER_UPDATE_SUCC', 'User updated successfully.'),
(98, 'en', 'USER_CHECK_USERNAME', 'This username already exist.'),
(125, 'en', 'USER_LOGOUT_SUCC', 'You have logged out successfully.'),
(126, 'en', 'USER_NO_ACC', 'No such account exists. Contact admin.'),
(124, 'en', 'USER_REJECT_CONF', 'The user account has been Rejected.Confirmation mail is sent to the user.'),
(123, 'en', 'USER_UNBLOCK_USER_SUCC', 'User unblocked successfully.'),
(122, 'en', 'USER_REG_SUCC', 'Registration is successful.'),
(121, 'en', 'USER_REG_FAIL', 'Registration failed.'),
(120, 'en', 'USER_CHECK_BOX', 'Check one checkbox.'),
(119, 'en', 'USER_PROF_UPDATE_SUCC', 'Profile updated successfully.'),
(118, 'en', 'USER_CORRECT_PASS', 'Give correct old password.'),
(117, 'en', 'USER_NO_SUCH_USER', 'No such user exist.'),
(116, 'en', 'USER_DELETE_SUCC', 'Successfully deleted.'),
(115, 'en', 'USER_LOGOUT_FAIL', 'Failure in logout.'),
(127, 'en', 'USER_CONF_MAIL', 'Please confirm your email.'),
(131, 'en', 'MESSAGE_DUP_KEY', 'This key is already exist.'),
(133, 'en', 'USER_PASS_CONF_MSG', 'Your password has been successfully sent to your e-mail.'),
(157, 'en', 'STTTING_JSMSG_INS_FAIL', 'Sorry, javascript message insertion failed'),
(158, 'en', 'SETTING_JSMSG_UPD_SUCC', 'Javascript message updated successfully'),
(156, 'en', 'SETTING_JSMSG_INS_SUCC', 'New javascript message inserted successfully.'),
(159, 'en', 'SETTING_JSMSG_UPD_FAIL', 'Sorry, updation of data failed'),
(161, 'en', 'CONTACTUS_INS_SUCC', 'Inserted successfully.'),
(176, 'en', 'USER_PASS_CHANGE_SUCC', 'Your password changed successfully.'),
(175, 'en', 'USER_PASS_TO_MAIL', 'Your password has been successfully sent to your e-mail.'),
(174, 'en', 'USER_WRONG_CAPTCHA_CODE', 'You have entered wrong code. Please reenter.'),
(177, 'en', 'USER_PASS_CHANGE_FAIL', 'Failed to change the password.'),
(178, 'en', 'USER_PWD_CHANGE_SUCC', 'Your password changed successfully.'),
(179, 'en', 'USER_WRONG_PASS', 'You have entered wrong oldpassword .Please try it again.'),
(180, 'en', 'CATMSG', 'Category inserted successfully'),
(190, 'en', 'USER_ADMIN_INVALID_UNAME_PASS', 'You are not authorised to access this url'),
(183, 'en', 'USER_ERROR_MSG', 'This field is required'),
(184, 'en', 'USER_CHK_SESSION', 'You have not login'),
(185, 'en', 'USER_REG_SUCC_NO_FB', 'Registration successful. Please verify your email to login'),
(186, 'en', 'USER_FACEBOOK_CANCEL', 'You are not connected with Facebook. Please verify your email  to login'),
(187, 'en', 'USER_REG_FACEBOOK_SUCC', 'You are successfully connected with your facebook account.'),
(188, 'en', 'FACEBOOK_ACCOUNT_EXISTS', 'This facebook account already is in use.Please logout your facebook account and enter another account information'),
(189, 'en', 'USER_FB_LOGIN_SUCC', 'Successfully connected with Facebook'),
(191, 'en', 'USER_LINKEDIN_SUCC', 'Successfully connected with Linkedin.'),
(192, 'en', 'LINKEDIN_ACCOUNT_NOT_REG', 'You are not register user of our site.'),
(193, 'en', 'LINKEDIN_ACCOUNT_EXISTS', 'This Linkedin account already exists.'),
(194, 'en', 'FACEBOOK_CANCEL', 'You are not connected with Facebook.'),
(195, 'en', 'USER_LINKEDIN_LOGIN_SUCC', 'Successfully connected with Linkedin'),
(196, 'en', 'EMAILCONTENT_INSERT', 'Email content inserted successfully'),
(197, 'en', 'EMAILCONTENT_UPDATE', 'Mail template updated successfully');

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__product`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `code` varchar(100) DEFAULT NULL,
  `video` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_type` int(2) DEFAULT '0',
  `code_category` varchar(50) DEFAULT NULL,
  `id_user` int(11) DEFAULT '0',
  `cr_date` date DEFAULT '0000-00-00',
  `ip` varchar(30) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'for test purpose...',
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__product_images`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__product_images` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) DEFAULT NULL,
  `code_product` varchar(50) DEFAULT NULL,
  `image_name` varchar(50) DEFAULT NULL,
  `id_seq` int(11) NOT NULL,
  PRIMARY KEY (`id_image`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flexyrelease__user`
--

CREATE TABLE IF NOT EXISTS `flexyrelease__user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT '',
  `last_name` varchar(100) DEFAULT '',
  `username` varchar(100) DEFAULT '',
  `email` varchar(100) DEFAULT '',
  `fb_email` varchar(100) DEFAULT NULL,
  `google_email` varchar(100) DEFAULT NULL,
  `password` varchar(50) NOT NULL DEFAULT '',
  `id_admin` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `phno` int(11) DEFAULT NULL,
  `altphno` int(11) DEFAULT NULL,
  `hobbies` varchar(100) DEFAULT '',
  `dob` date DEFAULT '0000-00-00',
  `address` text,
  `permission` text,
  `random_num` varchar(50) NOT NULL DEFAULT '',
  `fconnect` varchar(255) DEFAULT NULL,
  `id_linkedin` varchar(30) DEFAULT NULL,
  `id_google` text,
  `twitt_connect` varchar(30) DEFAULT NULL,
  `add_date` date NOT NULL DEFAULT '0000-00-00',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `flexyrelease__user`
--

INSERT INTO `flexyrelease__user` (`id_user`, `first_name`, `last_name`, `username`, `email`, `fb_email`, `google_email`, `password`, `id_admin`, `gender`, `phno`, `altphno`, `hobbies`, `dob`, `address`, `permission`, `random_num`, `fconnect`, `id_linkedin`, `id_google`, `twitt_connect`, `add_date`, `ip`, `flag`) VALUES
(1, 'afixi', 'afixi', 'admin', 'flexycms@mail.afixiindia.com', NULL, NULL, 'demoadmin', 1, 'M', NULL, NULL, '2', '1986-05-13', 'cuttack', NULL, '0', NULL, NULL, NULL, NULL, '2010-02-26', '192.168.1.128', 1),
(2, 'Developer', '', 'developer', 'developer@afixi.com', NULL, NULL, 'developer', 99, '', NULL, NULL, '', '0000-00-00', NULL, NULL, '0', NULL, NULL, NULL, NULL, '0000-00-00', '', 1);
