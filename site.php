<?php
class site {
	var $container_tpl;
	var $is_container_enabled;
	var $default_tpl;
	
	public function __construct($ce){
	    $this->container_tpl = "common/common";
	    $this->is_container_enabled = $ce; 
	}
	
	public function handle_page($page) {
		$this->cache_id = $page;
		$this->smarty->caching = 0;
		switch ($page) {
			case "static" :
				$this->default_tpl = "static/".$_REQUEST['choice'];
				break;
			case "common" :
				$this->default_tpl = "user/home";
				break;
			default :
				$this->default_tpl = $page.'/home';
				break;
		}
	}
	
	public function __destruct() {}
};