<?php
define("APP_ROOT",$_SERVER['DOCUMENT_ROOT']."/".SUB_DIR); // Document root of application
define("TEMPLATE_DIR",APP_ROOT."templates/"); // Document root of templates
define("TEMPLATE_BACKUP_DIR",APP_ROOT."templates_backup/"); // Document root of templates
define("APP_ADMIN_ROOT",APP_ROOT.'flexyadmin/'); // Document root of admin application
define("ADMIN_TEMPLATE_DIR",'templates_admin'); // Directory inside templates dir that contains all the admin templates
define("SITE_DOMAIN_NAME", $_SERVER['HTTP_HOST']."/".SUB_DIR);
define("APP_ROOT_URL",'http://'.SITE_DOMAIN_NAME);
define("APP_ROOT_URLS",'https://'.SITE_DOMAIN_NAME);
define("APP_ROOT_PATH",APP_ROOT_URL.'flexymvc/');
define("APP_ADMIN_ROOT_URL",APP_ROOT_URL.'flexyadmin/'); // URL for admin application
define("LBL_SITE_URL",APP_ROOT_URL);
define("LBL_ADMIN_SITE_URL",APP_ROOT_URL.'flexyadmin/');
define("JS_PATH",'http://'.$_SERVER['HTTP_HOST']."/");

//New to Flexymvc
define("LIBSEXT_PATH",'http://'.$_SERVER['HTTP_HOST']."/flexymvc_core/libsext/");

define("JS_PATH_EXT",LIBSEXT_PATH."jquery/");
//define("FLEXY_JS_PATH",APP_ROOT_URL."templates/flexyjs/");
define("JS_UI_PATH_EXT",LIBSEXT_PATH."jqueryui/");//for notification message
//define("CSS_PATH",APP_ROOT_URL."templates/css_theme/");
//define("TEMP_COM_IMG",APP_ROOT_URL."templates/default/images/");
define("TEMP_COM_IMG",APP_ROOT_URL."templates/".$_SESSION['multi_language']."/images/");
define("FLEXY_JS_PATH",APP_ROOT_URL."templates/".$_SESSION['multi_language']."/flexyjs/");
define("CSS_PATH",APP_ROOT_URL."templates/".$_SESSION['multi_language']."/css_theme/");

define("PAGINATE_ADMIN_PATH","flexyadmin/");

define("IMAGE_ORIG_PATH",APP_ROOT_URL."image/orig/");
define("IMAGE_THUMB_PATH",APP_ROOT_URL."image/thumb/");

define("PREVIEW_ORIG_PATH",APP_ROOT_URL."image/preview/orig/");
define("PREVIEW_THUMB_PATH",APP_ROOT_URL."image/preview/thumb/");

define("IP",$_SERVER['REMOTE_ADDR']);



define("LBL_SITE_RESOURCES_URL",APP_ROOT_URL.'waf_res/');
define("TEMP_ROOT",APP_ROOT.'waf_res/temp_content/'); // Remote file uploading
define("MAX",99999);
define('SESSION_TIMEOUT', 60);    //sets minutes
if(defined("SITE_USED")){
	include_once(AFIXI_ROOT.'configs/'.SITE_USED.'/constants.php');
}