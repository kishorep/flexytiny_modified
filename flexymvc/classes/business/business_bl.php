<?php

class business_bl extends business {

    function group_mail() {
	$sql = "SELECT MAX(mail_group)+1 AS group_mail FROM " . TABLE_PREFIX . "mail";
	$rec = getrows($sql, $err);
	if ($rec[0]['group_mail'] == '') {
	    return 1;
	} else {
	    return $rec[0]['group_mail'];
	}
    }

    function tot_reciver($mail_group) {
	$sql = "SELECT count(u.id_user) as cnt,GROUP_CONCAT( u.id_user SEPARATOR ', ' ) AS alliduser,GROUP_CONCAT( u.username SEPARATOR ', ' ) AS alluser FROM " . TABLE_PREFIX . "mail m, " . TABLE_PREFIX . "user u WHERE u.id_user = m.reciver
		AND mail_group =" . $mail_group . " GROUP BY mail_group";
	$record = getsingleindexrow($sql, $err);
	return $record;
    }

    function read($id) {
	global $link;
	$sql = "UPDATE " . TABLE_PREFIX . "mail set mail_read=1 WHERE id_mail=" . $id;
	mysqli_query($link, $sql);
    }

    function get_userid($uname) {
	global $link;
	$sql = "SELECT id_user FROM " . TABLE_PREFIX . "user WHERE username='" . $uname . "'";
	$res = mysqli_fetch_assoc(mysqli_query($link, $sql));
	return $res['id_user'];
    }

    function get_idparent($rid, $sid, $mail_group) {
	global $link;
	$sql = "SELECT id_parent FROM " . TABLE_PREFIX . "mail WHERE sender=" . $sid . " AND reciver=" . $rid . " AND mail_group=" . $mail_group . " LIMIT 1 ";
	$res = mysqli_fetch_assoc(mysqli_query($link, $sql));
	return $res['id_parent'];
    }

    function get_cash() {
	$sql = "SELECT cash,id_user FROM " . TABLE_PREFIX . "user WHERE id_user=" . $_SESSION['id_user'] . " LIMIT 1";
	return $sql;
    }

    function get_searchmail($search_text) {
	$sql = "SELECT * FROM " . TABLE_PREFIX . "mail WHERE (sender LIKE '%" . $search_text . "%' OR reciver  LIKE '%" . $search_text . "%' OR subject LIKE '%" . $search_text . "%') AND (sender ='" . $_SESSION['email'] . "' OR reciver ='" . $_SESSION['email'] . "')";
	return $sql;
    }

};