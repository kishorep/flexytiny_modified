<?php
class obj_data extends db_class {
    var $mtable;
    function obj_data($tbl) {
	$this->mtable = $tbl;
	parent::db_class($this->mtable);
    }
    
    /**
     *
     * Insert login details of users
     * @author Parwesh Sabri
     * @param Array $arr User array
     * @param int $status From 1 to 6
     */
    function loginLogs($arr, $status){
	$colField = "id_user,username,email,status,failure_attempt,msg,ip,date_login";
	$colValue = "'".$arr['id_user']."','".$arr['username']."','".$arr['email']."','".$status."','".$_SESSION['login_count']."','".$_SESSION['raise_message']['global']."'";
//	$extValue = "'".$_SERVER['SERVER_ADDR']."',NOW()";
       	$extValue = "'".$_SERVER['REMOTE_ADDR']."',NOW()"; // changed on PP discussed with debasis
	$ret = $this->insertStringFormat($colField, $colValue, $extValue, "login");
    }
    
    /**
     *
     * Insert block ip details into blockedip table
     * @author Parwesh Sabri
     * @param String $uname Username
     * @param String $reason Reason of failure
     */
    function blockedIP($uname, $reason){
	$colField = "ip,username,time_fail,reason,time_upto";
	$colValue = "'".IP."','".$uname."',NOW(),'".$reason."',ADDTIME(NOW(), '".$GLOBALS['conf']['IPINFO']['blocktimeinterval']."')";
	$this->insertStringFormat($colField, $colValue, "", "blockedip");
    }


    /**
 * validate() function is used to help in the server side vallidation.
 * @param array $arr
 * $arr contains the array of records to vallidate.
 * @param string $conf_pwd
 * It is the confirmation password entered by the user.
 * @return type 
 */
    function validate($arr,$conf_pwd){
		$validdata = array();
		$validdata[]= array("first_name", "isEmpty",$arr['first_name'],"Please enter the first name",);
		$validdata[]= array("last_name", "isEmpty",$arr['last_name'],"Please enter the last name",);
		$validdata[]= array("email", "isEmpty",$arr['email'],"Please enter your email",);
		$validdata[]= array("email", "checkValidemail",$arr['email'],"Please enter valid email",);
		if(!$_SESSION['id_user']){
			$validdata[]= array("username", "isEmpty",$arr['username'],"Please enter the username",);
			$validdata[]= array("password", "isEmpty",$arr['password'],"Please enter the password",);
			$validdata[]= array("conf_pwd", "isEmpty",$conf_pwd,"Please enter the confirm password",);
			if($conf_pwd)
				$validdata[]= array("conf_pwd", "isEqual",$arr['password'],"Please enter same password",$conf_pwd);
			$validdata[]= array("gender", "isEmpty",$arr['gender'],"Please select your gender",);
		}
		//$validdata[]= array("hobbies", "isEmpty",$arr['hobbies'],"Please enter the hobbies",);
		if(is_array($arr['address'])){
			foreach($arr['address'] as $key => $value){
				$validdata[]= array("address", "isEmpty", $value,"Please enter the address",);
			}
		}else{
			$validdata[]= array("address", "isEmpty",$arr['address'],"Please enter the address",);
		}
		$validdata[]= array("dob", "isEmpty",$arr['dob'],"Please select the date of birth",);
		return validator::validate($validdata);
	}
        
        function delete_from_archive($id,$mail_type){
        global $link; 
		if($mail_type=='inbox'){
			foreach($id as $key=>$val) {
				$sql="SELECT id_parent FROM ".TABLE_PREFIX."mail WHERE 1";
				$ssql=$sql." AND id_mail=".$val." LIMIT 1";
				$parent=mysqli_fetch_assoc(mysqli_query($link,$ssql));
				$sql="UPDATE ".TABLE_PREFIX."mail set del_receiver=1,arch_receiver=1 WHERE id_parent=".$parent['id_parent']." AND reciver=".$_SESSION['id_user'];
				mysqli_query($link,$sql);
				$sql="SELECT id_parent FROM ".TABLE_PREFIX."mail WHERE id_mail=".$val." AND (del_sender=1 AND del_receiver=1)";
				$par=mysqli_fetch_assoc(mysqli_query($link,$sql));
				if($par) {
					//$sql="DELETE FROM ".TABLE_PREFIX."mail WHERE id_parent=".$par['id_parent']." AND reciver=".$_SESSION['id_user'];
					//mysqli_query($link,$sql);
				}   
			}
			return 1;
		}
		elseif($mail_type=='sent_mail'){
			foreach($id as $key=>$val){
				$sql="SELECT id_parent FROM ".TABLE_PREFIX."mail WHERE 1";
                            	$ssql=$sql." AND id_mail=".$val." LIMIT 1";//print  $ssql;
                                $parent=mysqli_fetch_assoc(mysqli_query($link,$ssql));
				$qry="UPDATE ".TABLE_PREFIX."mail set del_sender=1,arch_sender=1 WHERE id_parent=".$parent['id_parent']." AND sender=".$_SESSION['id_user'];//print $qry;exit;
				mysqli_query($link,$qry);
				$sql=$sql." AND (del_sender=1 AND del_receiver=1) AND id_parent=".$parent['id_parent']." AND sender=".$_SESSION['id_user'];
				$par=mysqli_fetch_assoc(mysqli_query($link,$sql));//print_r($par);exit;
				if($par) {
				   //$sql="DELETE FROM ".TABLE_PREFIX."mail WHERE id_parent=".$par['id_parent']." AND sender=".$_SESSION['id_user'];
				  //  mysqli_query($link,$sql);
			       }	
			}
			return 1;
		}
		else{
			foreach($id as $key=>$val){
				$sql="SELECT sender,reciver FROM ".TABLE_PREFIX."mail WHERE mail_group=".$val." AND (sender='".$_SESSION['email']."' OR reciver='".$_SESSION['email']."')";
				$res=getrows($sql,$err);
				//echo'<pre>';print_r($res);exit;
				if($res[0]['sender']==$_SESSION['email']){
					$sql="UPDATE ".TABLE_PREFIX."mail set del_sender=1,arch_sender=1 WHERE mail_group=".$val;
					//print $sql;exit;
					mysqli_query($link,$sql);
				}
				else{
					$sql="UPDATE ".TABLE_PREFIX."mail set del_receiver=1,arch_receiver=1 WHERE mail_group=".$val." AND reciver='".$_SESSION['email']."'";
					//print $sql;exit;
					mysqli_query($link,$sql);
				}
				$sql="DELETE FROM ".TABLE_PREFIX."mail WHERE mail_group=".$val." AND (del_sender=1 AND del_receiver=1)";
				mysqli_query($link,$sql);
				return 1;
			}
		}
	}
};