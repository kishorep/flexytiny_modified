    <?php

class admin_practice extends practice_manager {

    /**
     * This is a constructor to initialized product admin module.This overrides user side constructor.
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input) {
        if ($_SESSION['id_admin']) {
            parent::__construct($smarty, $_output, $_input);
        } else {
            redirect(LBL_ADMIN_SITE_URL);
        }
    }
    
    function _practicemenu(){
        $this->_output['tpl'] = "admin/practice/main_page";
    }
    function _qrcode(){
        $this->_output['tpl'] = "admin/practice/qrcode";
    }
   function _showqrcode(){
       ob_clean();
                 $name= $this->_input['arr'];//pr($name);exit;
                 $str="NAME : {$name['name']}\nPHONE : {$name['phno']}\nADDRESS : {$name['add']}";//echo $str;exit;
                 require_once APP_ROOT . 'flexymvc/classes/common/phpqrcode/qrlib.php';
                 $res=APP_ROOT."image/qrcode/".$name['name'].".png";//pr($res);exit;
                 QRcode::png($str,$res);
                 echo APP_ROOT_URL."image/qrcode/".$name['name'].".png";exit;
//                 $mul = $this->_input['mul'];//pr($this->_input);exit;
//                 //$data = implode("name:[],add:[],ph:[]", $mul);pr($data);exit;
//                 $data .= " VALUES ('".implode("', '", $mul)."') ";pr($data);exit;
//                 $name=implode(",", $name);//pr($name);exit;
//                 $name= $this->_input['add'];//pr($data1);
//                 $name= $this->_input['phno'];//pr($name);exit;
                 
                 //$this->_form();
   }
   function _jpgraph(){
        $this->_output['tpl'] = "admin/practice/jpgraph";
    }
function _jpgraph_line(){
ob_clean();
require_once (APP_ROOT.'jpgraph/src/jpgraph.php');
require_once (APP_ROOT.'jpgraph/src/jpgraph_line.php');

// Some data
$ydata = array(111,3,82,12,52,1,92,13,52,7);

// Create the graph. These two calls are always required
$graph = new Graph(350,250);
$graph->SetScale('textlin');

// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot->SetColor('red');

// Add the plot to the graph
$graph->Add($lineplot);

header("Content-type: image/png");
// Display the graph
$graph->Stroke();
exit;
}

function _jpgraph_bar(){
require_once (APP_ROOT.'jpgraph/src/jpgraph.php');
require_once (APP_ROOT.'jpgraph/src/jpgraph_bar.php');

$datay=array(12,8,19,3,10,5);

// Create the graph. These two calls are always required
$graph = new Graph(300,200);
$graph->SetScale('textlin');

// Add a drop shadow
$graph->SetShadow();

// Adjust the margin a bit to make more room for titles
$graph->SetMargin(40,30,20,40);

// Create a bar pot
$bplot = new BarPlot($datay);

// Adjust fill color
$bplot->SetFillColor('orange');
$graph->Add($bplot);

// Setup the titles
$graph->title->Set('A basic bar graph');
$graph->xaxis->title->Set('X-title');
$graph->yaxis->title->Set('Y-title');

$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);

// Display the graph
$graph->Stroke();
}

function _jpgraph_pie(){
require_once (APP_ROOT.'jpgraph/src/jpgraph.php');
require_once (APP_ROOT.'jpgraph/src/jpgraph_pie.php');

$data = array(40,60,21,33);

$graph = new PieGraph(300,200);
$graph->SetShadow();

$graph->title->Set("A simple Pie plot");

$p1 = new PiePlot($data);
$graph->Add($p1);
header("Content-type: image/png");

$graph->Stroke();
}
function _autocomplete(){
    $this->_output['tpl'] = "admin/practice/autocomplete";
}
function _namesearch(){
    ob_clean();
            $searching_about =$_GET['term'];
//            $searching_about ='a';

    $cond = "firstname LIKE '%" . $searching_about . "%'";
    $sql = "SELECT id,firstname FROM " . TABLE_PREFIX . "practice_student WHERE {$cond} ";
    $res = getindexrows($sql,"id","firstname");
    
     foreach($res as $key=>$val){
        $name[] = array('id' => $key,'value' => $val);
    }
//    echo "<pre>";print_r($name);exit;
    
    print(json_encode($name));
exit;
    
}
function _tcpdfdemo(){
    ob_clean();
require_once(APP_ROOT.'TCPDF-master/tcpdf_import.php');
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->SetFont('helvetica', '', 9);
$pdf->AddPage();
$name="hello";
$html=<<<EOD
<html>
<head></head>
<body><table border="1">
<tr><th>name</th>
<th>company</th></tr>
<tr>
<td>$name</td>
<td>xx technologies</td>
</tr>
</table>
</body>
</html>
EOD;


$pdf->writeHTML($html, true, 0, true, 0);
$pdf->lastPage();
$pdf->Output('htmlout.pdf', 'I');          
//$this->_output['tpl'] = "admin/practice/tcpdfdemo";
    
}
function _xmldemo(){
   $fileurl=APP_ROOT."xml/note.xml";
        $xml = simplexml_load_file($fileurl);
        $array = json_decode(json_encode((array)$xml), TRUE);
        $array=$array['note'];
        if($this->_input['notes_no']){
            ob_clean();
            $noteid = ($this->_input['notes_no']-1);
            echo json_encode($array[$noteid]); exit;
        }
        $this->_output['list']=$array;
        $this->_output['tpl']='admin/practice/xmllisting';
 
}
function _insertxml(){
        $title=$this->_input['title'];
        $description=$this->_input['description'];
        $fileurl=APP_ROOT."xml/note.xml"; 
        $xmldoc = simplexml_load_file($fileurl);
        
        if($this->_input['id']){
            $id = $this->_input['id']-1;
            $xmldoc->note[$id]->title=$title;
            $xmldoc->note[$id]->description=$description;
        }else{
       
        $array = json_decode(json_encode((array)$xmldoc), TRUE);
        $array=$array['note'];
        $id=count($array)+1;
        
        $entry = $xmldoc->addChild('note'); 
        $entry->addChild("id",$id); 
        $entry->addChild("title",$title);
        $entry->addChild("description",$description);
        }
        $xmldoc->asXML($fileurl);
        $this->_xmldemo();
}
function _deletexml(){
        $fileurl=APP_ROOT."xml/note.xml"; 
        $xmldoc = simplexml_load_file($fileurl);
        
        $index = $this->_input['sno']-1;
        unset($xmldoc->note[$index ]);
        $array = json_decode(json_encode((array)$xmldoc), TRUE);
        $array=$array['note'];
        $id=count($array);
        while($index<$id){
            $xmldoc->note[$index]->id=$index+1;
            $index++;
        }
        
        $xmldoc->asXML($fileurl);

}
function _imagemapping(){
    $this->_output['tpl']='admin/practice/imagemapping';
}
function _phpmailerdemo(){
    $to="manoranjan.rout@afixi.com";
    $from="kishore.patra@afixi.com";
    $subject="phpmailer demo";
    $body="this mail is <br><b>sent</b><br> through <table border=1><tr><td>sdgsgsr</td></tr></table><u>phpmailer</u>";
    $msg=phpMailerlocal($to,'','',$from, $subject, $body,''); 
    if($msg){
        echo "email sent to manoranjan.rout@afixi.com";exit;
    }else{
        echo "email sending failed!";exit;
    }
}
function _dragndropmainpage(){
            $emp=  getindexrows(get_search_sql("empdrop"),"id","name");
            $this->_output['emp'] =$emp;
            $var1=  getindexrows(get_search_sql("empbatch"),"id");
            foreach ($var1 as $key => $item) {
                $emps.= $item['emp'].",";
                $item['emp'] = explode(',', $item['emp']);
                $val1[]= $item;
            }
            //pr($emps);exit;
            $tm = trim($emps,",");
            $val = explode(",", $tm);
            $result = array_unique($val);
             $this->_output['sql1'] =$val1;
             $this->_output['val1'] =$result;//pr($this->_output);exit;
            $this->_output['tpl'] = 'admin/practice/mainpage';
        }
function _updateBatch(){
    $data = $this->_input['data'];
    foreach ($data as $key => $val){
        if($val != undefined){
            $updt = "UPDATE ".TABLE_PREFIX."empbatch SET emp = '{$val}' WHERE id = '{$key}'"; 
            execute($updt);
        }   
    }
     $this->_output['tpl'] = 'admin/practice/mainpage';
}
function _showMap(){
    $this->_output['tpl'] = 'admin/practice/showmap';
}
function _showCalender(){
    $this->_output['tpl'] = 'admin/practice/showcalender';
}
/*
     * txtToImg : Creating an image using image magic.
     * @param $schooltype : Type of schools.
     * @return type.
     */
function _columnnameToImgfromtable(){
    global $link;
    $sql = "show tables";
	$res = mysqli_query($link, $sql);
        while ($rec = mysqli_fetch_assoc($res)) {
            $key=substr($rec['Tables_in_FLEXYMVCTEST_NEW'], 14);
            $list[$key]=$rec['Tables_in_FLEXYMVCTEST_NEW'];
        }
    $this->_output['list']=$list;
    $this->_output['tpl']="admin/practice/tablelist";
    
}

    public function _txtToImg() {
        $table = $this->_input['tablename'];
	global $link;
	//ob_clean();
	$pathname['system'] = APP_ROOT . "image/txtToImg";
	$fieldsNotRqrd = array("id_{$table}", "add_date", "ip");
	$sql = "DESCRIBE " . TABLE_PREFIX . "$table";
	$res = mysqli_query($link, $sql);
	while ($rec = mysqli_fetch_assoc($res)) {
	    if (!in_array($rec['Field'], $fieldsNotRqrd)) {
		$name = strtolower($table) . "_" . $rec['Field'];
		

if (!file_exists($pathname['system'] . "/" . $name . ".png")) {//Whether png image file exists or not.
		    $text = ucfirst(str_replace("_", " ", $rec['Field']));
		    $imgname = str_replace(")", "", str_replace("(", "", $name));
		    $imgname = preg_replace("/[^\w\d]+/", "", $imgname);
		    $imgname = strtolower($imgname);
		    $txtInfoArr = array("name" => "{$imgname}.png", "background_color" => 'none', "text" => "{$text}",
			"text_color" => "black", "font" => 'Arial', "pointsize" => '12x', "effect" => 'normal', "distortion" => '0',
			"arc_angle" => '0', "rotation" => '-90');
		    $req = $this->createArray($txtInfoArr);
		    $command = $this->makeCommand($req, $txtInfoArr['name'], $pathname['system']);
		   // print $command."<br>";//exit;
		    //continue;
		    if (false !== $command) {
			$bool = system($command, $answer);
			if (0 != $answer) {
			    echo "<status>error</status><description>Command could not run</description><br>";
			} else {
			    echo "<img src='" .LBL_SITE_URL. "image/txtToImg/" . $txtInfoArr['name'] . "' />";
			}
		    } else {
			echo "<description>Command not found</description>";
		    }
		}
	    }
	}
    }

    /*
     * createArray : Making an array for generating an image.
     */

    function createArray($request) {
	unset($request['name']);
	if ($request) {
	    foreach ($request as $key => $value) {
		if ($key == "text_color") {
		    $req['c'] = $value;
		} else {
		    $req[substr($key, 0, 1)] = $value;
		}
	    }
	}
	return $req;
    }

    /*
     * makeCommand : Making an unix/Linux command for excution and creating an image.
     */

    function makeCommand($req = null, $file = null, $pathname) {
	if (null != $req && null != $file) {
	    $command = 'bash ' . APP_ROOT . 'texteffect.sh ';
	    foreach ($req as $key => $val) {
		if (isset($val)) {
		    if ('t' == $key) {
			$command.=' -' . $key . ' "' . $val . '"';
		    } else {
			$command.=' -' . $key . ' ' . $val;
		    }
		}
	    }
	    $command.=' ' . $pathname . '/' . $file;
	    return $command;
	} else {
	    return false;
	}
    }


  function _donationReportExcel() {
     
    ob_clean();
    global $link;
//    $excel_sql="SELECT tu.*,CONCAT(first_name,' ',IFNULL(last_name, '')) as receiver_name FROM (SELECT tr.*,u.id_user as user_id,u.level,u.state as st,u.district,u.assembly,u.phone_no,IF(tr.transfer_in,transfer_in,sender_no) as phone_field FROM aapkadaan__transaction tr,aapkadaan__user u WHERE 1 AND ((tr.sender_no = u.phone_no AND tr.type IN(1,6,5)) OR ((tr.transfer_in =u.phone_no OR tr.transfer_out =u.phone_no) AND tr.type IN(4,2))) AND tr.add_date<'2017-08-01'GROUP BY tr.id_transaction) tu LEFT JOIN aapkadaan__user us ON tu.phone_field=us.phone_no ORDER BY tu.id_transaction DESC";
    $excel_sql="SELECT *  FROM `aapkadaan__transaction` WHERE `sender_no` LIKE '9643006238' or `donor_no` LIKE '9643006238' or `transfer_in` LIKE '9643006238' or `transfer_out` LIKE '9643006238' or `transfer_from_middle` LIKE '9643006238'
ORDER BY `aapkadaan__transaction`.`id_transaction`  DESC";
 
    $res = mysqli_query($link, $excel_sql);
//        print $excel_sql;exit;
    //For showing name of the donor in excel sheet
    $user_sql = "SELECT phone_no,address,city,district,CONCAT(first_name,' ', IFNULL(last_name, '')) as name"
            . " FROM aapkadaan__user WHERE id_admin NOT IN(99)";
    $acc_arr = getindexrows($user_sql, 'phone_no');

    $remove = array("\n", "\t", "\r");
    header("Content-type: application/msexcel");
    if ($this->_input['report'] == 3) {
      header("Content-disposition: filename=" . "export_" . date("Y-m-d_H-i", time()) . "_" . $report . ".xls");
      echo "Status" . "\t" . "Name" . "\t" . "Phone No" . "\t" . "Referred By" . "\t"
      . "Level" . "\t" . "Cash Limit" . "\t" . "Cash In hand " . "\t" . "Cheque In Hand  " ."\t" . "Total Cash Collected "."\t" . "Total Check Collected ". "\n";  //Include it if Header is needed in Excel sheet
    } else {
      header("Content-disposition: filename=" . "export_" . date("Y-m-d_H-i", time()) . "_" . $report . ".xls");
      echo "Type" . "\t" . "Date of Transaction" . "\t" . "Cheque No" . "\t" . "Receipt No" . "\t"
      . "Receiver Name" . "\t" . "Receiver No" . "\t" . "Donor No " . "\t" . "Donor Name  " . "\t"
      . "Amount   " . "\t" . "Donate To" . "\t" . "Mode " . "\t" . "\n";  //Include it if Header is needed in Excel sheet
    }
    while ($row = mysqli_fetch_assoc($res)) {
      /* if there is \n or \t or \r is present in the message field,then this part is used for removing those special characters from the string
        $message = str_replace($remove, "", str_replace('"', "'", $row['message']));
       */
      //Condition checking for displaying the donor name
      if ($this->_input['report'] == 3) {
        if ($row['live_demo'] == 1)
          $type = 'Live';
        else
          $type = 'Demo';
        if ($row['is_level'] != '') {
          if ($row['level'] == 1) {
            $level = 'National';
          } else if ($row['level'] == 2) {
            $level = 'State';
          } else if ($row['level'] == 4) {
            $level = 'District';
          } else if ($row['level'] == 8) {
            $level = 'Assembly';
          } else {
            $level = 'Fund Raiser';
          }
        }
		$t_cash=$collection_report[$row['id_user']]['cash_collection']?$collection_report[$row['id_user']]['cash_collection']:0;
		$t_cheqe=$collection_report[$row['id_user']]['cheque_collection']?$collection_report[$row['id_user']]['cheque_collection']:0;
        echo $type . "\t" . $row['first_name'] . " " . $row['last_name'] . " \t" . '"' . $row['phone_no'] . '"' . "\t"
        . $row['name'] . "\t" . $level . "\t"
        . $row['fund_limit'] . "\t" . $row['fund_in_hand'] . "\t"
        . $row['cheque_amount'] . "\t" .$t_cash ."\t" .$t_cheqe."\n";
        ob_flush();  //This is necessary for downloading large excel file
      } else {
        if ($this->_input['report'] == 1) {
          $donorname = $row['donor_name'];
          $receiver_name = $acc_arr[$row['sender_no']]['name'];
          $donor_no = $row['donor_no'];
          $sender_no = $row['sender_no'];
        } else {
          if ($row['transfer_out'] != '' && $row['status'] == 2) {
            $receiver_name = $acc_arr[$row['transfer_out']]['name'];
            $donorname = $acc_arr[$row['transfer_in']]['name'];
            $donor_no = $row['transfer_in'];
            $sender_no = $row['transfer_out'];
          } elseif ($row['transfer_out'] != '' && $row['status'] == 8) {
            $donorname = $acc_arr[$row['transfer_out']]['name'];
            $receiver_name = $acc_arr[$row['transfer_in']]['name'];
            $donor_no = $row['transfer_out'];
            $sender_no = $row['transfer_in'];
          } else if ($row['transfer_out'] == '' && $row['status'] == 1) {
            $receiver_name = $acc_arr[$row['sender_no']]['name'];
            $donor_no = $row['donor_no'];
            $sender_no = $row['sender_no'];
          } else if ($row['transfer_out'] == $row['transfer_in'] && $row['status'] == 1 && $row['transfer_from_middle'] != '') {
            $donor_no = $row['transfer_from_middle'];
            $sender_no = $row['transfer_in'];
            $donorname = $acc_arr[$row['transfer_from_middle']]['name'];
            $receiver_name = $row['receiver_name'];
          } else {
            if ($row['type'] == '5') {
              $donorname = $acc_arr['donor_no']['name'];
            } else {
              $donorname = $row['donor_name'];
            }
            $receiver_name = $row['receiver_name'];
            if ($row['transfer_out'] != '' && $row['status'] == 1) {
              $donor_no = $row['donor_no'];
              $sender_no = $row['transfer_out'];
            } else {
              $donor_no = $row['donor_no'];
              $sender_no = $row['sender_no'];
            }
          }
        }
        //$receiver_name = $acc_arr[$row['sender_no']]['name'];
        if ($row['type'] == 1) {
          $type = "Cash Donation";
        } elseif ($row['type'] == 2) {
          $type = "cheque Donation";
        } elseif ($row['type'] == 3) {
          $type = "Ecs";
        } elseif ($row['type'] == 4) {
          $type = "Cash Deposit";
        } elseif ($row['type'] == 5) {
          $type = "Cash Transfer";
        } else {
          $type = "Self Donation";
        }
		if($row['donate_to'] == 1){
			$donate_to='National';
		}elseif($row['donate_to'] == 2){
			$donate_to='State:'.$row['donate_to_state'];
		}elseif($row['donate_to'] == 4){
			$donate_to="State:".$row['donate_to_state']."::District:".$row['donate_to_district'];
		}elseif($row['donate_to'] == 8){
			$donate_to="State:".$row['donate_to_state']."::Distric:".$row['donate_to_district']."::Assembly:".$row['donate_to_assembly'];
		}
        echo $type . "\t" . date('d-m-Y H:i:s', strtotime($row['add_date'])) . "\t" . '"' . $row['cheque_no'] . '"' . "\t"
        . $row['receipt_no'] . "\t" . $receiver_name . "\t"
        . $sender_no . "\t" . $donor_no . "\t"
        . $donorname . "\t" . $row['amount'] . "\t" .$donate_to. "\t"
        . $row['method_of_post'] . "\t" . "\n";
        ob_flush();  //This is necessary for downloading large excel file
      }
    }
    exit;
  }

}

