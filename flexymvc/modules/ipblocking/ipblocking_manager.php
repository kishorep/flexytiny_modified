<?php

class ipblocking_manager extends mod_manager {

    /**
     * 
     * This is a constructor to initialized ipblocking module
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'ipblocking', $modTable = 'login') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
    }
    function get_module_name() {
	return 'ipblocking';
    }

    function get_manager_name() {
	return 'ipblocking';
    }

    /**
     * @method check_interval() function to check the interval of blocking</p>.
     * 
     * @return int It returns 0 if the condition satisfies.
     */
    function check_interval() {
	$sql_timediff = get_search_sql('blockedip', '', 'MAX(time_fail) as maxtime');
	$diff = getrows($sql_timediff, $err);
	if (count($diff)) {
	    $maxtime = $diff[0]['maxtime'];
	    $sql = "SELECT TIMEDIFF(TIMEDIFF(now(),$maxtime),'00:15:00') as interval";
	    $res = getrows($sql, $err);
	    $interval = $res[0]['interval'];
	}
	if (strpos($interval, '-')) {
	    $fail = 0;
	}
    }

    /**
     * @method insert_login() Function is used to insert a record in the login table when ever there is a login operation is taking place.</p>
     				Here also we are checking if the status is 5 or 6 (meaning the username and password given wrong) 
    				then a record is inserted into the table blockedip.</p>
     * @param array $arr Array of records to be inserted.
     * @param int $status This is the status field.
     * @return int Retuen the id of the inserted row.
     */
    function insert_login($arr, $status) {
	$sql = get_search_sql('login', "ip='" . $_SERVER['REMOTE_ADDR'] . "' ORDER BY id_login DESC LIMIT 1", "failure_attempt,ip");
	$res = getrows($sql, $err);
	$fail = (count($res) > 0) ? $res[0]['failure_attempt'] + 1 : 0;

	//entering the status of ip blocking to blockedip table.
	if ($status == 6 || $status == 5) {
	    $failure = $this->getipfailurevalue();

	    //blocking the ip if failure is more than the spacified value in the conf.
	    $ipcount = $GLOBALS['conf']['BLOCK_IP']['count'];
	    $ipstatus = ($failure >= ($ipcount - 1)) ? $GLOBALS['conf']['IPINFO']['statusblock'] : $GLOBALS['conf']['IPINFO']['statusactive'];
	    $block_time = ($failure > ($ipcount - 1)) ? date("Y-m-d H:i:s", strtotime('now')) : '';
	    $sql = "INSERT INTO " . TABLE_PREFIX . "blockedip VALUES('','" . $_SERVER['REMOTE_ADDR'] . "','" . $arr['username'] . "','" . $failure . "','" . $ipstatus . "','" . $block_time . "')";
	    execute($sql, $err);
	} else if ($status == 1) {
	    $fail = 0;
	}
	//inserting to database.
	$sql = "INSERT INTO " . TABLE_PREFIX . "login(id_login,id_user,ip,date_login,username,email,status,failure_attempt)
				 values(
				 	'',
					'" . $arr['id_user'] . "',
					'" . $_SERVER['REMOTE_ADDR'] . "',
					now(),
					'" . $arr['username'] . "',
					'" . $arr['email'] . "',
					'" . $status . "',
					'" . $fail . "'
				 )";
	execute($sql, $err);
	$ret = getinsertid();
	return $ret;
    }

    /**
     * @method block_ip() function is used to set the value of the flag field of user table from 1 to 0.
     * 
     * @return void
     */
    function block_ip() {
	$sql = "UPDATE " . TABLE_PREFIX . "user
				SET flag=0 WHERE ip=" . $_SERVER['REMOTE_ADDR'];
	execute($sql, $err);
    }

    /**
     * @method insert_block_ip() function insert a record into the table blockedip to make the user blocked.
     * 
     * @param type $user.
     * 
     * @return void.
     */
    function insert_block_ip($user) {
	$sql = "INSERT INTO " . TABLE_PREFIX . "blockedip VALUES('','" . $_SERVER['REMOTE_ADDR'] . "','" . $user . "')";
	execute($sql, $err);
    }

    /**
     * @method getipfailurevalue() function is used to return the number of time the user failed to login.
     * 
     * @return int The count of failure.
     */
    function getipfailurevalue() {
	$sql = get_search_sql('blockedip', "ip='" . $_SERVER['REMOTE_ADDR'] . "' ORDER BY id_block DESC LIMIT 1", 'ip,ipfailure');
	$res = getrows($sql, $err);
	$fail = (count($res) > 0) ? $res[0]['ipfailure'] + 1 : 0;
	return $fail;
    }

};