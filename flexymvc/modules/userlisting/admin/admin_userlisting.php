<?php

class admin_userlisting extends userlisting_manager {

    /**
     * This is a constructor to initialized product admin module.This overrides user side constructor.
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input) {
        if ($_SESSION['id_admin']) {
            parent::__construct($smarty, $_output, $_input);
        } else {
            redirect(LBL_ADMIN_SITE_URL);
        }
    }
    
    function _list_user(){
//        echo "<pre>";        print_r($_REQUEST);exit;
       // echo 23;exit;
        $data= get_search_sql('practice');
        $res=getrows($data,$err);
        $this->_output['res']=$res;
        $this->_output['tpl'] = "admin/userlisting/list_user";
    }
    function _form_user(){//pr(111);exit;
        $id=$this->_input['id'];//pr($id);exit;
        if($id){
            $res=recordExists('practice','id='.$id);//pr($res);exit;
            $this->_output['res']=$res;
        }
            $this->_output['tpl'] = "admin/userlisting/form_user"; 
       
    }
    function _insert_user() {
        ob_clean();
        $user = $this->_input[user];
        // pr($user[id]);exit;
        $id = $user[id];
        if ($id) {
            $val = $this->dataObj->updateArrayFormat($user, "id=" . $id, "practice");
            $_SESSION['raise_message']['global'] = "data updated";
        } else {
            $val = $this->dataObj->insertArrayFormat($user, "", "practice");
            $_SESSION['raise_message']['global'] = "data inserted";
        }
        if ($val) {

            redirect(LBL_ADMIN_SITE_URL . "userlisting/list_user");
//$this->_input['page'] = "userlisting";
//$this->_input['choice'] = "list_user";
//$this->_list_user();
        } else 
        {
            echo "data insertion failed";
        }   
    }
    function _delete_user(){
        $id=$this->_input[id];
        $this->dataObj->delete('id='.$id,"practice");
       
       $this->_input['page']='userlisting';
       $this->_input['choice']='list_user';
       $this->_list_user();

    }
    function _show_user(){
        $this->_output['tpl']="admin/userlisting/show_user";
    }
}