<?php

class admin_redirect extends redirect_manager {

    /**
     * This is a constructor to initialized product admin module.This overrides user side constructor.
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input) {
        if ($_SESSION['id_admin']) {
            parent::__construct($smarty, $_output, $_input);
        } else {
            redirect(LBL_ADMIN_SITE_URL);
        }
    }
	
	function _listing() {
		$uri = PAGINATE_ADMIN_PATH . "redirect/listing";
		$cond = 1;
		if ($this->_input['psearch']) {
			parse_str($this->_input['searchval'],$data);
			$uri = $uri . "/psearch/1";
			if ($data['old_name']) {
				$cond .= " AND old_url LIKE '%" .$data['old_name'] . "%'";
			}
			if ($data['new_name']) {
				$cond .= " AND new_url LIKE '%" .$data['new_name'] . "%'";
			}
		}

		
			$sql = get_search_sql("redirecturl",$cond);
		// AND p.image_type !=2  this part is added by prabhu
		$this->_output['limit'] =10;//$GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] =3;// $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
                $this->_output['uri'] = $uri;
		$this->_output['ajax'] = '1';
		$this->_output['qstart'] = $this->_input['qstart'];
		$this->_output['sql'] = $sql;
		$this->_output['uri'] = urlencode($uri);
		$this->_output['sort_by'] = "id";
		$this->_output['sort_order'] = "ASC";
		$this->_output['ajax'] = 1;

		if (!(($this->_input['insEditDel'] == 1) || ($this->_input['pg'] == 1) || ($this->_input['psearch'] == 1))) {
			$this->blObj->page_listing($this, 'admin/redirect/search');
		} else {
			$this->blObj->page_listing($this, 'admin/redirect/listing');
		}
	}
	
	function _addredirecturl(){
		if($this->_input['id']){
			$res=  recordExists("redirecturl", "id={$this->_input['id']}");
		$this->_output['res'] =$res;
		}
		$this->_output['tpl'] ='admin/redirect/add';
	}
	function _insertredirecturl(){
		$exp=$this->_input['exp'];
		if($this->_input['id']){
		$this->dataObj->updateArrayFormat($exp, "id= " . $this->_input['id'], "redirecturl");
		}else{
		$exp['add_date']=date("Y-m-d H:i:s");
		$idcat=$this->dataObj->insertArrayFormat($exp, "", "", "redirecturl");
		}
		$upsql="UPDATE ".TABLE_PREFIX."redirecturl SET new_url='{$exp['new_url']}' WHERE new_url='{$exp['old_url']}'";
		execute($upsql, $err);
		$this->_input['choice']="listing";
		$this->_input['insEditDel']=1;
		$this->_listing();
	}
	
	function _deleteredirecturl(){
		$this->dataObj->delete("id={$this->_input['id']}", "redirecturl");
		$this->_input['choice']="listing";
		$this->_input['insEditDel']=1;
		$this->_listing();
	}
}