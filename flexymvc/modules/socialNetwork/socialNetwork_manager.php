<?php

chdir(APP_ROOT . "flexymvc/modules/user");
include("user_manager.php");

class socialNetwork_manager extends mod_manager {

    /**
     * This is a constructor to initialized category module
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'socialNetwork', $modTable = '') {
        $this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
        //check_session();
    }

    function get_module_name() {
        return 'socialNetwork';
    }

    function get_manager_name() {
        return 'socialNetwork';
    }

    function _commonSocialNetwork() {
        $this->_output['id_user'] = $this->_input['id_user'] ? $this->_input['id_user'] : md5($_SESSION['id_user']);
        $this->_output['tpl'] = 'socialNetwork/common_social_network';
    }

    function _getUserId() {
        if ($_SESSION['id_user']) {
            $sql = get_search_sql("user", " 1 AND id_user = " . $_SESSION['id_user'], "fconnect");
        } else {
            $sql = get_search_sql("user", " 1 AND fconnect = " . $this->_input['userID'], "fconnect");
        }
        $res = getsingleindexrow($sql);
        ob_clean();
        if ($res) {
            print $res['fconnect'];
            exit;
        } else {
            print "0";
            exit;
        }
    }

    function _skipSocial() {
        if (!$_SESSION['id_user']) {
            $_SESSION['raise_message']['global'] = getmessage('USER_REG_SUCC_NO_FB');
            if ($GLOBALS['conf']['LOGIN']['autologin'] == 1) {
                $sql_auto = get_search_sql("user", "md5(id_user) ='" . $this->_input['fid'] . "'", "username,password");
                $res_auto = getsingleindexrow($sql_auto);
                user_manager::_setLogin($res_auto['username'], $res_auto['password'], 0);
            }
        }
        redirect(LBL_SITE_URL);
    }

    function _linkedin() {
        ob_clean();
        $uid = $this->_input['userID'];
        $sql = get_search_sql("user", "id_linkedin='" . $uid . "' LIMIT 1");
        $res = getsingleindexrow($sql);

        if ($res) {
            $_SESSION['linkedin_flag'] = 1;
            user_manager::_setLogin($res['username'], $res['password'], 0, 1);
        } else {
            $sql = get_search_sql("user", "email = '" . $this->_input['email'] . "' LIMIT 1");
            $res_email = getsingleindexrow($sql);

            if (!empty($res_email)) {
                $arr['id_linkedin'] = $uid;
                $this->dataObj->updateArrayFormat($arr, "email = '" . $this->_input['email'] . "'", "user");
                $_SESSION['linkedin_flag'] = 1;
                user_manager::_setLogin($res_email['username'], $res_email['password'], 0, 1);
            } else {
                $arr['user']['first_name'] = $this->_input['first_name'];
                $arr['user']['last_name'] = $this->_input['last_name'];
                $arr['user']['username'] = strtolower($this->_input['first_name']) . strtolower($this->_input['last_name']) . substr(md5(uniqid()), 0, 5);
                $arr['user']['password'] = substr(md5(uniqid()), 0, 10);
                $arr['user']['email'] = $this->_input['email'];

                $arr['cpwd'] = $arr['user']['password'];
                $arr['id_linkedin'] = $this->_input['userID'];

                user_manager::_insert($arr);
            }
        }
    }

    function _linkedinConnect() {
        $this->_output['tpl'] = 'socialNetwork/linkedinConnect';
    }

    function _connectLinkedin() {
        $lid = $this->_input['id_linkedin'];
        $id_user = $this->_input['id_user'] ? $this->_input['id_user'] : md5($_SESSION['id_user']);
        $chk_user = recordExists("user", "id_linkedin = '" . $lid . "'");
        if (empty($chk_user)) {
            $arr['random_num'] = '0';
            $arr['id_linkedin'] = $lid;
            $this->dataObj->updateArrayFormat($arr, "md5(id_user) = '" . $id_user . "'", "user");
            $sql_auto = get_search_sql("user", "id_linkedin='" . $lid . "'", "username,password");
            $res_auto = getsingleindexrow($sql_auto);
            $_SESSION['linkedin_flag'] = 1;
            if (!$_SESSION['id_user']) {
                user_manager::_setLogin($res_auto['username'], $res_auto['password'], 0, 1);
            } else {
                $_SESSION['id_linkedin'] = $lid;
                $_SESSION['raise_message']['global'] = getmessage("USER_LINKEDIN_SUCC");
                redirect(LBL_SITE_URL);
            }
        } else {
            $_SESSION['raise_message']['global'] = getmessage("LINKEDIN_ACCOUNT_EXISTS");
            redirect(LBL_SITE_URL);
        }
    }

    function _facebook() {
        ob_clean();
        $uid = $this->_input['userID'];
        $sql = get_search_sql("user", "fconnect='" . $uid . "' LIMIT 1");
        $res = getsingleindexrow($sql);

        if ($res) {
            $_SESSION['fconnect_flag'] = "1";
            user_manager::_setLogin($res['username'], $res['password'], 1);
        } else {
            $sql = get_search_sql("user", "email = '" . $this->_input['email'] . "' LIMIT 1");
            $res_email = getsingleindexrow($sql);
            if (!empty($res_email)) {
                $arr['fconnect'] = $uid;
                $this->dataObj->updateArrayFormat($arr, "email = '" . $this->_input['email'] . "'", "user");
                $_SESSION['fconnect_flag'] = "1";
                user_manager::_setLogin($res_email['username'], $res_email['password'], 1);
            } else {
                $arr['user']['first_name'] = $this->_input['first_name'];
                $arr['user']['last_name'] = $this->_input['last_name'];
                $arr['user']['username'] = strtolower($this->_input['first_name']) . strtolower($this->_input['last_name']) . substr(md5(uniqid()), 0, 5);
                $arr['user']['password'] = substr(md5(uniqid()), 0, 10);
                $arr['user']['email'] = $this->_input['email'];

                $arr['cpwd'] = $arr['user']['password'];
                $arr['gender'] = $this->_input['gender'] == 'male' ? "M" : "F";
                $arr['fconnect'] = $this->_input['userID'];

                user_manager::_insert($arr);
            }
        }
    }

    function _fconnect() {
        $this->_output['fid'] = $this->_input['fid'] ? $this->_input['fid'] : '';
        $this->_output['tpl'] = 'socialNetwork/fconnect';
    }

    function _connect_facebook() {
        $id_user = $this->_input['fid'];
        $uid = $this->_input['selected_profiles'];
        $chk_user = recordExists("user", "fconnect = '" . $uid . "'");
        if (empty($chk_user)) {
            $arr['random_num'] = '0';
            $arr['fconnect'] = $uid;
            $this->dataObj->updateArrayFormat($arr, "md5(id_user) = '" . $id_user . "'", "user");
            $_SESSION['fconnect_flag'] = "1";
            $_SESSION['fconnect'] = $uid;
            if (!$_SESSION['id_user']) {
                $_SESSION['reg'] = '1';
                $sql_auto = get_search_sql("user", "fconnect='" . $uid . "'", "username,password");
                $res_auto = getsingleindexrow($sql_auto);
                user_manager::_setLogin($res_auto['username'], $res_auto['password'], '11');
            } else {
                $_SESSION['raise_message']['global'] = getmessage("USER_FB_LOGIN_SUCC");
                redirect(LBL_SITE_URL);
            }
        } else {
            $_SESSION['raise_message']['global'] = getmessage('FACEBOOK_ACCOUNT_EXISTS');
            redirect(LBL_SITE_URL);
        }
    }

    function _tconnect() {
        $this->_output['fid'] = $this->_input['fid'] ? $this->_input['fid'] : '';
        $this->_output['tpl'] = 'socialNetwork/twitterConnect';
    }

    function _twitterConnect() {
        include_once(APP_ROOT . "twitter-library/twitteroauth.php");
        if (isset($this->_input['oauth_token']) && $_SESSION['token'] !== $this->_input['oauth_token']) {
            session_destroy();
            redirect(LBL_SITE_URL);
        } elseif (isset($this->_input['oauth_token']) && $_SESSION['token'] == $this->_input['oauth_token']) {
            $connection = new TwitterOAuth($GLOBALS['conf']['TWITTER']['consumer_key'], $GLOBALS['conf']['TWITTER']['consumer_secret'], $_SESSION['token'], $_SESSION['token_secret']);
            $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
            if ($connection->http_code == '200') {

                $_SESSION['status'] = 'verified';
                $_SESSION['request_vars'] = $access_token;
                $twitterid = $_SESSION['request_vars']['user_id'];

                $my_tweets = $connection->get("https://api.twitter.com/1/users/lookup.json?user_id={$twitterid}&include_entities=true");
                $obj = $my_tweets[0];
                $_SESSION['tname'] = $obj->name;

                unset($_SESSION['token']);
                unset($_SESSION['token_secret']);
                redirect(LBL_SITE_URL . "socialNetwork/twittDetails");
            } else {
                die("error, try again later!");
            }
        } else {
            if (isset($_GET["denied"])) {
                redirect(LBL_SITE_URL);
                die();
            }

            $connection = new TwitterOAuth($GLOBALS['conf']['TWITTER']['consumer_key'], $GLOBALS['conf']['TWITTER']['consumer_secret']);
            $request_token = $connection->getRequestToken(APP_ROOT_URL . 'user/twitt_connect');

            //received token info from twitter
            $_SESSION['token'] = $request_token['oauth_token'];
            $_SESSION['token_secret'] = $request_token['oauth_token_secret'];

            // any value other than 200 is failure, so continue only if http code is 200
            if ($connection->http_code == '200') {
                //redirect user to twitter
                $twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
                header('Location: ' . $twitter_url);
            } else {
                die("error connecting to twitter! try again later!");
            }
        }
    }

    function _twittDetails() {
        session_start();
        //just simple session reset on logout click
        if ($_GET["reset"] == 1) {
            session_destroy();
            redirect(LBL_SITE_URL);
        }
        include_once(APP_ROOT . "twitter-library/twitteroauth.php");

        $screenname = $_SESSION['request_vars']['screen_name'];
        $twitterid = $_SESSION['request_vars']['user_id'];
        $oauth_token = $_SESSION['request_vars']['oauth_token'];
        $oauth_token_secret = $_SESSION['request_vars']['oauth_token_secret'];
        $getUserData = getsingleindexrow(get_search_sql("user", "twitt_connect={$twitterid}"));

        unset($_SESSION['request_vars']);

        if ($_SESSION['id_user']) {
            if (empty($getUserData)) {
                $this->dataObj->updateStringFormat("twitt_connect", $twitterid, "id_user={$_SESSION['id_user']}", "user");
                $_SESSION['twitt_connect'] = $twitterid;
                $_SESSION['twitter_flag'] = 1;
                $_SESSION['raise_message']['global'] = getmessage('USER_TWITTER_LOGIN_SUCC'); //"Successfully connected with Twitter";
                redirect(LBL_SITE_URL . "user/userHome");
            } else {
                $_SESSION['raise_message']['global'] = getmessage('TWITTER_ACCOUNT_EXISTS');
                redirect(LBL_SITE_URL);
            }
        } else {
            if (!empty($getUserData)) {
                if ($getUserData['email']) {
                    $_SESSION['twitter_flag'] = 1;
                    user_manager::_setLogin($getUserData['username'], $getUserData['password'], 0, 0, 1);
                } else {
                    redirect(LBL_SITE_URL . "socialNetwork/twitterEmail/uid/" . md5($getUserData['id_user']));
                }
            } else {
                $tname = explode(" ", $_SESSION['tname']);

                $arr['user']['first_name'] = strtolower($tname['0']);
                $arr['user']['last_name'] = strtolower($tname['1']);
                $arr['user']['username'] = $screenname . substr(md5(uniqid()), 0, 5);
                $arr['user']['password'] = substr(md5(uniqid()), 0, 10);
                $arr['user']['email'] = "";

                $arr['cpwd'] = $arr['user']['password'];
                $arr['twitt_connect'] = $twitterid;
                unset($_SESSION['tname']);
                user_manager::_insert($arr);
            }
        }
    }

    function _twitterEmail() {
        $id_user = $this->_input['uid']; // ? $this->_input['uid'] : md5('310');
        $sql = get_search_sql('user', "md5(id_user) = '" . $id_user . "'", "id_user,first_name,last_name");
        $this->_output['res'] = getsingleindexrow($sql);
        $this->_output['tpl'] = 'socialNetwork/twitterEmail';
    }

    function _updateEmail() {
        $this->dataObj->updateStringFormat("email", $this->_input['email'], "id_user={$this->_input['id_user']}", "user");
        $sql = get_search_sql("user", "id_user = '" . $this->_input['id_user'] . "'", "username,password,first_name,email");
        $user = getsingleindexrow($sql);
        $_SESSION['twitter_flag'] = 1;

        $info['social_flag'] = "Twitter";
        $info['first_name'] = $user['first_name'];
        $info['username'] = $user['username'];
        $info['password'] = $user['password'];
        $this->smarty->assign('sm', $info);
        $mail_message = $this->smarty->fetch($this->smarty->add_theme_to_template("user/account_activate"));
        $subject = "Get Started with " . $GLOBALS['conf']['SITE_ADMIN']['admin_email'];
        $r = sendmail($user['email'], $subject, $mail_message, $GLOBALS['conf']['SITE_ADMIN']['email']);

        user_manager::_setLogin($user['username'], $user['password'], 0, 0, '11');
    }

    function _checkValidUser() {
        $cond = "";
        if ($this->_input['username'] && $this->_input['password']) {
            $cond = "username = '" . $this->_input['username'] . "' AND password = '" . $this->_input['password'] . "'";
        } elseif ($this->_input['id_facebook']) {
            $cond = "fconnect='" . $this->_input['id_facebook'] . "'";
        } elseif ($this->_input['id_linkedin']) {
            $cond = "id_linkedin='" . $this->_input['id_linkedin'] . "'";
        } else {
            print('error');
            exit;
        }

        $sql = get_search_sql("user", $cond, "id_user,username,password,first_name");
        $res = getsingleindexrow($sql);

        if (!empty($res)) {
            $sql_tw = get_search_sql("user", "id_user = '" . $this->_input['prev_user'] . "'", "id_user,twitt_connect");
            $res_tw = getsingleindexrow($sql_tw);
            $this->dataObj->updateStringFormat("twitt_connect", $res_tw['twitt_connect'], "id_user={$res['id_user']}", "user");
            $this->dataObj->delete("id_user={$res_tw['id_user']}", "user");
            $_SESSION['twitter_flag'] = 1;

            user_manager::_setLogin($res['username'], $res['password'], 0, 0, '11');
        } else {
            print('error');
            exit;
        }
    }

    function _google_loginurl() {
        include_once(APP_ROOT . "flexymvc/classes/common/Google/src/Google_Client.php");
        include_once(APP_ROOT . "flexymvc/classes/common/Google/src/contrib/Google_Oauth2Service.php");
        $google_client_id = $GLOBALS['conf']['GOOGLE']['client_id']; //'518015102173-emljres741uscd5bavcqscgbae17cind.apps.googleusercontent.com'; // $GLOBALS['conf']['GOOGLE']['client_id']
        $google_client_secret = $GLOBALS['conf']['GOOGLE']['client_secret']; //'2VfXbjGbWHVHS796oFDmpI7H';// $GLOBALS['conf']['GOOGLE']['client_secret']
        $google_redirect_url = LBL_SITE_URL . 'socialNetwork/google_loginurl/'; //path to your script
        $gClient = new Google_Client();
//$gClient->setApplicationName('Aapkadaan Google Login');
        $gClient->setClientId($google_client_id);
        $gClient->setClientSecret($google_client_secret);
        $gClient->setRedirectUri($google_redirect_url);
        $gClient->setScopes("https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email");
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        if (isset($_REQUEST['code'])) {
//For logged in user, get details from google using access token
            $gClient->authenticate($_REQUEST['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            $gClient->setAccessToken($_SESSION['token']);
            $user = $google_oauthV2->userinfo->get();

            $arr['user']['first_name'] = $user['given_name'];
            $arr['user']['last_name'] = $user['family_name'];
            $arr['user']['username'] = ($GLOBALS['conf']['SOCIAL_REGD_TYPE']['regd_type'] == 1 || $GLOBALS['conf']['SOCIAL_REGD_TYPE']['regd_type'] == 2) ? $user['given_name'] : $user['email'];
            $arr['user']['password'] = substr(md5(uniqid()), 0, 10);
            $arr['user']['email'] = $user['email'];
            $arr['user']['id_google'] = $user['id'];
            $arr['user']['type'] = 'id_google';
            $this->_commonInsert($arr);
            exit;
        } else {
//For Guest user, get google login url
            $gClient->setAccessType('online');
            $gClient->setApprovalPrompt('auto');
            $auth = $gClient->createAuthUrl();
            header("location:$auth");
//            exit;
            //return $auth;
        }
    }

    function _fb_loginurl() {
        $arr['user']['fconnect'] = $this->_input['userID'];
        $arr['user']['first_name'] = $this->_input['first_name'];
        $arr['user']['last_name'] = $this->_input['last_name'];
        $arr['user']['gender'] = $this->_input['gender'];
        $arr['user']['email'] = $this->_input['email'];
        $arr['user']['password'] = substr(md5(uniqid()), 0, 10);
        $arr['user']['username'] = strtolower($this->_input['first_name']) . strtolower($this->_input['last_name']) . substr(md5(uniqid()), 0, 5);
        $arr['user']['type'] = 'fconnect';
        $this->_commonInsert($arr);
        exit;
    }

    function _commonInsert($arr = "") {
        $type = $arr['user']['type'];
        unset($arr['user']['type']);
        unset($_SESSION['regd_type']);
        unset($_SESSION['login_type']);
        $this->_input = $arr ? $arr : $this->_input;
        $user = $this->_input['user'];
        $uid = $arr['user'][$type];
        $social_regd_type = $GLOBALS['conf']['SOCIAL_REGD_TYPE']['regd_type'];
        $sql = get_search_sql("user", "{$type}='" . $uid . "' AND random_num ='0' LIMIT 1");
        $res = getsingleindexrow($sql);
        $_SESSION['login_type'] = $type;

        if (!empty($res)) {
            if ($type == 'fconnect') {
                user_manager::_setLogin($res['username'], $res['password'], 1);
            } else if ($type == 'id_google') {
                user_manager::_setLogin($res['username'], $res['password'], '', '', '', 1);
            } else if ($type == 'twitt_connect') {
                user_manager::_setLogin($res['username'], $res['password'], '', '', 1);
            }
        } else {
            $res_email = '';
            if (isset($user['email'])) {
                if ($user['email'] != 'undefined' || $user['email'] != '') {
                    $sql = get_search_sql("user", "email = '" . $user['email'] . "' LIMIT 1");
                    $res_email = getsingleindexrow($sql);
                }
            }
            if (!empty($res_email)) {
//already registered but not updated and set his new password (random_num !=0)
                if ($res_email['random_num'] != '0') {
                    $_SESSION[$type] = $uid;
                    $_SESSION['regd_type'] = $social_regd_type;
                    $edit_url = LBL_SITE_URL . "user/edit";
                    header("location:$edit_url");
                } else {
//already registered user, but google id/id_facebook is not present in database.so here is code for updating id_google/id_facebook
                    unset($user);
                    $user['email'] = $this->_input['user']['email'];
                    if (!empty($user['email'])) {
                        $user[$type] = $uid;
                        if ($type == 'fconnect') {
                            $user['fb_email'] = $arr['user']['email'];
                        } else if ($type == 'id_google') {
                            $user['google_email'] = $arr['user']['email'];
                        }
                        $this->dataObj->updateArrayFormat($user, "email = '" . $user['email'] . "'", "user");
                        user_manager::_setLogin($res_email['username'], $res_email['password']);
                    }
                }
            } else {
                if ($type == 'fconnect') {
                    $user['fb_email'] = ($arr['user']['email'] != 'undefined') ? $arr['user']['email'] : '';
                } else if ($type == 'id_google') {
                    $user['google_email'] = $arr['user']['email'];
                }
                /*                 * user is new* */
//case -1:when only admin have the authority to register an user
                if ($social_regd_type == 3) {
                    $_SESSION['raise_message']['global'] = "You are not authorized to login";
                    header("location:" . LBL_SITE_URL . "user/loginForm");
//case-2:Direct Login using gmail id and password
                } else if ($social_regd_type == 4) {
                    $_SESSION[$type] = $user;
                    header("location:" . LBL_SITE_URL . "user/insert");
                } else {
                    $confirm_code = md5(uniqid(rand(), true));
                    if ($type != 'twitt_connect') {
                        unset($user['username']);
                    } else {
                        $user['username'] = strtolower($user['username']);
                    }
                    $user['email'] = ($arr['user']['email'] != 'undefined') ? $arr['user']['email'] : '';
                    $ins = $this->dataObj->insertArrayFormat($user, "random_num,add_date,ip", "'" . $confirm_code . "',NOW(),'" . $_SERVER['REMOTE_ADDR'] . "'", "user");
//case-3 :Login with basic information of a user
                    if ($social_regd_type == 1) {
                        $_SESSION['regd_type'] = $social_regd_type;
                        $_SESSION[$type] = $uid;
                        header("location:" . LBL_SITE_URL . "user/edit");
//case-4:Login with extended information along with the basic information
                    } elseif ($social_regd_type == 2) {
                        $_SESSION['regd_type'] = $social_regd_type;
                        $_SESSION[$type] = $uid;
                        header("location:" . LBL_SITE_URL . "user/edit");
                    }
                }
                /*                 * End * */
            }
        }
    }

    function _twitter() {
        include_once APP_ROOT . "flexymvc/classes/common/twitter/tmhOAuth.php";
        include_once APP_ROOT . "flexymvc/classes/common/twitter/tmhUtilities.php";

        $tmhOAuth = new tmhOAuth(array(
            'consumer_key' => $GLOBALS['conf']['TWITTER']['consumerKey'], //'EfQ90vcTsQmO1Ji8yRyxDNYEE',
            'consumer_secret' => $GLOBALS['conf']['TWITTER']['consumerSecret']//Le6YIqmKjjSj97kCzg8VtEmFWwU0QnuBEuG3oMJRSymxpRdwi5'
        ));

        if ($this->_input['type']) {
            $_SESSION['type'] = $this->_input['type'];
        }

        if ($this->_input['oauth_verifier']) {
            $this->_twitter_accessToken($tmhOAuth);
        } else {
            $this->_twitter_requestToken($tmhOAuth);
        }
    }

    function _twitter_requestToken($tmhOAuth) {
        $code = $tmhOAuth->request(
                'POST', $tmhOAuth->url('oauth/request_token', ''), array(
            'oauth_callback' => tmhUtilities::php_self() . "/"
                )
        );
        if ($code == 200) {
            $_SESSION['oauth'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
            $this->_twitter_authorize($tmhOAuth);
        } else {
            outputError($tmhOAuth);
        }
    }

    function _twitter_authorize($tmhOAuth) {

        $authurl = $tmhOAuth->url("oauth/authenticate", '') . "?oauth_token={$_SESSION['oauth']['oauth_token']}";
        header("Location: {$authurl}");

        // in case the redirect doesn't fire
        echo '<p>To complete the OAuth flow please visit URL: <a href="' . $authurl . '">' . $authurl . '</a></p>';
    }

    function _twitter_accessToken($tmhOAuth) {
        $tmhOAuth->config['user_token'] = $_SESSION['oauth']['oauth_token'];
        $tmhOAuth->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];

        $code = $tmhOAuth->request(
                'POST', $tmhOAuth->url('oauth/access_token', ''), array(
            'oauth_verifier' => $this->_input['oauth_verifier']//$_REQUEST['oauth_verifier']
                )
        );
        if ($code == 200) {
            global $link;
            $data = $tmhOAuth->extract_params($tmhOAuth->response['response']);
//            pre($data, 1);
            $arr['user']['twitt_connect'] = $data['user_id'];
            $arr['user']['username'] = $data['screen_name'];
            $arr['user']['type'] = 'twitt_connect';
            $arr['user']['password'] = substr(md5(uniqid()), 0, 10);
            $this->_commonInsert($arr);
            exit;
        } else {
            outputError($tmhOAuth);
        }
    }

}


?>