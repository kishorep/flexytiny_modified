<?php

class student_manager extends mod_manager {

    /**
     * This is a constructor to initialized product module
     * 
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'student', $modTable = 'practice_student') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
	//check_session();
    }

    function get_module_name() {
	return 'student';
    }

    function get_manager_name() {
	return 'student';
    }

    function _default() {
	
    }
    //Function for drag & drop listing 
        
        function _student_reorder(){
                ob_clean();
                $res=explode(",",$this->_input['ids']);
                $fld = ($this->_input['fld'])?$this->_input['fld']:"id_".$this->_input['tbl'];
//                if($_SESSION['id_admin']){
//                        $sql = "UPDATE ".TABLE_PREFIX.$this->_input['clientname']."_".$this->_input['tbl']."  SET id_seq = CASE ".$fld;
//                }else{
                        $sql = "UPDATE ".TABLE_PREFIX.$this->_input['tbl']."  SET id_seq = CASE ".$fld;
//                }
                $i=1;
                foreach ($res as $key => $value) {
                    if($value){
                        $sum=$this->_input['qstart']+$i ;
                        $sql .= " WHEN ".$value." THEN ".$sum." ";
                        $ids .= $value.",";
                        $i++;
                    }
                }
                $sql .= "END WHERE ".$fld."  IN (".rtrim($ids,",").")";
                execute($sql,$err);
                exit;
        }
    
}