<?php

class admin_user extends user_manager {

	/**
	 * This is a constructor to initialized user admin module.This overrides user side constructor.
	 *
	 * @param object $smarty Reference of smarty object
	 *
	 * @param Array $_output Output query string
	 *
	 * @param Array $_input Input query string
	 */
	public function __construct(& $smarty, & $_output, & $_input) {
		if ($_SESSION['id_admin']) {
			parent::__construct($smarty, $_output, $_input);
		} else {
			redirect(LBL_ADMIN_SITE_URL);
		}
	}

	function _clear_all() {
		echo VAR_DIR . "cache<br>";
		echo VAR_DIR . "captcha_cache<br>";
		echo VAR_DIR . "templates_admin_c<br>";
		echo VAR_DIR . "templates_c<br>";
	}

	/**
	 * Function is used list all the user in the admin side.
	 * @method _listing()
	 * @param array $src This array contains data like fname,email,address etc.
	 * @param string $qst This contains the value of the qstart.
	 * @param string $del This contains a conditional value (0,1) during the delete case.
	 */
	function _listing($src = array(), $qst = 0, $del = '') {//echo 36;exit;
		$uri = PAGINATE_ADMIN_PATH . 'user/listing';
		$churi = "";
		$this->_input['fname'] = $this->_input['fname'] ? $this->_input['fname'] : $src['fname'];
		$this->_input['email'] = $this->_input['email'] ? $this->_input['email'] : $src['email'];
		$this->_input['addr'] = $this->_input['addr'] ? $this->_input['addr'] : $src['addr'];
		if ($this->_input['fname']) {
			$cond.=" AND (first_name LIKE '%" . $this->_input['fname'] . "%' OR last_name LIKE '%" . $this->_input['fname'] . "%')";
			$uri = $uri . "/fname/" . $this->_input['fname'];
			$churi = "/fname/" . $this->_input['fname'];
		}
		if ($this->_input['email']) {
			$cond.=" AND email LIKE '%" . $this->_input['email'] . "%'";
			$uri = $uri . "/email/" . $this->_input['email'];
			$churi = $churi . "/email/" . $this->_input['email'];
		}
		if ($this->_input['addr']) {
			$cond.=" AND address LIKE '%" . $this->_input['addr'] . "%'";
			$uri = $uri . "/addr/" . $this->_input['addr'];
			$churi = $churi . "/addr/" . $this->_input['addr'];
		}
		$this->_input['choice'] = 'listing';
		$src['cond'] = $src['cond'] ? $src['cond'] : $qst;
		$qstart = isset($this->_input['qstart']) ? $this->_input['qstart'] : $qst;
		$this->_output['hlink'][] = array("prompt" => "Uncheck All", "action" => "onclick=\"unCheckAll();\"");
		$this->_output['hlink'][] = array("prompt" => "Check All", "action" => "onclick=\"checkAll();\"");
		$this->_output['hlink'][] = array("prompt" => "Delete", "action" => "onclick=\"deleteUser();\"");
		//$this->_output['hlink'][] = array("prompt"=>"Add", "action"=>"onclick=\"addUser();\"");
		$this->_output['special'] = array("prompt" => "Check", "action" => "onclick=\"alert('hello');\"", "id" => "faq_code");
		$sql = get_search_sql("user", "random_num='0' AND username NOT IN('admin','developer')" . $cond);
		$this->_output['pg_header'] = "User Lists";
		$this->_output['limit'] = $GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] = $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "normal"; // extra, nextprev, advance, normal, box
		$this->_output['field'] = array("id_user" => array("User", 1), "first_name" => array("First Name", 1), "last_name" => array("Last Name", 1), "email" => array("Email", 1), "address" => array("Address", 1, "truncate:40"), "add_date" => array("Date Added", 1, "format" => "date_format:'%m-%d-%Y'"), 'flag' => array('Status', 1, 'anchor' => LBL_ADMIN_SITE_URL . "user/changeStatus" . $churi . "/id/", 'condition' => array('0' => 'Enable', '1' => 'Disable')));
		$this->_output['qstart'] = $qstart;
		$this->_output['sql'] = $sql;
		$this->_output['uri'] = urlencode($uri);
		$this->_output['sort_by'] = isset($this->_input['sort_by']) ? $this->_input['sort_by'] : "id_user";
		$this->_output['sort_order'] = "DESC";
		$this->_output['ajax'] = 1;
		$this->_output['links'][] = array("Action", "", "id_user", LBL_SITE_URL . "templates/css_theme/img/led-ico/pencil.png", "function" => "editUser");
		$this->_output['links'][] = array("", "", "id_user", LBL_SITE_URL . "templates/css_theme/img/led-ico/eye.png", "function" => "viewDetails");
		if (isset($this->_input['ed'])) {
			$this->blObj->page_listing($this, 'admin/user/search');
		} else if ($del) {
			$this->blObj->page_listing($this, 'admin/user/listing');
			//$this->blObj->page_listing($this);
		} else {
			if (($this->_input['u_search'] || isset($this->_input['qstart'])) && !($src['cond'])) {
				$this->blObj->page_listing($this, 'admin/user/listing'); // Pass the template name as a 2nd optional parameter.
				//$this->blObj->page_listing($this);
			} else {
				$this->blObj->page_listing($this, 'admin/user/search');
			}
		}
	}

	/**
	 * Function updates the sdtatus field of the user table from 0 to 1 and vice versa.
	 * This function is in the user listing enable or disable case through get_next_page().
	 * Which shows whether the user is blocked or not.
	 * @method _change_status()
	 * @return void Redirect to the listing function.
	 */
	function _changeStatus() {
		$x = array_slice(explode("/", $_SERVER['REQUEST_URI']), 6);
		$y = array_slice($x, 0, count($x) - 2);
		if ($this->_input['id']) {
			$sql = get_search_sql("user", "id_user = '" . $this->_input['id'] . "' LIMIT 1");
			$rs = getsingleindexrow($sql);
			if ($rs['flag'] == 1) {
				$flag = 0;
			} else {
				$flag = 1;
			}
			$err = $this->dataObj->updateStringFormat("flag", $flag, "id_user=" . $this->_input['id']);
			if ($err) {
				if ($rs['flag'] == 1) {
					$_SESSION['raise_message']['global'] = "You have blocked " . $rs['username'] . "";
				} else {
					$_SESSION['raise_message']['global'] = "You have unblocked " . $rs['username'] . "";
				}

				$this->_listing('', $this->_input['qstart']);
				// redirect(LBL_ADMIN_SITE_URL . "user/listing/qstart/" . $this->_input['qstart'] . "/ce/0/" . implode('/', $y));
			}
		}
	}

	/**
	 * Function is used to recover the password of the user.
	 * It sends a mail to the user which contains the infomation about the password.
	 * @method _recover_pass()
	 * @return message Password confirmation message if success else User account not exist message.
	 */
	/* function _recoverPass() {
	  ob_clean();
	  print "here";
	  print "<pre>";print_r($this->_input);exit;
	  $user_name = $this->_input['usr_name'];
	  //$arr = $this->dataObj->getForgotPwd($user_name, " AND id_admin != 0");
	  $cond = "email = '".$user_name."'  AND id_admin != 0";
	  $arr = recordExists("user", $cond);
	  $cnt = count($arr);
	  if ($cnt > 0) {
	  $info['email'] = $arr[0]['email'];
	  $info['password'] = $arr[0]['password'];
	  $info['username'] = $arr[0]['username'];

	  $to = $arr[0]['email'];
	  $from = $GLOBALS['conf']['SITE_ADMIN']['email'];
	  $subject = "Forgot password \n\n";

	  $tpl = "admin/user/forgot_password";
	  $this->smarty->assign('sm', $info);
	  $body = $this->smarty->fetch($this->smarty->add_theme_to_template($tpl));
	  $msg = sendmail($to, $subject, $body, $from); // also u can pass  $cc,$bcc
	  print getmessage('USER_PASS_CONF_MSG');
	  } else {
	  print getmessage('USER_NO_ACC');
	  }
	  exit;
	  } */

	/**
	 * Function is used for editing of the profile page of th admin.
	 * @method _profile()
	 * @return template Return the profile template with pre filled data.
	 */
	function _profile() {//echo 5687;exit;
		$sql = get_search_sql('user', "id_user='" . $_SESSION['id_user'] . "' LIMIT 1");
		$userdata = getsingleindexrow($sql);
		$this->_output['userdata'] = $userdata;
		$this->_output['tpl'] = 'admin/user/profile';
	}

	/**
	 * Function is udsed to update the profile of the admin.
	 * After successful updation a mail is sent.
	 * @method _update_profile()
	 * @global object $smarty It defines the smarty object.
	 */
	function _updateProfile() {
		$arr['email'] = $this->_input['email'];
		$arr['username'] = $this->_input['uname'];
		$arr['password'] = $this->_input['npass'] ? $this->_input['npass'] : $this->_input['oldpass'];
		global $smarty;
		if ($this->_input['opass'] == '') {
			$this->_input['opass'] = $this->_input['oldpass'];
			$this->_input['npass'] = $this->_input['oldpass'];
		}
		$sql_pass = get_search_sql("user", "id_user='" . $_SESSION['id_user'] . "' AND password='" . $this->_input['opass'] . "'");
		$pass_exist = getrows($sql_pass, $err);
		if (count($pass_exist) && $pass_exist[0]['password'] == $this->_input['opass']) {
			$ret = $this->dataObj->updateArrayFormat($arr, "id_user='" . $_SESSION['id_user'] . "'");
			if ($ret) {
				$_SESSION['email'] = $this->_input['email'];
				$_SESSION['username'] = $this->_input['uname'];
				$smarty->assign('username', $this->_input['uname']);
				$smarty->assign('password', $this->_input['npass']);
				$smarty->assign('email', $this->_input['email']);
				$body = $smarty->fetch('admin/user/profilemail.tpl.html');
				//$from = "test@test.com";
				//$to = "surya@mail.afixiindia.com";
				$from = $GLOBAL['conf']['SITE_ADMIN']['email'];
				$to = $this->_input['email'];
				$subject = "Updated Profile Information.";
				mail($to, $subject, $body, $headers);
				$this->_output['fail_msg'] = "<font color='red'>" . getmessage('USER_PROF_UPDATE_SUCC') . "</font>";
			}
		} else {
			$this->_output['fail_msg'] = "<font color='red'>" . getmessage('USER_CORRECT_PASS') . "</font>";
		}
		$this->_profile();
	}

	/**
	 * Function is used to check whether the user name is exists in the database or not.
	 * @method _check_username()
	 * @return int If user name exist then 2 else 1
	 */
	function _checkUsername() {
		ob_clean();
		$sql_uname = get_search_sql("user", " username='" . $this->_input['uname'] . "'");
		$uname_res = getrows($sql_uname, $err);
		if (count($uname_res)) {
			print 2;
		} else {
			print 1;
		}
	}

	/**
	 * Function is only take the user to the login_users template.
	 * @method _loginusers()
	 * @return template Return the login_users template.
	 */
	function _loginUsers() {
		$this->_output['qstart'] = $this->_input['qstart'];
		$this->_output['c'] = $this->_input['c'];
		$this->_output['delete_msg'] = $this->_input['d'];
		$this->_output['tpl'] = "admin/user/login_users";
	}

	/**
	 * Function is used to list all the user that are login to that site.
	 * @method _getuserlogin()
	 * @return template Return alphauserlogin template containing list of users.
	 */
	function _getUserLogin() {
		$uri = PAGINATE_ADMIN_PATH . 'user/getUserLogin/';
		$this->_output['pg_header'] = "Login User Details";
		if ($this->_input['sfld']) {
			$this->_output['sfld'] = $this->_input['sfld'];
			$uri .= "sfld/" . $this->_input['sfld'] . "/";
			$search['username'] = $this->_input['username'];
			$search['ip'] = $this->_input['ip'];
			$search['email'] = $this->_input['email'];
			$cond = '1';
			$url = '';
			foreach ($search as $f => $v) {
				if ($v) {
					$cond.=" AND $f='$v'";
					$url.="$f/$v/";
				}
			}
			$uri.=$url;
			$this->_output['url'] = $url;
			$sql = get_search_sql('login', $cond);
		} else {
			if ($this->_input['c'] != 'all') {
				$sql = get_search_sql('login', "1 AND username LIKE '" . $this->_input['c'] . "%'");
			} else {
				$sql = get_search_sql('login');
			}
			$uri .= "c/" . $this->_input['c'] . "/";
		}
		$this->_output['limit'] = $GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] = $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "normal"; // extra, nextprev, advance, normal, box
		$this->_output['field'] = array("id_user" => array("User", 1));
		$this->_output['qstart'] = $this->_input['qstart1'] ? $this->_input['qstart1'] : $this->_input['qstart'];
		$uri .="qstart/" . $this->_output['qstart'];
		$this->_output['sql'] = $sql;
		$this->_output['sort_by'] = "id_login";
		$this->_output['sort_order'] = "DESC";
		$this->_output['uri'] = $uri;
		$this->_output['ajax'] = 1;
		$this->_output['c'] = $this->_input['c'];
		$this->blObj->page_listing($this, 'admin/user/alphauserlogin');
	}

	/**
	 * Function is used to delete a record from the login table according to the $this->_input['loginid'] value.
	 * @method _deleteuserlogin()
	 * @return void Redirect to the $url constructed by condition.
	 */
	function _deleteUserLogin() {
		$this->_input['loginid'] = trim($this->_input['loginid'], ',');
		if ($this->_input['loginid']) {
			$this->dataObj->delete("id_login IN(" . $this->_input['loginid'] . ")", "login"); //CHANGED BY PRABHU
		} else {
			$this->_output['failmsg'] = "<center>" . getmessage('USER_CHECK_BOX') . ".</center>";
		}
		ob_clean();
		if ($this->_input['sfld']) {
			$url = LBL_ADMIN_SITE_URL . "user/getUserLogin/ce/0/qstart/" . $this->_input['qstart'] . "/sfld/1/" . $this->_input['uri'];
		} else {
			$url = LBL_ADMIN_SITE_URL . "user/getUserLogin/ce/0/qstart/" . $this->_input['qstart'] . "/c/" . $this->_input['c'];
		}
		redirect($url);
		exit;
	}

#######################################################
####################USER DETAILS ######################
#######################################################

	function _detailUser() {
		$cond = " 1 AND id_user = " . $this->_input['id_user'];
		$sql = get_search_sql("user", $cond);
		$res = getsingleindexrow($sql);
		$this->_output['res'] = $res;
		$this->_output['tpl'] = "admin/user/details";
	}

	/**
	 * Function is used to show the searchlogin page for searching.
	 * @method _search_tpl()
	 */
	function _searchTpl() {
		$this->_output['tpl'] = "admin/user/searchlogin";
	}

	/**
	 * Function is used to show the register template for registration.
	 * @method _register()
	 */
	function _register() {
		$this->_output['hobbies'] = $GLOBALS['conf']['HOBBIES'];
		$this->_output['gender'] = $GLOBALS['conf']['GENDER'];
		$this->_output['logUname'] = $GLOBALS['conf']['LOGIN']['name'];
		$this->_output['tpl'] = 'admin/user/register';
	}

	/**
	 * Function is used to insert user data in the database.
	 * @method _insert()
	 * @return void Redirect to the listing function.
	 */
	function _insert() {
		$user = $this->_input['user'];
		$name = $user['username'];
		$user['username'] = strtolower($user['username']);
		$user['gender'] = $this->_input['gender'];
		if ($this->_input['hobbies']) {
			$hobbies = implode(',', $this->_input['hobbies']);
			$user['hobbies'] = $hobbies;
		}
		$conf_pass = $this->_input['cpwd'];
		$user['dob'] = $this->_input['dob_Year'] . "-" . $this->_input['dob_Month'] . "-" . $this->_input['dob_Day'];
		if ($GLOBALS['conf']['PASSWORD']['type'] != 1) {
			$user['password'] = $GLOBALS['conf']['PASSWORD']['type']($user['password']);
		}
		$val = $this->dataObj->insertArrayFormat($user, "add_date,ip", "NOW(),'" . $_SERVER['REMOTE_ADDR'] . "'"); //insert($user);
		if ($val) {
			if ($GLOBALS['conf']['LOGIN']['name'] == 2) {
				$this->dataObj->updateStringFormat("username", $this->_input['user']['email'], "id_user=" . $this->_input['id']);
			}
			$_SESSION['raise_message']['global'] = getmessage('USER_REG_SUCC');
		} else {
			$_SESSION['raise_message']['global'] = getmessage('USER_REG_FAIL');
		}
		redirect(LBL_ADMIN_SITE_URL . "user/listing");
	}

	/**
	 * Function is used to edit the user record.
	 * This function is called from the listing page by ajax.
	 * @method _edit_user()
	 * @return template Return the register template with some fields pre filled like (hobbies,gender etc.)
	 */
	function _editUser() {
		check_session();
		$sql = get_search_sql("user", "id_user = '" . $this->_input['id_user'] . "' LIMIT 1");
		$result = getsingleindexrow($sql);

		$pos = strpos($result['hobbies'], ',');
		if ($pos) {
			if ($result['hobbies']) {
				$hobbies = explode(',', $result['hobbies']);
				$result['hobbies'] = $hobbies;
			}
		}
		$this->_output['hobbies'] = $GLOBALS['conf']['HOBBIES'];
		$this->_output['gender'] = $GLOBALS['conf']['GENDER'];
		//$this->_output['logUname'] = $GLOBALS['conf']['LOGIN']['name'];
		$this->_output['res'] = $result;
		$this->_output['qstart'] = $this->_input['qstart'];
		$this->_output['search_val'] = $this->_input['search_val'];
		$this->_output['flag'] = 1;
		$this->_output['tpl'] = 'admin/user/register';
	}

	/**
	 * Function is used update the user profile.
	 * The update operation is done by the function updateArrayFormat().
	 * Which is a function presented in the db.class .
	 * @method _update_user_profile()
	 * @author parwesh sabri
	 * @return void Return to the listing function.
	 */
	function _updateUserProfile() {//print "<pre>";print_r($this->_input);exit;
		check_session();
		$user = $this->_input['user'];
		if ($this->_input['hobbies']) {
			$hobbies = implode(',', $this->_input['hobbies']);
			$user['hobbies'] = $hobbies;
		} else {
			$user['hobbies'] = '';
		}
		$user['gender'] = $this->_input['gender'];
		$user['dob'] = $this->_input['dob_Year'] . "-" . $this->_input['dob_Month'] . "-" . $this->_input['dob_Day'];
		unset($user['username']);
		if ($this->_input['id_user']) {
			$this->dataObj->updateArrayFormat($user, "id_user=" . $this->_input['id_user']);
		}
		$_SESSION['raise_message']['global'] = getmessage('USER_UPDATE_SUCC');
		$this->_listing($this->_input['search'], $this->_input['qstart']);
	}

	/**
	 * Function is used to delete the users whose id is provided by the variable $this->_input['id_user'].
	 * Here we can delete multiple user.
	 * @method _deleteUser()
	 */
	function _deleteUser() {
		ob_clean();
		$this->_input['id_user'] = trim($this->_input['id_user'], ',');
		$id_user = "'" . str_replace(',', "','", $this->_input['id_user']) . "'";
		$this->dataObj->delete("id_user IN (" . $id_user . ")"); //deleteuser($id_user);
		$this->_listing($this->_input['search_val'], $this->_input['qstart'], 1);
	}
        
        function _admintime(){
          $this->_output['res'] = $this->_input['id_user'];
          $this->_output['tpl'] = 'admin/user/admin_time';  
        }
      
     function _makeAdmin(){
         $id_user=$this->_input['id_user'];
         $time=$this->_input['admin_time'];
         $dt=date("Y-m-d H:i:s", strtotime("+{$time} minutes"));
         $upsql="UPDATE ".TABLE_PREFIX."user SET id_admin=1,admin_time='{$dt}',flag=1 WHERE id_user={$this->_input['id_user']}";
         execute($upsql);
         $this->_input['u_search']="search";
         $this->_output['choice']='listing';
         $this->_listing();
     }

}

;
