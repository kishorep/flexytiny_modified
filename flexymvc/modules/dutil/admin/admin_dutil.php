<?php

class admin_dutil extends dutil_manager {

    private $file_name;

    public function __construct(& $smarty, & $_output, & $_input) {
	if ($_SESSION['id_admin']) {
	    parent::__construct($smarty, $_output, $_input);
	}else{
	    redirect(LBL_ADMIN_SITE_URL);
	}
    }

    function _show_db() {
	$sql = "SHOW  TABLE STATUS LIKE '" . TABLE_PREFIX . "%'";
	$row = getrows($sql, $err);
	foreach ($row as $k => $v) {
	    $arr[$k]['Name'] = $v['Name'];
	    $arr[$k]['Rows'] = $v['Rows'];
	    $arr[$k]['Space_used'] = round((($v['Data_length'] + $v['Index_length']) / 1024), 2);
	}
	$this->_output['arr'] = $arr;
	$this->_output['tpl'] = 'admin/dutil/database_show';
    }

    function _dump() {
	$save = $this->_input['save'];
	$table = $this->_input['table_db'];
	if ($this->_input['filename']) {
	    $this->file_name = $this->_input['filename'];
	} else {
	    $this->file_name = (count($table) == 1) ? $table[0] : TABLE_PREFIX;
	}
	$file_type_export = $this->_input['file_type_export'];
	$path = APP_ROOT . "image/db_dump";
	$this->delete($path);
	if ($file_type_export == "sql") {
	    $this->sql_create($table, $this->_input['sql_structure'], $this->_input['sql_data'], $save);
	} else if ($file_type_export == "csv") {
	    $this->csv_create($table, $this->_input['first_row'], $save);
	} else if ($file_type_export == "xls") {
	    $this->xls_create($table);
	} else if ($file_type_export == "pdf") {
	    $this->pdf_create($table);
	} else {
	    $this->xml_create($table, $save);
	}
    }

    function sql_create($table, $struct="", $tb_data="", $save) {
	$sql_str = '';
	$sql_version = $this->sql_verson();
	$sql_str.="-- phpMyAdmin SQL Dump \n";
	$sql_str.="-- version " . $sql_version . "\n";
	$sql_str.="-- http://www.phpmyadmin.net\n";
	$sql_str.="-- Host: localhost\n";
	$sql_str.="-- Generation Time: " . date('M j, Y  h:s A') . "\n";
	$sql_str.="-- Server version: 4.1.20\n";
	$sql_str.="-- PHP Version: " . phpversion() . "\n--\n";
	$sql_str.="-- Database: `" . DB_DB . "`\n--\n-- --------------------------------------------------------\n\n";
	$this->mysql_dump_table($table, $tb_data, $struct, $sql_str, $save);
	exit;
    }

    function mysql_dump_table($table, $tb_data='', $struct='', $sql_str, $save) {
	$h = fopen(APP_ROOT . "image/db_dump/" . $this->file_name . ".sql", "w+");
	fwrite($h, $sql_str);
	foreach ($table as $k => $v) {
	    if ($struct) {
		$sql = "show create table `" . $v . "`; ";
		$row = getsingleindexrow($sql);
		if ($row) {
		    fwrite($h, "-- Table structure for table `" . $v . "`\n--\n\n");
		    fwrite($h, $row['Create Table'] . ";\n\n");
		    if ($tb_data) {
			$sql_data_str = $this->mysql_dump_table_data($v);
			fwrite($h, $sql_data_str);
		    }
		}
	    } else {
		$sql_data_str = $this->mysql_dump_table_data($v);
		fwrite($h, $sql_data_str);
	    }
	}
	fclose($h);
	$dfile = APP_ROOT . "image/db_dump/" . $this->file_name . ".sql";
	if (file_exists($dfile)) {
	    if ($save == 'no') {
		ob_clean();
		header('Content-type: text/x-sql');
		header('Content-Disposition: attachment; filename=' . $this->file_name . ".sql");
		readfile($dfile);
		exit;
	    } else {
		$this->zip_file($save, "sql");
	    }
	} else {
	    $_SESSION['raise_message']['global'] = "Please Give Permission to DB_DUMP folder";
	    redirect(LBL_ADMIN_SITE_URL . "dutil/show_db");
	}
    }

    function mysql_dump_table_data($table) {
	global $link;
	$sql_return_data_str = '';
	$sql = "select * from " . $table; //." where id_user=3"; //BETWEEN 190 AND 195";
	$result = mysqli_query($link, $sql);
	$row_no = mysqli_num_rows($result);
	if ($row_no) {
	    $sql_return_data_str.="-- \n -- Dumping data for table `" . $table . "` \n--\n";
	    $no_fields = mysqli_num_fields($result);
	    for ($i = 0; $i < $no_fields; $i++) {
		$field_name_type = mysqli_fetch_field($result);
		$field_name[] = $field_name_type->name;
		$field_type[] = $field_name_type->type;
	    }
	    $field_str = '';
	    $field_str = stripslashes(implode('`,`', $field_name));
	    $ins_str = "insert into `" . $table . "` ( `" . $field_str . "` ) values\n ";
	    $c = 0;
	    $row_insert = 0;
	    while ($row = mysqli_fetch_row($result)) {
		$stmt = '';
		for ($i = 0; $i < $no_fields; $i++) {
		    if ($row[$i] == '0') {
			$stmt.=', 0';
		    } else if (!$row[$i]) {
			$stmt.=', NULL';
		    } else {
			if ($field_type[$i] == 'int') {
			    $stmt.=", " . $row[$i];
			} else {
			    $stmt.=", '" . preg_replace(array('/\r\n/'), '\r\n', addslashes($row[$i])) . "'";
			}
		    }
		}
		$c++;
		if (($row_no == $c) || (($c % 50) == 0)) {
		    $row_insert = $c;
		    $stmt.=" );\n";
		} else {
		    $stmt.=" ),\n";
		}
		$stmt = ltrim($stmt, ",");
		//$ins_str.="(".$stmt;
		if ($c == ($row_insert + 1)) {
		    $sql_return_data_str.=$ins_str;
		} else if ($c == 1) {
		    $sql_return_data_str.=$ins_str;
		}
		$sql_return_data_str.="(" . $stmt;
	    }
	}
	$sql_return_data_str.="\n\n";
	return $sql_return_data_str;
    }

    function csv_create($table, $first_row=" ", $save) {
	global $link;
	ob_clean();
	$csv = "";
	foreach ($table as $k => $v) {
	    $sql = "show columns from " . $v;
	    $result = mysqli_query($link, $sql);
	    $row_no = mysqli_num_rows($result);
	    $i = 0;
	    if ($row_no) {
		while ($row = mysqli_fetch_assoc($result)) {
		    if ($first_row) {
			$csv .= '"' . $row['Field'] . '";';
		    }
		    $i++;
		}
		if ($first_row) {
		    $csv .= "\n";
		}
		$csv .= $this->value_data($v, $i);
	    }
	}
	if ($save == 'no') {
	    ob_clean();
	    header("Content-type: application/vnd.ms-excel");
	    header("Content-disposition: csv" . date("Y-m-d") . ".csv");
	    header("Content-disposition: filename=" . $this->file_name . ".csv");
	    print $csv;
	    exit;
	} else {
	    $h = fopen(APP_ROOT . "image/db_dump/" . $this->file_name . ".csv", "w+");
	    fwrite($h, $csv);
	    fclose($h);
	    if (file_exists(APP_ROOT . "image/db_dump/" . $this->file_name . ".csv")) {
		$this->zip_file($save, "csv");
	    } else {
		$_SESSION['raise_message']['global'] = "Please Give Permission to DB_DUMP folder";
		redirect(LBL_ADMIN_SITE_URL . "dutil/show_db");
	    }
	}
    }

    function value_data($table, $count_row) {
	global $link;
	$csv_output = "";
	$sql = "SELECT * FROM " . $table;
	//print $sql;exit;
	$values = mysqli_query($link, $sql);
	while ($rowr = mysqli_fetch_row($values)) {
	    for ($i = 0; $i < $count_row; $i++) {
		$csv_output .='"' . preg_replace(array('/\r\n/'), '\r\n', addslashes($rowr[$i])) . '";';
	    }
	    $csv_output .= "\n";
	}
	return $csv_output;
    }

    function xml_create($table, $save) {
	global $link;
	$sql_version = $this->sql_verson();
	$h = fopen(APP_ROOT . "image/db_dump/" . $this->file_name . ".xml", "w+");
	fwrite($h, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
	fwrite($h, "<!--\n");
	fwrite($h, "-\n");
	fwrite($h, "-version " . $sql_version . "\n");
	fwrite($h, "-http://www.phpmyadmin.net\n");
	fwrite($h, "-\n");
	fwrite($h, "-Host: localhost\n");
	fwrite($h, "-Generation Time: " . date('M j, Y  h:s A') . "\n");
	fwrite($h, "-Server version: 4.1.20\n");
	fwrite($h, "-PHP Version: " . phpversion() . "\n");
	fwrite($h, "-->\n");
	fwrite($h, "\n");
	fwrite($h, "<!--\n");
	fwrite($h, "-Database: 'demo_afixiindia_com_-_db'-\n");
	fwrite($h, "-->\n\n");
	fwrite($h, "\t<" . DB_DB . ">\n");
	foreach ($table as $k => $v) {
	    $sql = "select * from " . $v;
	    $result = mysqli_query($link, $sql);
	    $row_no = mysqli_num_rows($result);
	    fwrite($h, "\t<!--Table " . $v . "-->\n");
	    while ($row = mysqli_fetch_assoc($result)) {
		fwrite($h, "\t\t<" . $v . ">\n");
		foreach ($row as $k1 => $val1) {
		    $find2 = array('/"/', '/</', '/>/', '/&/');
		    $replace2 = array('&quot;', '&lt;', '&gt;', '&amp;');
		    fwrite($h, "\t\t\t<" . $k1 . ">" . preg_replace($find2, $replace2, $val1) . "</" . $k1 . ">\n");
		}
		fwrite($h, "\t\t</" . $v . ">\n");
	    }
	}
	fwrite($h, "\t</" . DB_DB . ">\n");
	fclose($h);
	$dfile = APP_ROOT . "image/db_dump/" . $this->file_name . ".xml";
	if (file_exists($dfile)) {
	    if ($save == 'no') {
		ob_clean();
		header("Content-Type:text/xml");
		header('Content-Disposition: attachment; filename=' . $this->file_name . ".xml");
		readfile($dfile);
		exit;
	    } else {
		$this->zip_file($save, "xml");
	    }
	} else {
	    $_SESSION['raise_message']['global'] = "Please Give Permission to DB_DUMP folder";
	    redirect(LBL_ADMIN_SITE_URL . "dutil/show_db");
	}
    }

    function xls_create($table) {
	global $link;
	ob_clean();
	header("Pragma: public");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Disposition: attachment;filename=$this->file_name.xls ");
	header("Content-Transfer-Encoding: binary ");

	$this->xlsBOF();
	$cell_right = 0;
	foreach ($table as $k => $v) {
	    $sql = "show columns from " . $v;
	    $result = mysqli_query($link, $sql);
	    $row_no = mysqli_num_rows($result);
	    if ($row_no) {
		$cell_left = 0;
		while ($row = mysqli_fetch_assoc($result)) {
		    //$arr[]=$row['Field'];
		    $this->xlsWriteLabel($cell_right, $cell_left, $row['Field']);
		    $cell_left++;
		}
		$cell_right++;

		$sql1 = "SELECT * FROM " . $v;
		$values1 = mysqli_query($link, $sql1);
		while ($rowr = mysqli_fetch_row($values1)) {
		    for ($i = 0; $i < $cell_left; $i++) {
			$this->xlsWriteLabel($cell_right, $i, $rowr[$i]);
		    }
		    $cell_right++;
		}
		$cell_right++;
	    }
	}
	$this->xlsEOF();
	exit();
    }

    /* function pdf_create($table){
      $pdf = new PDF('L','pt','A3');
      $pdf->SetFont('Arial','',11.5);
      //$pdf->connect('192.168.1.117','parwesh','p455w0rd','account');
      $attr = array('titleFontSize'=>18, 'titleText'=>'this would be the title');
      foreach($table as $k => $v ){
      $pdf->mysql_report("SELECT * FROM  ".$v ,false,$attr);
      }
      $pdf->Output();
      } */

    /** 		Excel file function				  * */
    function xlsBOF() {
	echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
	return;
    }

    function xlsEOF() {
	echo pack("ss", 0x0A, 0x00);
	return;
    }

    function xlsWriteNumber($Row, $Col, $Value) {
	echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
	echo pack("d", $Value);
	return;
    }

    function xlsWriteLabel($Row, $Col, $Value) {
	$L = strlen($Value);
	echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
	echo $Value;
	return;
    }

    function zip_file($save, $extn) {
	chdir("..");
	chdir('image/db_dump');
	if ($save == 'tar') {
	    exec("tar -zcf " . $this->file_name . ".tar " . $this->file_name . "." . $extn);
	    $ext = ".tar";
	} else if ($save == 'bzip') {
	    exec("bzip2 -9  " . $this->file_name . "." . $extn);
	    $ext = "." . $extn . ".bz2";
	} else {
	    exec("zip -9 -r " . $this->file_name . ".zip " . $this->file_name . "." . $extn);
	    $ext = ".zip";
	}
	$this->download($ext);
    }

    function download($ext) {
	ob_clean();
	$dfile = APP_ROOT . "image/db_dump/" . $this->file_name . $ext;
	header("Content-type:application/force-download");
	header("Content-Disposition:attachment;filename=" . $this->file_name . $ext);
	readfile($dfile);
	exit;
    }

    function delete($path) {
	if ($handle = opendir($path)) {
	    while (false !== ($file = readdir($handle))) {
		if ($file != "." && $file != "..") {
		    unlink($path . "/" . $file);
		}
	    }
	    closedir($handle);
	}
    }

    function sql_verson() {
	$res = getsingleindexrow("Select version() as version");
	return $res['version'];
    }
    function _checkList(){
            $sql=get_search_sql("checkList");
            $res=getrows($sql, $err);
            $this->_output['res']=$res;
            $this->_output['tpl']="admin/dutil/checkList";

    }
    function _addCheckList(){
            $this->_output['tpl']="admin/dutil/addCheckList";
    }
    function _insertCheckList(){
            $checklist=$this->_input['checklist'];
            $id=$this->dataObj->insertArrayFormat($checklist, "add_date,ip", "NOW(),'" . IP . "'", "checkList");
            $_SESSION['raise_message']['global'] = "Inserted successfully.";
            redirect(LBL_ADMIN_SITE_URL."dutil/checkList");
    }
    function _editCheckList(){
            $sql=get_search_sql("checkList","id_checklist='".$this->_input['id']."' LIMIT 1");
            $res=getrows($sql,$err);
            $this->_output['res']=$res;
            $this->_output['tpl']="admin/dutil/addCheckList";
    }
    function _updateCheckList(){
            $checklist=$this->_input['checklist'];
            $id_checklist=$this->_input['id_checklist'];
            $this->dataObj->updateArrayFormat($checklist, "id_checklist =".$id_checklist ,"checkList");
            $_SESSION['raise_message']['global'] = "Updated successfully.";
            redirect(LBL_ADMIN_SITE_URL."dutil/checkList");
    }
    function _deleteCheckList(){
            $this->dataObj->delete("id_checklist=".$this->_input['id'], "checkList");
            $_SESSION['raise_message']['global'] = "Deleted successfully.";
            redirect(LBL_ADMIN_SITE_URL."dutil/checkList/ce/0");
            //$this->_checkList();
    }



	function _dbListing(){
		if ($handle = opendir(APP_ROOT.'/db_backup')) {
    while (false !== ($entry = readdir($handle))) {
		if($entry=='.svn' || $entry=='..' || $entry=='.' ){

		}else{
       $arr[]=$entry;
	$dt[$entry]=date ("d-m-Y H:i:s", filemtime(APP_ROOT . "db_backup/".$entry));
		}
    }
    closedir($handle);
}
		$file=trim($_SERVER['HTTP_HOST']."_".SUB_DIR,'/');
		$this->_output['fname']=$file.".".date("Y-m-d--H_i_s");
		$this->_output['res']=$arr;
		$this->_output['dt']=$dt;
		$this->_output['tpl']='admin/dutil/dblisting';
	}
	function _showdetails(){
	$nm=substr($this->_input['fname'],-3);
		$fpath = APP_ROOT . "db_backup/" . $this->_input['fname'];
		if ($nm=='sql'){
	if (@fopen($fpath, 'r')) {
	    $this->_output['file_content'] = file_get_contents($fpath);
	}
		}
	if($nm=='tar'||$nm=='zip'){
	ob_clean();
	$dfile = APP_ROOT . "db_backup/" . $this->_input['fname'];
	header("Content-type:application/force-download");
	header("Content-Disposition:attachment;filename=" .$this->_input['fname']);
	readfile($dfile);
	exit;
	}

	$this->_output['tpl']='admin/dutil/filedetails';
	}
	function _createfile() {
		$file = trim($_SERVER['HTTP_HOST'] . "_" . SUB_DIR, '/');
		$file1 = $file . "." . date("d-m-Y--H_i_s");
		$pr = TABLE_PREFIX;
		$user = DB_USER;
		$port = "3306";
		$host = DB_HOST;
		$pwd = DB_PASS;
		$db = DB_DB;

		$file = $this->_input['fname'] ? $this->_input['fname'] . ".sql" : $file1 . ".sql";
		$sfile = $file;
		if ($user && $host && $pwd && $db) {
			$ss = <<<EOF
echo "show tables like '$pr%';" | mysql    -u $user -h $host -P $port -p$pwd $db | grep -v 'Tables_in_' |xargs
EOF;

			$output2 = exec("{$ss}");
chdir("..");
	chdir('db_backup/');
			$ss1 = <<<EOF
mysqldump  -u $user -h $host -P $port -p$pwd $db $output2 > $sfile
EOF;

$output3 = exec("{$ss1}");

			// for tar the sql file
if($this->_input['filetype']=='.tar'){
	chdir("..");
	chdir('db_backup/');
	$file_name=$this->_input['fname']?$this->_input['fname']:$file1;
	$dfile= $file_name;
	chmod($file,0777);
	 exec("tar -zcf " . $dfile . ".tar " . $file );
	 exec("rm -rf ".$file);
}
// for zip the sql file
if($this->_input['filetype']=='.zip'){
	chdir("..");
	chdir('db_backup/');
	$file_name=$this->_input['fname']?$this->_input['fname']:$file1;
	$dfile=  $file_name;
	chmod($file,0777);
	exec("zip -9 -r " . $dfile . ".zip " . $file);
	exec("rm -rf ".$file);
}
	redirect(LBL_ADMIN_SITE_URL.'dutil/dbListing');

}
	}

};