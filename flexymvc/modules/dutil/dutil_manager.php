<?php

class dutil_manager extends mod_manager {

    /**
     * This is a constructor to initialized module module
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'dutil', $modTable = '') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
    }
    function get_module_name() {
	return 'dutil';
    }

    function get_manager_name() {
	return 'dutil';
    }

    function _default() {
	
    }

    function _unzip() {
	$file = $this->_input['file'];
	if (file_exists($file)) {
	    $ext = array_pop(explode(".", $file));
	    if (strtolower($ext) == "zip") {
		if (!$this->_input['r'])
		    echo "<font color='red'><b>" . getmessage('USER_UNZIP_MSG') . " " . $file . "</b>.</font>";
		$file = $this->_input['r'] ? $file : " -l " . $file;
		print "<pre>";
		passthru("unzip $file");
	    } else {
		echo "<font color='red'><b>" . $file . "</b>" . getmessage('USER_IS_VALID_ZIP') . ".</font>";
	    }
	} else {
	    echo "<font color='red'>The <b>$file</b> " . getmessage('USER_FILE_FOUND') . ".</font>";
	    print "<br><br><b>" . getmessage('USER_LIST_ZIP') . "</b><pre>";
	    passthru("ls -ll *.zip");
	}
    }

    function _untar() {
	$file = $this->_input['file'];
	if (file_exists($file)) {
	    $ext = array_pop(explode(".", $file));
	    if (strtolower($ext) == "tar") {
		if (!$this->_input['r'])
		    echo "<font color='red'><b>" . getmessage('USER_UNTAR_MSG') . " $file</b>.</font>";
		$file = $this->_input['r'] ? " -zxf " . $file : " -ztf " . $file;
		print "<pre>";
		passthru("tar $file");
	    } else {
		echo "<font color='red'><b>$file</b> " . getmessage('USER_IS_VALID_TAR') . "</font>";
	    }
	} else {
	    echo "<font color='red'>The <b>$file</b> " . getmessage('USER_FILE_FOUND') . ".</font>";
	    print "<br><br><b>" . getmessage('USER_LIST_TAR') . "</b><pre>";
	    passthru("ls -ll *.tar");
	}
    }

    function _phpinfo() {
	phpinfo();
    }

    /**
     * printsession() function to print the session.
     */
    function _printsession() {
	print "<pre>";
	print_r($_SESSION);
    }

    /**
     * unsetsession() function to unset the session variables.
     */
    function _unsetsession() {
	$unset_arr = $_SESSION['input'];
	if ($unset_arr != NULL) {
	    foreach ($unset_arr as $key => $value) {
		unset($_SESSION['input'][$key]);
	    }
	    unset($_SESSION['input']);
	}
    }

    /**
     * setsession() function to set the user variables in the session.
     */
    function _setsession() {
	$arr = $this->_input;
	foreach ($arr as $key => $value) {
	    if ($key != 'page' && $key != 'choice' && $key != 'dutil') {
		$_SESSION['input'][$key] = $value;
	    }
	}
    }
    
    function _fck(){
	$this->_output['tpl'] = "dutil/fck";
    }
	/* set in config
	 * [PING]
google = "http://www.google.com/webmasters/tools/ping?sitemap="
bing = "http://www.bing.com/webmaster/ping.aspx?siteMap="
ask = "http://submissions.ask.com/ping?sitemap="
yahoo = "http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid=YahooDemo&url="
 
	 */
	function _ping() {
	$sitemap_url = urlencode(LBL_SITE_URL . "sitemap.xml");
	$curl_req = array();
	$urls = array();
	// Finding SEs that we will be pining which is set dynamically.
	foreach ($GLOBALS['conf']['PING'] as $key => $value) {
	    $urls[] = $value . $sitemap_url;
	}

	foreach ($urls as $url) {
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURL_HTTP_VERSION_1_1, 1);
	    $curl_req[] = $curl;
	}
	//initiating multi handler
	$multiHandle = curl_multi_init();

	// adding all the single handler to a multi handler
	foreach ($curl_req as $key => $curl) {
	    curl_multi_add_handle($multiHandle, $curl);
	}
	do {
	    $multi_curl = curl_multi_exec($multiHandle, $isactive);
	} while ($isactive || $multi_curl == CURLM_CALL_MULTI_PERFORM);
	$success = true;
	foreach ($curl_req as $curlO) {
	    if (curl_errno($curlO) != CURLE_OK) {
		$success = false;
	    }
	}
	curl_multi_close($multiHandle);
	return $success;
    }

    /**
     * sitemap() function to set links in the sitemap template
     * 
     * 
     */
    function _sitemap(){
	$sqlcms = get_search_sql('content');
	$rescms = getrows($sqlcms,$err);
	foreach($rescms  as $k1=>$v1){
	    $cms[$k1]['code'] = $v1['cmscode'];
	    $cms[$k1]['lang'] = $v1['language'];
	}
	$this->_output['cms'] = $cms;
	header('Content-type: text/xml');
        $this->_output['tpl']='dutil/sitemap';
    }

};