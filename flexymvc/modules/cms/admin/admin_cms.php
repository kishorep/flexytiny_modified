<?php

class admin_cms extends cms_manager {

	/**
	 * This is a constructor to initialized cms admin module.This overrides user side constructor.
	 * @param object $smarty Reference of smarty object
	 * @param Array $_output Output query string
	 * @param Array $_input Input query string
	 */
	public function __construct(& $smarty, & $_output, & $_input) {
		if ($_SESSION['id_admin']) {
			parent::__construct($smarty, $_output, $_input);
		} else {
			redirect(LBL_ADMIN_SITE_URL);
		}
	}

	/**
	 * @method _list() list the content.
	 * @return template Listing template is shown with prefilled data.
	 */
	function _listing() {
              $uri = PAGINATE_ADMIN_PATH . "cms/listing";
		$cond = "1";
                if ($this->_input['psearch']) {
			parse_str($this->_input['searchval'],$data);
			$uri = $uri . "/psearch/1";
			if ($data['searchq']) {
				$cond .= " AND name LIKE '" .$data['searchq'] . "%'";
			}
			if ($data['category']) {
				$cond .= " AND cmscategory LIKE '" .$data['category'] . "%'";
			}
			if ($data['code']) {
				$cond .= " AND cmscode LIKE '" .$data['code'] . "%'";
			}
                }
		$cond .= " GROUP BY name  ORDER BY ctime DESC";
		$sql = get_search_sql('content', $cond, '*,GROUP_CONCAT(language) AS lang,GROUP_CONCAT(id_content) AS id,GROUP_CONCAT(cmscode) AS code');
		$this->_output['limit'] =10;//$GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] =3;// $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
                 $this->_output['uri'] = $uri;
		$this->_output['ajax'] = '1';
		$this->_output['qstart'] = $this->_input['qstart'];
		$this->_output['sql'] = $sql;
		$this->_output['uri'] = urlencode($uri);
		$this->_output['sort_by'] = "id_content";
		$this->_output['sort_order'] = "DESC";
		$this->_output['ajax'] = 1;

		if (($this->_input['insEditDel'] == 1) || ($this->_input['pg'] == 1)|| ($this->_input['psearch'] == 1)) {
			$this->blObj->page_listing($this, 'admin/cms/listing');
		} else {
			$this->blObj->page_listing($this, 'admin/cms/search');
		}
	}

	/**
	 * @method _auto_name() This function is used for the autocomplete purpose.
	 * @return template return auto_list template which is fall on the search text field;
	 */
	function _autoContentList() {
		ob_clean();
		$cond .= " name LIKE '%" . $this->_input['q'] . "%'" . " ORDER BY name ASC";
		$sql = get_search_sql("content", $cond, "DISTINCT name"); //print $sql;exit;
		$this->_output['data'] = getautorows($sql, "name", "");
		$this->_output['tpl'] = 'admin/cms/auto_list';
	}

	/**
	 ** @method _add() Show the add/edit form for content.
	 * @return template shows add.tpl.html if $this->_input['chk'] not set else add_form.tpl.html if $this->_input['chk'] is set.
	 */
	function _add() {
//		$this->_output['language'] = array_flip($GLOBALS['conf']['LANGUAGE']);
		$this->_output['sel_lang'] = isset($this->_input['language']) ? $this->_input['language'] : 'en';
//		$this->_output['mul_lang'] = $GLOBALS['conf']['MULTI_LANG']['islang'];
                  
		if ($this->_input['code']) {
			$sql = get_search_sql('content', "cmscode='" . $this->_input['code'] . "' LIMIT 1");
			$resdata = getsingleindexrow($sql);
			$this->_output['cms'] = $resdata;
			if (empty($this->_output['cms'])) {
				$sql_cms = get_search_sql('content', "cmscode='" . $this->_input['code'] . "' LIMIT 1", "name,cmscode,language");
				$this->_output['cms'] = getsingleindexrow($sql_cms);
			}
//		 $tagmap=  getindexrows(get_search_sql("tagmapping", "id_tour={$resdata['id_content']}"),"id","id_tag");
//				  if(!empty($tagmap)){
//				  $tagids=  implode(",", $tagmap);
//				  $tags=  getindexrows(get_search_sql("tags","id_tags IN ({$tagids})"),"id_tags","tags_name");
//				  $this->_output['tagshidval']=  implode(",", $tags);
//				  }
//				  $this->_output['tags']=$tags;
		$this->_output['selectedAttributeRes'] = getsingleindexrow(get_search_sql("attributedata", " 1 AND id_content=" . $resdata['id_content']));
		}
		$cmscategory=  getindexrows(get_search_sql("cmscategory"), "code","name");
//		$cmsexp=  getindexrows(get_search_sql("experiences"), "value","name");
		$attr = getrows(get_search_sql("attribute"), $err);
//		$this->_output['cmsexp'] =$cmsexp;
		$this->_output['attributeList'] =$attr;
		$this->_output['cmscategory'] = $cmscategory;
		$this->_output['qstart'] = $this->_input['qstart'];
                $this->_output['code']=array("301"=>301,"302"=>302);
		$this->_output['cms']['language'] = $this->_output['sel_lang'];
		$this->_output['tpl'] = (!$this->_input['chk']) ? 'admin/cms/add' : 'admin/cms/add_form';
	}

	function _checkName() {
		ob_clean();
		$cond = "name= '{$this->_input['name']}' and language= '{$this->_input['language']}' ";
		if ($this->_input['id_content']) {
			$cond .= " AND id_content != " . $this->_input['id_content'];
		}
		$sql = get_search_sql("content", $cond, "name");
		$res = getsingleindexrow($sql);

		if ($res) {
			print "1";
			exit;
		} else {
			print "0";
			exit;
		}
	}

	/**
	 ** @method _getPermalink() Get permalink for the content with duplicate checking for cmscode.
	 * @return string permalink as a string.
	 */
	function _getPermalink() {
		ob_clean();
		$cond = "1";
		if (!empty($this->_input['id_content']))
			$cond .= " AND id_content != " . $this->_input['id_content'];
		$cond .= " AND cmscode='" . $this->_input['permcode'] . "'";
		$sql = get_search_sql('content', $cond);
		$res = getsingleindexrow($sql, $err);
		if (empty($res))
			print convert_me(trim($this->_input['permcode']));
		else
			print 1;
		exit();
	}

	function _getCode() {
		ob_clean();
		print urlencode($this->_input['permcode']); //convert_me(trim($this->_input['permcode']));
	}

	/**
	 ** @method _insert() insert content.
	 * @return int Returns insert id if insertion is success else back to the add template.
	 */
	function _insert() {
//		$multiLang = $GLOBALS['conf']['MULTI_LANG']['islang'];
		$cms = $this->_input['cms'];
		$id_con = $this->_input['id_content'];
		if ($this->_input['description']) {
			$cms['description'] = $this->_input['description'];
		        }
                      if($this->_input['id_content']){
                        $attrFormData['id_content']=$this->_input['id_content'];  
                       $extra = "ip='" . IP . "',last_update_time = NOW()";
			$this->dataObj->updateArrayFormat($cms, "id_content = " . $id_con, "content", FALSE, $extra);
			if ($this->_input['prev_name'] != $cms['name']) {
			$this->dataObj->updateStringFormat('name', $cms['name'], "name = '" . $this->_input['prev_name'] . "'", "content", '', 1);
			}
                       }else{
			$ext_field = "ctime,ip";
			$ext_value = "NOW(),'" . IP . "'";
			$id_con = $this->dataObj->insertArrayFormat($cms, $ext_field, $ext_value);
                        $attrFormData['id_content'] = $id_con;
                       }
                        
              $attr=$this->_input['attrdata'];
      foreach ($attr as $key => $value) {
        if (is_array($value)) { //Here goes for board or university type : varchar
          if ($this->_input["boardOrUnivFlg_{$key}"]) {
            $attrFormData[$key] = implode(",", array_unique($value));
          } else {
            $attrList = 0;
            foreach ($value as $key1 => $value1) {
              $attrList = $attrList + $value1;
            }
            $attrFormData[$key] = $attrList;
            $attrList = 0;
          }
        } else {
          $attrFormData[$key] = $attr[$key];
          if (!empty($this->_input['dt'])) {
            //Converting date format YYYY-mm-dd to dd-mm-YYYY.
            foreach ($this->_input['dt'] as $datekey => $datevalue) {
              if ($key == $datekey) {
                $attrFormData[$key] = convertodate($attr[$key], 'dd-mm-yy', 'yy-mm-dd');
              }
            }
          }
        }
}
             if($this->_input['idattr']){
                  $this->dataObj->updateArrayFormat($attrFormData, "id={$this->_input['idattr']} AND id_content = " . $id_con, "attributedata");
             }else{
                    $idattr = $this->dataObj->insertArrayFormat($attrFormData,"","","attributedata");
                  }
                $this->_input['choice']="listing";
		$this->_input['insEditDel']=1;
		$this->_listing();
	}

	/**
	 ** @method _update() update content also insert a record into the content_archieve table.
	 * @return void
	 */
	function _update() {
		$multiLang = $GLOBALS['conf']['MULTI_LANG']['islang'];
		$id_con = $this->_input['id_content'];
		$permcode = $this->_input['permcode'];
		$lang = $this->_input['cms']['language'];
		$cms = $this->_input['cms'];
		if ($this->_input['description']) {
			$cms['description'] = $this->_input['description'];
		}

		$con_cond = "name='" . $cms['name'] . "' AND language='" . $cms['language'] . "' AND id_content != " . $id_con;
		$res = recordExists("content", $con_cond, "name");
		if (empty($res)) {
			/* if ($GLOBALS['conf']['AUDIT']['status']) {
			  $sql	    = get_search_sql('content', "id_content=" . $id_con . " LIMIT 1");
			  $old_res    = getsingleindexrow($sql);
			  $this->dataObj->insertArrayFormat($old_res, "archieve_time", "NOW()", "content_archieve");
			  } */

			$extra = "ip='" . IP . "',last_update_time = NOW()";
			$this->dataObj->updateArrayFormat($cms, "id_content = " . $id_con, "content", FALSE, $extra);
			if ($this->_input['prev_name'] != $cms['name']) {
				$this->dataObj->updateStringFormat('name', $cms['name'], "name = '" . $this->_input['prev_name'] . "'", "content", '', 1);
			}
                        
                        
		$attr=$this->_input['attrdata'];
		$attrFormData['id_content'] = $id_con;
      foreach ($attr as $key => $value) {
        if (is_array($value)) { //Here goes for board or university type : varchar
          if ($this->_input["boardOrUnivFlg_{$key}"]) {
            $attrFormData[$key] = implode(",", array_unique($value));
          } else {
            $attrList = 0;
            foreach ($value as $key1 => $value1) {
              $attrList = $attrList + $value1;
            }
            $attrFormData[$key] = $attrList;
            $attrList = 0;
          }
        } else {
          $attrFormData[$key] = $attr[$key];
          if (!empty($this->_input['dt'])) {
            //Converting date format YYYY-mm-dd to dd-mm-YYYY.
            foreach ($this->_input['dt'] as $datekey => $datevalue) {
              if ($key == $datekey) {
                $attrFormData[$key] = convertodate($attr[$key], 'dd-mm-yy', 'yy-mm-dd');
              }
            }
          }
        }
      }
	  if($this->_input['idattr']){
	  $this->dataObj->updateArrayFormat($attrFormData, "id={$this->_input['idattr']} AND id_content = " . $id_con, "attributedata");
	  }else{
		  $idattr = $this->dataObj->insertArrayFormat($attrFormData,"","","attributedata");
	  }
          
			$this->_updateMenu($id_con, $permcode, $lang);
			if ($this->_input['save_exit'] == '1') {
				$_SESSION['raise_message']['global'] = getmessage(CMS_UPDATE);
				redirect(LBL_ADMIN_SITE_URL . "cms/add/tname/{$cms['name']}/language/{$cms['language']}/code/{$cms['cmscode']}");
			} else {
				$_SESSION['raise_message']['global'] = getmessage(CMS_UPDATE);
				redirect(LBL_ADMIN_SITE_URL . "cms/listing");
			}
			if ($multiLang == 1) {
				$this->_output['language'] = array_flip($GLOBALS['conf']['LANGUAGE']);
				$this->_output['sel_lang'] = isset($cms['language']) ? $cms['language'] : 'en';
				$this->_output['mul_lang'] = $GLOBALS['conf']['MULTI_LANG']['islang'];

				$sql = get_search_sql('content', "cmscode='" . $cms['cmscode'] . "' AND language = '" . $this->_output['sel_lang'] . "' LIMIT 1");
				$this->_output['cms'] = getsingleindexrow($sql);

				$this->_output['tpl'] = 'admin/cms/add_form';
			} else {
				$_SESSION['raise_message']['global'] = getmessage(CMS_UPDATE);
				redirect(LBL_ADMIN_SITE_URL . "cms/listing");
			}
		} else {
			$cms['cmscode'] = '';
			$this->_output['cms'] = $this->_input['cms'];
			$this->_output['language'] = $GLOBALS['conf']['LANGUAGE'];
			$this->_output['sel_lang'] = $this->_input['language'];
			$val = array_flip($this->_output['language']);
			$this->_output['message'] = getmessage('CMS_LANG_1') . " " . $val[$this->_input['cms']['language']] . " " . getmessage('CMS_LANG_2'); //language  already exist";
			$this->_output['tpl'] = 'admin/cms/add';
		}
	}

	function _updateMenu($id, $permcode, $lang) {
		$path = LBL_SITE_URL . $lang . '/' . $permcode . '.html';
		$menu['path'] = $path;
		$this->dataObj->updateArrayFormat($menu, "id_content = " . $id, "menu");
	}

	/**
	 ** @method _delete() delete content.
	 * @return void redirect to list function after deletion.
	 */
	function _delete() {
//		$qstring = "";
//		if (!empty($this->_input['searchq']))
//			$qstring .= "?searchq=" . $this->_input['searchq'];
//		if (!empty($this->_input['stype'][0])) {
//			$qstring .= "&stype[]=" . $this->_input['stype'][0];
//		}
//		if (!empty($this->_input['stype'][1])) {
//			$qstring .= "&stype[]=" . $this->_input['stype'][1];
//		}
//		if ($GLOBALS['conf']['AUDIT']['status']) {
//			$sql = get_search_sql('content', "cmscode='" . addslashes($this->_input['code']) . "'");
//			$old_res = getsingleindexrow($sql);
//			$this->dataObj->insertArrayFormat($old_res, "archieve_time,is_deleted", "NOW(),1", "content_archieve");
//		}
//		$arr = explode(",", $this->_input['code']);
//		foreach ($arr as $key => $value) {
//			$this->dataObj->delete("cmscode='" . addslashes($value) . "'");
//		}
//		$arrid = explode(",", $this->_input['id']);
//		foreach ($arrid as $key => $value) {
//			$this->dataObj->delete("id_content=$value", "menu");
//		}
//		$_SESSION['raise_message']['global'] = getmessage('CMS_DELETE');
//		redirect(LBL_ADMIN_SITE_URL . 'cms/listing' . $qstring);
                $del=$this->dataObj->delete("id_content={$this->_input['id_content']}", "content");
                $del2=$this->dataObj->delete("id_content={$this->_input['id_content']}", "attributedata");
		$this->_input['choice']="listing";
		$this->_input['insEditDel']=1;
		$this->_listing();
            
	}
        
        
        function _category_listing() {
		$uri = PAGINATE_ADMIN_PATH . "cms/category_listing";
		$cond = 1;
		if ($this->_input['psearch']) {
			parse_str($this->_input['searchval'],$data);
			$uri = $uri . "/psearch/1";
			if ($data['cat_name']) {
				$cond .= " AND name LIKE '" .$data['cat_name'] . "%'";
			}
		}

		
			$sql = get_search_sql("cmscategory",$cond);
		// AND p.image_type !=2  this part is added by prabhu
		$this->_output['limit'] =10;//$GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] =3;// $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
                 $this->_output['uri'] = $uri;
		$this->_output['ajax'] = '1';
		$this->_output['qstart'] = $this->_input['qstart'];
		$this->_output['sql'] = $sql;
		$this->_output['uri'] = urlencode($uri);
		$this->_output['sort_by'] = "id_cmscategory";
		$this->_output['sort_order'] = "DESC";
		$this->_output['ajax'] = 1;

		if (!(($this->_input['insEditDel'] == 1) || ($this->_input['pg'] == 1) || ($this->_input['psearch'] == 1))) {
			$this->blObj->page_listing($this, 'admin/cms/search_category');
		} else {
			$this->blObj->page_listing($this, 'admin/cms/list_category');
		}
	}
	
	function _addCategory(){
		if($this->_input['id_cat']){
			$res=  recordExists("cmscategory", "id_cmscategory={$this->_input['id_cat']}");
		$this->_output['res'] =$res;
		}
		$this->_output['tpl'] ='admin/cms/add_category';
	}
	function _insertCategory(){
		$category=$this->_input['cat'];
		if($this->_input['id_cat']){
		$this->dataObj->updateArrayFormat($category, "id_cmscategory = " . $this->_input['id_cat'], "cmscategory");	
		}else{
		$idcat=$this->dataObj->insertArrayFormat($category, "", "", "cmscategory");
		}
		$this->_input['choice']="category_listing";
		$this->_input['insEditDel']=1;
		$this->_category_listing();
	}
	
	function _deletecategory(){
                $res = recordExists("content", "cmscategory='".$this->_input['code']."'");//pr($res);//exit;
                if(!empty($res)){
                echo 1;
                }else{
                $this->dataObj->delete("id_cmscategory={$this->_input['id_cat']}", "cmscategory");
		$this->_input['choice']="category_listing";
		$this->_input['insEditDel']=1;
		$this->_category_listing();
                }
            }
            
            function _search_catagory(){
                ob_clean();
             $namesql = get_search_sql("cmscategory","1  GROUP BY name ORDER BY name asc","name");
             $namesql = getrows($namesql);
             $str="<option value=''>Choose One</option>";
             foreach ($namesql as $key => $value) {
             $str.= "<option value=" ."'". $value['name'] ."'". ">" . $value['name'] . "</option>";
            }
                echo $str;
                exit;
            }
                    
        function _uniquename(){
            ob_clean();
            if(empty($this->_input['id'])){
                $res = recordExists("cmscategory", "name='".$this->_input['nameval']."'");
            }else{
                $res = recordExists("cmscategory", "name='".$this->_input['nameval']."' AND id_cmscategory!=".$this->_input['id']);
            }
            if(!empty($res)){
                echo 1;
            }
        }
        function _uniquecode(){
            ob_clean();
            if(empty($this->_input['id'])){
                $res = recordExists("cmscategory", "code='".$this->_input['codeval']."'");
            }else{
                $res = recordExists("cmscategory", "code='".$this->_input['codeval']."' AND id_cmscategory!=".$this->_input['id']);
            }
            if(!empty($res)){
                echo 1;
            }
        }
        
        function _search_cmscontent(){
            ob_clean();
             $namesql = get_search_sql("content","1  GROUP BY name ORDER BY name asc","name");
             $names = getrows($namesql);
           $str="<option value=''>Choose One</option>";
        foreach ($names as $key => $value) {
           $str.= "<option value=" ."'". $value['name'] ."'". ">" . $value['name'] . "</option>";
        }
        
         $lang=array_flip($GLOBALS['conf']['LANGUAGE']);
          $str1="<option value=''>Choose One</option>";
        foreach ($lang as $key1 => $value1) {
           $str1.= "<option value=" ."'".$key1 ."'". ">" .$value1 . "</option>";
        }
        
        $catagory=getindexrows(get_search_sql("cmscategory"),"code","name");
        $str2="<option value=''>Choose One</option>";
        foreach ($catagory as $key2 => $value2) {
           $str2.= "<option value=" ."'".$key2 ."'". ">" .$value2 . "</option>";
        }
        echo $str.'@@'.$str1.'@@'.$str2;
        exit;
        }
        
        function _update_cms_seq(){
            $all = $this->_input['all_list'];
           foreach ($all as $k => $v) {
           $this->dataObj->updateStringFormat($this->_input['seq_field'], $k + 1, $this->_input['cond_field'] . " = {$v}",$this->_input['tbl']);
              }
        }
		
		function _getCodeexist(){
			ob_clean();
			if($this->_input['id_cnt']){
			$res=  recordExists("content","id_content !={$this->_input['id_cnt']} AND name='{$this->_input['tname']}' AND cmscode='{$this->_input['permcode']}' AND cmscategory='{$this->_input['cmscat']}'");
			}else{
			$res=  recordExists("content","name='{$this->_input['tname']}' AND cmscode='{$this->_input['permcode']}' AND cmscategory='{$this->_input['cmscat']}'");
			}
			if(empty($res)){
				echo 1;exit;
			}
			else{
				echo 2;exit;
			}
		}

}

;
