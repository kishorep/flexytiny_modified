<?php

class cms_manager extends mod_manager {

    /**
     * 
     * This is a constructor to initialized cms module
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'cms', $modTable = 'content') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
    }
    function get_module_name() {
	return 'cms';
    }

    function get_manager_name() {
	return 'cms';
    }

    function _default() {
	return $this->_show();
    }

######################################################
######### _show() : ##################################
######################################################

    function _show() {
	global $smarty;
	$code = $this->arg['code'] ? $this->arg['code'] : ($this->_input['code'] ? $this->_input['code'] : '');
	$cmscategory = $this->arg['cmscode'] ? $this->arg['cmscode'] : ($this->_input['cmscode'] ? $this->_input['cmscode'] : '');
	if(!empty ($cmscategory)){
	$cond = "cmscode = '" . strip_tags(addslashes($code)) . "' AND cmscategory = '" . strip_tags(addslashes($cmscategory)) . "'";
        } else {
        $cond = "cmscode = '" . strip_tags(addslashes($code)) . "' AND cmscategory IS NULL";
        }
        $sql = get_search_sql('content',  $cond." LIMIT 1",'targetlink');
        $res = getsingleindexrow($sql, $err);
        if(empty ($res[targetlink])){
            if(!empty ($code)){
	    if (file_exists(APP_ROOT . 'templates/static/' . addslashes($code) . 'tpl.html')) {
		$this->_output['tpl'] = 'static/'.$this->_input['code'];
	    } else {
//                if(!empty ($cmscategory)){
//		$cond = "cmscode = '" . strip_tags(addslashes($code)) . "' AND cmscategory = '" . strip_tags(addslashes($cmscategory)) . "'";
//                } else {
//                $cond = "cmscode = '" . strip_tags(addslashes($code)) . "' AND cmscategory IS NULL";
//                }
//		$ln   = $this->arg['ln'] ? $this->arg['ln'] : ($this->_input['ln'] ? $this->_input['ln'] : 'en');
		$ln   = 'en';
		if(!empty ($ln))
		    $cond .= " AND language='" . $ln . "'";
			
		$sql = get_search_sql('content',  $cond." LIMIT 1");
		$res = getsingleindexrow($sql, $err);
		if(!empty ($res)){
		$sqlattr = get_search_sql('attributedata', "id_content={$res['id_content']}");
		$resattr = getsingleindexrow($sqlattr, $err);
		$resattrnm = getsingleindexrow(get_search_sql("attribute"), $err);
		$resattr['c15']=number_format($resattr['c15']);
		}

		if(!empty ($res)){
		    $smarty->assign('title',$res['title']);
		    $smarty->assign('meta_keywords',$res['meta_keywords']);
		    $smarty->assign('meta_description',$res['meta_description']);
		    $this->_output['attrdata'] = $resattr;
		    $this->_output['data'] = $res;
		    $this->_output['tpl'] = 'cms/show_content';
		}  else {
		    $_SESSION['raise_message']['global'] = getmessage('CMS_CORRECT_CODE');
		    redirect(LBL_SITE_URL);
		}
	    }
	}else{
	    $_SESSION['raise_message']['global'] = getmessage('CMS_NO_CODE');
	    redirect(LBL_SITE_URL);
	}
        }else{
            redirect($res[targetlink]);
        }
        
    }

};