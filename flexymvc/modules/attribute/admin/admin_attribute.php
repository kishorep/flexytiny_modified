<?php

class admin_attribute extends attribute_manager {

    /**
     * This is a constructor to initialized product admin module.This overrides user side constructor.
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input) {
        if ($_SESSION['id_admin']) {
            parent::__construct($smarty, $_output, $_input);
        } else {
            redirect(LBL_ADMIN_SITE_URL);
        }
    }
    function _listing() {
	$uri = PAGINATE_ADMIN_PATH . "attribute/listing";
	$cond = 1;
        if ($this->_input['psearch']) {
            $uri = $uri . "/psearch/1";
            if ($this->_input['pname'] != '') {
                $cond .= " AND attribute_name LIKE '%" . $this->_input['pname'] . "%'";
                $uri = $uri . "/pname/" . $this->_input['pname'];
            }
            if ($this->_input['etype'] != '') {
                $cond .= " AND entry_type LIKE '%" . $this->_input['etype'] . "%'";
                $uri = $uri . "/etype/" . $this->_input['etype'];
            }
            if ($this->_input['stype'] != '') {
                $cond .= " AND search_type LIKE '%" . $this->_input['stype'] . "%'";
                $uri = $uri . "/stype/" . $this->_input['stype'];
            }
        }	
	$attr = $GLOBALS['conf']['ATTRIBUTE_TYPE'];
	$attr = array_flip($attr);
	$sql = get_search_sql("attribute",$cond);
	$this->_output['course'] = getindexrows(get_search_sql('course', 1), 'id_course', 'course_name', $err);
	$this->_output['field'] = array("id_attribute" => array("id_attribute", 1));
        $this->_output['attr_name']=  getindexrows(get_search_sql("attribute",1,"id_attribute,attribute_name"),"id_attribute","attribute_name");
	$this->_output['sort_by'] = "id_attribute";
        $this->_output['sort_order'] = "DESC";
        $this->_output['limit'] = $GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
        $this->_output['show'] = $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
        $this->_output['sql'] = $sql;
        $this->_output['ajax'] = 1;
        $this->_output['type'] = "box"; // extra, nextprev, advance, normal, box	
	$this->_output['attribute'] =  $attr;
        $this->_output['uri'] = urlencode($uri);

	if(!(($this->_input['insEditDel'] == 1) || ($this->_input['psearch'] == 1) || ($this->_input['pg'] == 1))){
	    $this->blObj->page_listing($this, 'admin/attribute/search');
	}else{
	    $this->blObj->page_listing($this, 'admin/attribute/listing');
	}
           
    }
    /*
     * @method _autoSearchField Used for autocomplete
     * @ argument as $this->_input['q'] as the search field
     * @return
     */
//    function _autoSearchField() {
//        ob_clean();
//        $cond = $this->_input['fieldName'] . " LIKE '%" . $this->_input['q'] . "%' ORDER BY " . $this->_input['fieldName'] . " ASC";
//        $this->_output['data'] = getautorows(get_search_sql($this->_input['tblName'], $cond, " DISTINCT " . $this->_input['fieldName']), $this->_input['fieldName'], "");
//        $this->_output['tpl'] = 'admin/attribute/autoSearch';
//    }
    /*
     * @method _checkAttribute Used for duplicacy check in attribute name
     */
    function _checkAttribute() {
        if ($this->_input['id_attribute']) {
            $cond = "id_attribute != " . $this->_input['id_attribute'] . " AND attribute_name ='" . $this->_input['attributename'] . "'";
        } else {
            $cond = "attribute_name ='" . $this->_input['attributename'] . "'";
        }
        $attributename = recordExists("attribute", $cond, "*", 1);
        if ($attributename['attribute_name']) {
            print 1;
        }
    }
    
    function _unique_name(){
        ob_clean();
        if($this->_input['id']){
             $res=  recordExists('attribute',"attribute_name='{$this->_input['name']}' AND id_attribute!='{$this->_input['id']}'");
        }else{
            $res=  recordExists('attribute',"attribute_name='{$this->_input['name']}'");
        }
         if(empty($res)){
             echo 1;exit;
         }else{
             echo 2;exit;
         }
    }
    function _addAttribute() {
        ob_clean();
	$attr = $GLOBALS['conf']['ATTRIBUTE_TYPE'];
	$attr = array_flip($attr);
	 if ($this->_input['id_attribute']) {
            $this->_output['res'] = recordExists("attribute", "id_attribute='" . $this->_input['id_attribute'] . "'");
	    $this->_output['course'] = getindexrows(get_search_sql('course', '1'), 'id_course', 'course_name', $err);
            $this->_output['referer'] = $this->_input['uri'];
        }
	$this->_output['qstart'] = $this->_input['qstart'];
	$this->_output['attr'] =  $attr;
        $this->_output['tpl'] = 'admin/attribute/add';
    }
    /*
     * @method _deleteAttribute Used for deletion of attribute
     */
    function _deleteAttribute() {
	$delVal = $this->dataObj->delete("id_attribute = " . $this->_input['id_attribute']);
	if ($delVal) {
	    $this->_output['qstart'] = $this->_input['qstart'];
	    $this->_input['insEditDel'] = 1;
	    $this->_input['choice'] = 'listing';
	    $this->_input['uri'] = urlencode($this->_input['uri']);
	    if($this->_input['pname'] || $this->_input['etype'] || $this->_input['stype'] ){
		$this->_input['psearch'] = 1;
	    }
	    $this->_deleteAttrFromTable($this->_input['id_attribute']);
	    $this->_listing();
	}
    }
    /*
     * @method _autoSearchkey Used for multi-autocomplete
     * @ argument as $_REQUEST['tag'] as the search field
     * @return type json formatted name and value pair
     */
    function _autoSearchkey(){
	ob_clean();
	if($_REQUEST['course'] == 'null'){
	    $sql = get_search_sql("course","1 AND course_name LIKE '%".trim($_REQUEST['tag'])."%'","course_name as name, id_course as value");
	}else{
	    $sql = get_search_sql('course', ' course_name LIKE "%'.trim($_REQUEST['tag']).'%" AND id_course NOT IN ('.$_REQUEST['course'].')',"course_name as name, id_course as value");
	}
        $items = getrows($sql, $err);
        print json_encode($items);
        exit;
	
    }
    function _insertAttribute(){
//	print "<pre>";print_r($this->_input);
	// coresponding value of attribute_label saved in binary format as like 1,2,4,8,..
	if($this->_input['attribute']['attribute_label']){
	    $this->_input['attribute']['attribute_label'] = trim($this->_input['attribute']['attribute_label'],',');
	    $this->_input['attribute']['attribute_label'] = explode(",", $this->_input['attribute']['attribute_label']);
	    for ($x=0; $x <= count($this->_input['attribute']['attribute_label']); $x++) {
		if($this->_input['attribute']['attribute_label'][$x]){
		    $attr_val .= pow(2,$x).",";
		}
	    }
	    $attr_val = trim($attr_val,",");
	    $this->_input['attribute']['attribute_values'] = $attr_val;
	    $this->_input['attribute']['attribute_label'] = implode(",",$this->_input['attribute']['attribute_label']);
	}
	// to arrange id_courses in to comma(,) separated format
//	if($this->_input['attribute']['id_course']){
//	    $this->_input['attribute']['id_course'] = array_unique($this->_input['attribute']['id_course']);
//	    $this->_input['attribute']['id_course'] = implode(",",$this->_input['attribute']['id_course']);
//	}
	// this is for checking, when attribute type is text, by defult not display in search case
	if(($this->_input['attribute']['entry_type'] == $GLOBALS['conf']['ATTRIBUTE_TYPE']['text'] && $this->_input['attribute']['search_type'] == $GLOBALS['conf']['ATTRIBUTE_TYPE']['text']) || isset($this->_input['attribute']['datatype'])){
	    // to remove the text_values(<,>,...), when string type is selected.
	    if($this->_input['attribute']['datatype']==2 && isset($this->_input['attribute']['text_values'])){ // for string case
		$this->_input['attribute']['text_values']='';
	    }
	    $this->_input['attribute']['is_searchable'] = '0';
	}
	$attribute = $this->_input['attribute'];
	//print_r($attribute);print "</pre>";exit;
	$max_idSeq = getsingleindexrow(get_search_sql("attribute","1","MAX(id_seq)+1 AS id_seq"));
	$attribute['id_seq'] = $max_idSeq['id_seq'] ? $max_idSeq['id_seq'] : 1;
	$id_attribute = $this->dataObj->insertArrayFormat($attribute,"add_date,ip", "NOW(),'" . IP . "'","attribute");
	$this->_input['choice'] = 'listing';
	$this->_input['insEditDel'] = 1;
	if($this->_input['pname'] || $this->_input['ctype'] || $this->_input['stype']){
	    $this->_input['psearch'] = 1;
	}
//	$this->_addAttrToTables($id_attribute, $this->_input['attribute']['id_course']);
	$this->_addAttrToTables($id_attribute);
	$this->_listing();
    }
    function _updateAttribute() {
        ob_clean();
	//print "<pre>";print_r($this->_input);
	// this is for checking, when attribute type other than text, but the text related field has the values 
	if($this->_input['attribute']['entry_type']!=$GLOBALS['conf']['ATTRIBUTE_TYPE']['text']){
	    if(isset($this->_input['attribute']['text_values']))    // >,<,=,... 
		$this->_input['attribute']['text_values']='';
	    if(isset($this->_input['attribute']['datatype']))	    // integer or string
		$this->_input['attribute']['datatype'] = '';
	}
	// this is for checking, when attribute type is text, by defult not display in search case
	if(($this->_input['attribute']['entry_type'] == $GLOBALS['conf']['ATTRIBUTE_TYPE']['text'] && $this->_input['attribute']['search_type'] == $GLOBALS['conf']['ATTRIBUTE_TYPE']['text']) && isset($this->_input['attribute']['datatype'])){
	    // to remove the text_values(<,>,...), when string type is selected.
	    if($this->_input['attribute']['datatype']==2 && isset($this->_input['attribute']['text_values'])){ // for string case
		$this->_input['attribute']['text_values']='0';
	    }
	    $this->_input['attribute']['is_searchable'] = '0';
	}else{
	    $this->_input['attribute']['is_searchable'] = '1';
	}
	// this is for checking, when attribute type other than dropdown, but the dropdwn related field has the values 
	if($this->_input['attribute']['entry_type']!=$GLOBALS['conf']['ATTRIBUTE_TYPE']['dropdown'] || !isset($this->_input['attribute']['dropdown_values'])){
	     $this->_input['attribute']['dropdown_values'] = "";
	}
	// this is for checking, when attribute type other than radio or check box, so optype set as default(1); 
	if(($this->_input['attribute']['entry_type']!=$GLOBALS['conf']['ATTRIBUTE_TYPE']['radio'] || $this->_input['attribute']['entry_type']!=$GLOBALS['conf']['ATTRIBUTE_TYPE']['checkbox']) && isset($this->_input['attribute']['optype'])){
	    $this->_input['attribute']['optype'] = 1;
	}
	// coresponding value of attribute_label saved in binary format as like 1,2,4,8,..
	if($this->_input['attribute']['attribute_label']){
	    $this->_input['attribute']['attribute_label'] = trim($this->_input['attribute']['attribute_label'],',');
	    $this->_input['attribute']['attribute_label'] = explode(",", $this->_input['attribute']['attribute_label']);
	    for ($x=0; $x <= count($this->_input['attribute']['attribute_label']); $x++) {
		if($this->_input['attribute']['attribute_label'][$x]){
		    $attr_val .= pow(2,$x).",";
		}
	    }
	    $attr_val = trim($attr_val,",");
	    $this->_input['attribute']['attribute_values'] = $attr_val;
	    $this->_input['attribute']['attribute_label'] = implode(",",$this->_input['attribute']['attribute_label']);
	}else{
	    $this->_input['attribute']['attribute_values'] = "";
	}
	// to arrange id_courses in to comma(,) separated format
//	if($this->_input['attribute']['id_course']){
//	    $this->_input['attribute']['id_course'] = array_unique($this->_input['attribute']['id_course']);
//	    $this->_input['attribute']['id_course'] = implode(",",$this->_input['attribute']['id_course']);
//	}
	//print "<pre>";print_r($this->_input['attribute']);print("</pre>");exit;
//	$id_courses_to_be_updated = $this->_chkAttrValAndTkAction($this->_input['id_attribute'],$this->_input['attribute']['id_course']);
//	$id_courses_to_be_updated = $this->_chkAttrValAndTkAction($this->_input['id_attribute']);
	$this->_input['attribute']['id_course'] = $id_courses_to_be_updated ; 
	$attribute = $this->_input['attribute'];
	
	$bool =  $this->dataObj->updateArrayFormat($attribute,"id_attribute=".$this->_input['id_attribute'] , "attribute");
	$this->_input['choice'] = 'listing';
	$this->_input['insEditDel'] = 1;
	if($this->_input['pname'] || $this->_input['ctype'] || $this->_input['stype']){
	    $this->_input['psearch'] = 1;
	}
	$this->_listing();
    }
    function _attributeDetail(){
	$attr = $GLOBALS['conf']['ATTRIBUTE_TYPE'];
	$this->_output['attr'] = array_flip($attr);
	$sql = get_search_sql("attribute","id_attribute=".$this->_input['id_attribute']);
	$this->_output['res'] = getsingleindexrow($sql);
	$this->_output['course'] = getindexrows(get_search_sql('course', '1'), 'id_course', 'course_name', $err);
	$this->_output['tpl']="admin/attribute/attributeDetail";
    }
    /*
     * @method _showType() : in case of entryType either radio or checkbox, the showType is also either type.
     */
    function _showType(){
	$attr = $GLOBALS['conf']['ATTRIBUTE_TYPE'];
	$attr = array_flip($attr);
	foreach ($attr as $key => $value) {
	    if($value == "radio" || $value == "checkbox"){
		$newattr[$key] = $value;
	    }
	}
	$this->_output['newattr'] = $newattr;
	$this->_output['tpl'] = 'admin/attribute/searchType';
    }
    
    function _searchable(){
	ob_clean();
	if(recordExists("attribute", "id_attribute='" . $this->_input['id_attribute'] . "'"))
	    $bool = $this->dataObj->updateStringFormat("is_searchable",$this->_input['status'],"id_attribute=".$this->_input['id_attribute'],"attribute");
	if($bool){
	    print 1;
	    
	}else{
	    print 0;
	}
	$_SESSION['raise_message']['global'] = ($bool)?"Attribute status changed sucessfully.":"Attribute status can't be changed.";
	exit;
    }
    
    
//    public function _chkAttrValAndTkAction($id_attr='',$id_courses=''){
//	global $link;
//	//Here let's fetch all courses from course table
////        $id_cousres_arr = getindexrows(get_search_sql("course", " 1 " , "id_course,code"),"id_course","code");
////        $id_cousres_arr_keys = ($id_cousres_arr) ? array_keys($id_cousres_arr) : array() ;
//        
//        //Here let's fetch course's assigned to this attribute " $id_attr " 
////        $id_cousres_assigned = getindexrows(get_search_sql("attribute", " id_attribute = $id_attr LIMIT 1 ", "id_attribute,id_course"),'id_attribute','id_course');
////        $id_cousres_assignedarr = ($id_cousres_assigned[$id_attr]) ? explode(',',$id_cousres_assigned[$id_attr]) : array() ;//$id_cousres_arr_keys);
////        asort($id_cousres_assignedarr);
//
//        //Here the new id_courses that added(updated) to this attribute " $id_attr "
////        $new_id_courses_arr = ($id_courses) ? explode(',',$id_courses) : $id_cousres_arr_keys;
////        asort($new_id_courses_arr);
//       
////        $attr_to_be_aded_to_cousre_arr = array_diff($new_id_courses_arr, $id_cousres_assignedarr);
////        $arr_not_to_dropcol = array();
//         //Here adding columns to newly added courses of this attibute " $id_attr "
////        if(!(!$attr_to_be_aded_to_cousre_arr && (count($new_id_courses_arr)==count($id_cousres_assignedarr)))){
////           foreach ($attr_to_be_aded_to_cousre_arr as $key => $value) {
////                $tbl = TABLE_PREFIX . "inst__{$id_cousres_arr[$value]}";
//                $tbl = TABLE_PREFIX . "attributedata";
////		$tblpricolnm = $id_cousres_arr[$value];
//		$col = "c$id_attr";
//		//$coltype = getsingleindexrow(get_search_sql("attribute", "id_attribute = $id_attr", "id_attribute,datatype"));
//		//$coltype = ($coltype['datatype']=='2') ? "VARCHAR(255)" : "INT" ;
//		$coltype = getsingleindexrow(get_search_sql("attribute", "id_attribute = $id_attr", "id_attribute,datatype,entry_type,search_type,dropdown_values"));
//		
//		if(!empty($coltype)){
//		    if($coltype['datatype']=='2'){
//			$coltyp="VARCHAR(255)";
//		    }elseif($coltype['entry_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget'] || $coltype['search_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget']){ // for calender widget,date case
//			$coltyp="DATE";
//		    }elseif(($coltype['entry_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['dropdown'] || $coltype['search_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['dropdown']) && ($coltype['dropdown_values'] == $GLOBALS['conf']['BOARD_TYPE']['board'] || $coltype['dropdown_values'] == $GLOBALS['conf']['BOARD_TYPE']['university'])){ // for board or university : varchar for multiauto
//			$coltyp="VARCHAR(255)";
//		    }else{
//			$coltyp="INT";
//		    }
//		}
//		/*$coltype = getsingleindexrow(get_search_sql("attribute", "id_attribute = $id_attr", "id_attribute,datatype,entry_type,search_type"));
//		if(!empty($coltype)){
//		    if($coltype['datatype']=='2'){
//			$coltyp="VARCHAR(255)";
//		    }elseif($coltype['entry_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget'] || $rec['search_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget']){ // for calender widget,date case
//			$coltyp="DATE";
//		    }else{
//			$coltyp="INT";
//		    }
//		}*/
//		$sql = "ALTER TABLE {$tbl} ADD {$col} {$coltyp}"; 
//		execute($sql,$err);
////		mysqli_query($link,$sql);
////            }
//            //Here to check courses one by one wheather the attribute column can ce droped or not
//            //If no values in that column then column droped else not
////            $attr_to_be_droped_from_cousre_arr = array_diff($id_cousres_assignedarr,$new_id_courses_arr);
////            if($attr_to_be_droped_from_cousre_arr){
////                foreach ($attr_to_be_droped_from_cousre_arr as $k => $v) {
////                    $filtr = $this->_checkValueExistInCol("c$id_attr","inst__{$id_cousres_arr[$v]}");
////                    if($filtr){
////                        $arr_not_to_dropcol[] = $v;
////                    }
////                }
////            }
//                    $filtr = $this->_checkValueExistInCol("c$id_attr","attributedata");
//                    if($filtr){
//                        $arr_not_to_dropcol[] = $v;
//                    }
////        }
//        $tmp = ($arr_not_to_dropcol) ? implode(',',$arr_not_to_dropcol) :'';
////        $id_courses_to_be_updated = trim("$tmp,$id_courses,",",");
////	return $id_courses_to_be_updated ;
//    }
    function _checkValueExistInCol($col,$tbl){
        global $link;
        $flag = 0 ;
        $sql = get_search_sql($tbl, " 1 ", $col);
        $res = mysqli_query($link,$sql);
        while($rec = mysqli_fetch_assoc($res)){
            if($rec[$col]){
                $flag=1;
                break;
            }
        }
        if(!$flag){
	    $tbl = TABLE_PREFIX.$tbl;
            $dropcolsql = " ALTER TABLE {$tbl} DROP {$col}";
			execute($sql,$err);
//            $dropcolsql = "CALL dropcolfromtable('$tbl','$col')";
//            mysqli_query($link,$dropcolsql);
        }
        return $flag;
    }
    
    
        
        function _search_attribute(){
            ob_clean();
             $namesql = get_search_sql("attribute","1  GROUP BY attribute_name ORDER BY attribute_name asc","attribute_name");
             $names = getrows($namesql);
           $str="<option value=''>Choose One</option>";
        foreach ($names as $key => $value) {
           $str.= "<option value=" ."'". $value['attribute_name'] ."'". ">" . $value['attribute_name'] . "</option>";
        }
        
           $attr = $GLOBALS['conf']['ATTRIBUTE_TYPE'];
           $attr = array_flip($attr);
           $str2="<option value=''>Choose One</option>";
        foreach ($attr as $key => $value) {
           $str2.= "<option value=" ."'". $key ."'". ">" . $value . "</option>";
        }
           $attr2 = $GLOBALS['conf']['ATTRIBUTE_TYPE'];
           $attr2 = array_flip($attr2);
           $str3="<option value=''>Choose One</option>";
        foreach ($attr2 as $key => $value) {
           $str3.= "<option value=" ."'". $key ."'". ">" . $value . "</option>";
        }
         echo $str.'@@'.$str2.'@@'.$str3;
        exit;
        
        }
        
};
