<?php

class admin_setting extends setting_manager {

	/**
	 * This is a constructor to initialized setting admin module.This overrides user side constructor.
	 *
	 * @param object $smarty Reference of smarty object
	 *
	 * @param Array $_output Output query string
	 *
	 * @param Array $_input Input query string
	 */
	public function __construct(& $smarty, & $_output, & $_input) {
		if ($_SESSION['id_admin']) {
			parent::__construct($smarty, $_output, $_input);
		} else {
			redirect(LBL_ADMIN_SITE_URL);
		}
	}

	function _default() {
		return $this->_listing();
	}

	/**
	 * * @method _listing() function is used fo the listing of the config variables and respective value.
	 *
	 * @return Template with config variable data
	 */
	function _listing() {
		$cond = isset($_SESSION['id_developer']) ? 1 : "is_editable = 1";
		$sql = get_search_sql("config", $cond . " ORDER BY name,id_seq");
		$this->_output['res'] = getrows($sql, $err);
		$this->_output['tpl'] = isset($_SESSION['id_developer']) ? 'admin/setting/dev_listing' : 'admin/setting/listing';
	}

	/**
	 * * @method _add() show template with form data to add config variables(It will display only in case of developer side.)
	 *
	 * @return template with form data
	 */
	function _add() {
		if ($this->_input['id_config']) {
			$this->_output['section'] = recordExists("config", "id_config = '" . $this->_input['id_config'] . "'");
			$this->_output['add_section'] = 1;
		} else {
			$sql = get_search_sql("config", " 1 GROUP BY name");
			$this->_output['section'] = getindexrows($sql, 'name', 'name');
		}
		$this->_output['f_type'] = array("text" => "Text", "radio" => "Radio", "checkbox" => "Checkbox", "dropdown" => "Drop Down", "textarea" => "Text Area");
		$this->_output['tpl'] = 'admin/setting/dev_add';
	}

	/**
	 * * @method _edit() show template with pre filled form data to edit config variables
	 *
	 * @return template with pre filled form data
	 */
	function _edit() {
		$cond = "1 GROUP BY name";
		$sql = get_search_sql("config", $cond); // changed by prabhu.
		$this->_output['section'] = getindexrows($sql, 'name', 'name');
		$sql_config = get_search_sql('config', "id_config = '" . $this->_input['id_config'] . "' LIMIT 1");
		$res = getrows($sql_config, $err);
		$this->_output['res'] = $res[0];
		$this->_output['f_type'] = array("text" => "Text", "radio" => "Radio", "checkbox" => "Checkbox", "dropdown" => "Drop Down", "textarea" => "Text Area");
		$this->_output['tpl'] = 'admin/setting/dev_add';
	}

	/**
	 * * @method _updateConfig() Updates section config variable
	 *
	 * @return void Write all contents config variable to file
	 *
	 */
	function _updateConfig() {
		$sectype = $this->_input['sectype'];
		foreach ($this->_input[$sectype] as $key => $value) {
			if ($_SESSION['id_developer']) {
				if (is_array($this->_input[$sectype][$key][$key])) {
					//This one line sufficient for all config except checkbox.
					//Bellow 5 line is not required;
					$arr['value'] = implode(",", $this->_input[$sectype][$key][$key]);
				} else if ($this->_input[$sectype][$key][$key]) {
					$arr['value'] = $this->_input[$sectype][$key][$key];
				} else {
					$arr['value'] = 0;
				}
				$arr['comment'] = $this->_input[$sectype][$key]['comment'];
				$this->dataObj->updateArrayFormat($arr, "id_config=" . $key); //Here we use UpdateArrayFormat because the problem in the checkbox update.
			} else {
				if (is_array($value)) {
					$value = implode(",", $value);
				}
				$this->dataObj->updateStringFormat("value", $value, "id_config=" . $key);
			}
		}
		$this->writeConfig('ajax');
	}

	/**
	 * * @method _deleteConfig() Delete config variable(Only for developer)
	 *
	 * @return void Write all contents config variable to file
	 *
	 */
	function _deleteConfig() {
		ob_clean();
		$keys = trim(stripslashes($this->_input['keys']), ',');
		$this->dataObj->delete("id_config IN (" . $keys . ")");
		$this->writeConfig('ajax');
	}

	/**
	 * * @method _setConfig() Insert new config variable(Only for developer)
	 *
	 * @return void Write to config file and return to listing function
	 *
	 */
	function _setConfig() {
		ob_clean();
		$cres = getsingleindexrow(get_search_sql('config', " ckey='" . $this->_input['sec']['ckey'] . "' LIMIT 1"));
		if ($cres['name'] == $this->_input['sec']['name']) {
			print "no";
			exit;
		} else {
			$this->_input['sec']['name'] = str_replace(" ", "_", trim($this->_input['sec']['name']));
			$this->_input['sec']['name'] = strtoupper($this->_input['sec']['name']);
			$this->dataObj->insertArrayFormat($this->_input['sec']);
			$this->writeConfig('ajax');
		}
	}

	/**
	 * * @method _editConfig() Update single config variable
	 *
	 * @return void Write all contents config variable to file
	 */
	function _editConfig() {
		ob_clean();
		$cres = getsingleindexrow(get_search_sql('config', " ckey='" . $this->_input['sec']['ckey'] . "' AND id_config != '" . $this->_input['id_config'] . "' LIMIT 1"));
		if ($cres['name'] == $this->_input['sec']['name']) {
			print "no";
			exit;
		} else {
			$this->_input['sec']['name'] = strtoupper($this->_input['sec']['name']);
			$this->dataObj->updateArrayFormat($this->_input['sec'], "id_config = " . $this->_input['id_config']);
			$this->writeConfig('ajax');
		}
	}

	/**
	 * * @method writeConfig() Used to prepare data for writing config file
	 *
	 * @param string $p either ajax or plain
	 *
	 * @return void redirect to listing function or javascript function if $p = 'ajax'
	 */
	function writeConfig($p = '') {
		$sql = get_search_sql('config', " 1 ORDER BY name,id_config");
		$res = getrows($sql, $err);
		foreach ($res as $key => $val) {
			$sampleData[$val['name']][$val['ckey']] = $val['value'];
		}
		$f = $this->writeIniFile($sampleData, APP_ROOT . 'flexymvc/configs/' . SITE_USED . '/config.ini.php');
		if ($p) {
			print LBL_ADMIN_SITE_URL . "setting/listing";
		} else {
			redirect(LBL_ADMIN_SITE_URL . "setting/listing");
		}
	}

	/**
	 *
	 * * @method writeIniFile() Used to write congig data into config file
	 *
	 * @param Arary $assoc_arr  Array of config variable
	 *
	 * @param String $path  Path to config file
	 *
	 * @param Boolean $has_sections Either TRUE or FALSE. Default is TRUE
	 *
	 * @return Boolean true or false
	 */
	function writeIniFile($assoc_arr, $path, $has_sections = TRUE) {
		$content = "";
		if ($has_sections) {
			foreach ($assoc_arr as $key => $elem) {
				if ($content)
					$content .= "\n";
				$content .= "[" . $key . "]\n";
				foreach ($elem as $key2 => $elem2) {
					if (is_array($elem2)) {
						for ($i = 0; $i < count($elem2); $i++) {
							$content .= $key2 . "[] = \"" . $elem2[$i] . "\"\n";
						}
					} else if ($elem2 == "")
						$content .= $key2 . " = \n";
					else
						$content .= $key2 . " = \"" . $elem2 . "\"\n";
				}
			}
		}else {
			foreach ($assoc_arr as $key => $elem) {
				if (is_array($elem)) {
					for ($i = 0; $i < count($elem); $i++) {
						$content .= $key2 . "[] = \"" . $elem[$i] . "\"\n";
					}
				} else if ($elem == "")
					$content .= $key2 . " = \n";
				else
					$content .= $key2 . " = \"" . $elem . "\"\n";
			}
		}
		if (!$handle = fopen($path, 'w')) {
			return false;
		}
		if (!fwrite($handle, $content)) {
			return false;
		}
		fclose($handle);
		return true;
	}

	/**
	 * * @method _msgList() Listing message
	 *
	 * @param boolean $flag Used for redirection
	 *
	 * @param string $ser_msg_name Search form field
	 */
	function _msgList($flag = '', $ser_msg_name = '') {
		$uri = PAGINATE_ADMIN_PATH . 'setting/msgList';
		$ser_msg_name = $this->_input['ser_msg_name'] ? $this->_input['ser_msg_name'] : '';
		$flag = $this->_input['flag'] ? $this->_input['flag'] : $flag;
		$cond1 = "1";

		if ($ser_msg_name) {
			$cond1.=" AND (key_name like '%" . $ser_msg_name . "%' OR key_value like '%" . $ser_msg_name . "%')";
			$uri.="/ser_msg_name/" . $ser_msg_name;
		}

		$this->_output['uri'] = $uri;
		$this->_output['type'] = "box";  // no, extra, nextprev, advance, normal, box
		$this->_output['ajax'] = "1";
		$this->_output['pg_header'] = "Message Listing";
		$this->_output['sort_by'] = "lang_code";

		$this->_output['field'] = array("key_name" => array("Message Key", 1), "key_value" => array("Message Value", 1), "lang_code" => array("Language", 1));

		$this->_output['links'][] = array("Edit", "", "id_message", CSS_PATH . "img/led-ico/pencil.png", "function" => "editMsg");
		if ($_SESSION['id_developer']) {
			$this->_output['links'][] = array("Delete", "", "key_name", CSS_PATH . "img/led-ico/delete.png", "function" => "deleteMsg");
		}

		$this->_output['limit'] = $GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['sql'] = get_search_sql("message", $cond1);

		if ($this->_input['pg'] || $ser_msg_name || $flag) {
			$this->blObj->page_listing($this, "admin/setting/msg_list");
		} else {
			$this->blObj->page_listing($this, "admin/setting/msg_search");
		}
	}

	/**
	 * * @method _addMsg() show template with form data to add message
	 *
	 * @uses According to the value of the $this->_input['chk'] the return type of the function is changed.
	 * 	    The value of the $this->_input['chk'] is set after the insertion or during the edit.
	 *
	 * @return template If $this->_input['chk'] is set then it returns the add_msg_main template else add_msg_new template.
	 */
	function _addMsg() {
		//print "<pre>";
		//print_r($this->_input);
		//print "</pre>";
		$this->_output['language'] = $GLOBALS['conf']['LANGUAGE'];
		$sel_lang = isset($this->_input['lang_code']) ? $this->_input['lang_code'] : $GLOBALS['conf']['LANGUAGE']['English'];
		$this->_output['sel_lang'] = $sel_lang;
		if (!$this->_input['chk']) {
			$this->_output['tpl'] = 'admin/setting/add_msg_main';
		} else {
			$cond = "key_name = '" . strtoupper($this->_input['key_name']) . "' AND lang_code ='" . $sel_lang . "'";
			$this->_output['row'] = getsingleindexrow(get_search_sql("message", $cond));
			$this->_output['tpl'] = 'admin/setting/add_msg_new';
		}
	}

	/**
	 * * @method _insertMsg() Used for the insertion of the message
	 *
	 * @return int it returns either 0 (if insertion fail) or inserted id (if insertion success.) .
	 */
	function _insertMsg() {
		ob_clean();
		$data = $this->_input['msg'];
		if ($data['key_name']) {
			$data['key_name'] = strtoupper($data['key_name']);
			$insID = $this->dataObj->insertArrayFormat($data, "", "", "message");
			$this->writeFile();
			print $insID;
			exit;
		} else {
			print 0;
			exit;
		}
	}

	/**
	 * * @method _editMsg() shows template add_msg_main with pre filled data for edit.
	 *
	 * @return template with pre filled form data
	 */
	function _editMsg() {
		$this->_output['row'] = getsingleindexrow(get_search_sql("message", "id_message = '" . $this->_input['id_msg'] . "'"));
		$this->_output['language'] = $GLOBALS['conf']['LANGUAGE'];
		$this->_output['sel_lang'] = $this->_output['row']['lang_code'];
		$this->_output['tpl'] = 'admin/setting/add_msg_main';
	}

	/**
	 * * @method _updateMsg() update message.
	 *
	 * @return void.
	 */
	function _updateMsg() {pr($this->_input);//exit;
		$data = $this->_input['msg'];//pr($data);//exit;
		$data['key_name'] = strtoupper($data['key_name']);//pr($data['key_name']);//exit;
		$this->dataObj->updateArrayFormat($data, "id_message = " . $this->_input['id_message'], "message");
		$this->writeFile();
	}

	/**
	 * * @method _deleteMsg() deletie message.
	 *
	 * @return void redirect to the msgList with some predefined condition.
	 */
	function _deleteMsg() {
		$this->dataObj->delete("key_name = '" . $this->_input['key_name'] . "'", "message");
		$this->writeFile();
		$this->_msgList(1, $this->_input['ser_msg_name']);
	}

	/**
	 * * @method _checkDuplicateKey() function is used for the checking of the duplicate key in case of message insertion or updation.
	 *
	 * @return string Returns  "Error:1" if the message key_name already exist and true otherwise.
	 */
	function _checkDuplicateKey() {
		ob_clean();
		$res = getrows(get_search_sql("message", "key_name = '" . strtoupper($this->_input['msg_key']) . "' LIMIT 1"), $err);
		if ($res) {
			print "Error:1";
		} else {
			print true;
		}
		exit;
	}

	/**
	 * * @method writeFile() write message to the file message_constant.php.
	 */
	function writeFile() {
		$fp = fopen(AFIXI_ROOT . "configs/" . SITE_USED . "/message_constant.php", "w");
		$res = getrows(get_search_sql("message"), $err);
		$line = "<?php\n/* This is a system generated file. Please do not modified it manually */\n";
		foreach ($res as $key => $val) {
			$key_lang_code = $val['key_name'] . "_" . strtoupper($val['lang_code']);
			$line .= "define('" . $key_lang_code . "','" . addslashes($val['key_value']) . "');" . "\n";
		}
		$line .="?>";
		fwrite($fp, $line);
		fclose($fp);
	}

	/**
	 * * @method _jsMsgList() listing of the javascript message.
	 *
	 * @param type $dev
	 *
	 * @uses $dev is a conditional variable to check whether the user is a developer or not.
	 *
	 * @return template jsmsg_list_dev.tpl.html if developer else jsmsg_list with pre filled data.
	 */
	function _jsMsgList($dev = '') {
		$this->_output['list'] = getrows(get_search_sql(jsmsg, " 1 order by level asc"), $err);
		if ($_SESSION['id_developer'] || $dev) {
			$this->_output['tpl'] = 'admin/setting/jsmsg_list_dev';
		} else {
			$this->_output['tpl'] = 'admin/setting/jsmsg_list';
		}
	}

	/**
	 * * @method _updateAllJsMsg() update javascript message.
	 *
	 * @return void Redirect to the jsMsgList.
	 */
	function _updateAllJsMsg() {
		$res = $this->_input['res'];
		$s = $this->dataObj->updateArrayFormat($res, "id_jsmsg = ", "jsmsg", TRUE);
		if ($s) {
			$this->_writeJsMsgFile();
			$_SESSION['raise_message']['global'] = getmessage('SETTING_JSMSG_UPD_SUCC');
		} else {
			$_SESSION['raise_message']['global'] = getmessage('SETTING_JSMSG_UPD_FAIL');
		}
		redirect(LBL_ADMIN_SITE_URL . "setting/jsMsgList");
	}

	/**
	 * * @method _writeJsMsgFile() write the javascript message to the file flexymessage.js.
	 *
	 * @return void.
	 */
	function _writeJsMsgFile() {
		$fp = fopen(APP_ROOT . 'templates/flexyjs/flexymessage.js', "w");
		$res = getrows(get_search_sql("jsmsg", "1", "level,message"), $err);
		$line = "flexymsg ={ \n";
		foreach ($res as $key => $val) {
			$line.="	" . $val['level'] . ": " . '"' . $val['message'] . '",' . "\n";
		}
		$line = rtrim($line, ",\n");
		$line .="\n};";
		fwrite($fp, $line);
		fclose($fp);
	}

	/**
	 * * @method _deleteJsMsg() delete javascript message.
	 *
	 * @return void.
	 */
	function _deleteJsMsg() {
		$s = $this->dataObj->delete("id_jsmsg = " . $this->_input['id'], "jsmsg");
		$this->_writeJsMsgFile();
	}

	/**
	 * * @method _insertJsMsg() insert jaavscript message.
	 *
	 * @return the url to jsMsgList with a additional parameter $msg (message for successful insertion or fail to insert)
	 */
	function _insertJsMsg() {
		ob_clean();
		$res['level'] = $this->_input['lev'];
		$res['message'] = $this->_input['msg'];
		$res['description'] = $this->_input['des'];
		$id = $this->dataObj->insertArrayFormat($res, "", "", "jsmsg");
		if ($id) {
			$this->_writeJsMsgFile();
			$msg = getmessage('SETTING_JSMSG_INS_SUCC');
		} else {
			$msg = getmessage('STTTING_JSMSG_INS_FAIL');
		}
		echo LBL_ADMIN_SITE_URL . "setting/jsMsgList=$msg";
		exit;
	}

	/**
	 * * @method _searchJsMsg() searching of the javascript message.
	 *
	 * @return template jsmsg_list_dev.tpl.html with pre filled data if developer else jsmsg_list.
	 */
	function _searchJsMsg() {
		$this->_output['list'] = getrows(get_search_sql("jsmsg", " level LIKE '" . $this->_input['val'] . "%' OR message LIKE '" . $this->_input['val'] . "%' ORDER BY level ASC "), $err);
		if ($this->_input['dev']) {
			$this->_output['tpl'] = 'admin/setting/jsmsg_list_dev';
		} else {
			$this->_output['tpl'] = 'admin/setting/jsmsg_list';
		}
	}

	/**
	 * * @method _checkLevel() check whether the level of javascript message is present in the database or not.
	 *
	 * @return string  "exist"
	 */
	function _checkLevel() {
		$res = getrows(get_search_sql("jsmsg", " level = '" . $this->_input['val'] . "'"), $err);
		if ($res[0]['level']) {
			print "<font color='red'>*" . "exist" . ".</font>";
		}
	}

	function _manageLogo() {
		$this->_output['tpl'] = "admin/setting/logo";
	}

	function _previewImage() {
		ob_clean();
		//$imgId is array element number to delete session preview image
		$imgId = $this->_input['imgId'] ? $this->_input['imgId'] : 0;
		previewImage($_FILES['image'], $imgId, "1");
	}

	function _updateLogo() {
		$search = LBL_SITE_URL . $GLOBALS['conf']['IMAGE']['image_logo'];
		$imgname = str_replace($search, '', $this->_input['prevlogo']);
		$previewImg = $this->_input['prev_img'];
		if ($previewImg != '') {
			$previewOrigDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_orig'];
			$uploadDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_logo'];
			$newFileName = substr($previewImg, (strpos($previewImg, "_") + 1));
			if ($imgname) {
				@unlink($uploadDir . $imgname);
			}
			@copy($previewOrigDir . $previewImg, $uploadDir . $newFileName);

			$url = LBL_SITE_URL . $GLOBALS['conf']['IMAGE']['image_logo'] . $newFileName;
			$status = $this->_input['status'];

			$sql_url = "UPDATE " . TABLE_PREFIX . "config SET `value`='" . $url . "' WHERE name='ADMIN_LOGO' AND ckey='url'";
			execute($sql_url, $err);
		}
		$sql_status = "UPDATE " . TABLE_PREFIX . "config SET `value`='" . $status . "' WHERE name='ADMIN_LOGO' AND ckey='status'";
		execute($sql_status, $err);
		$this->writeConfigurl();
		redirect(LBL_ADMIN_SITE_URL . "setting/manageLogo");
	}

	function writeConfigurl() {
		$sql = get_search_sql('config', " 1 ORDER BY name,id_config");
		$res = getrows($sql, $err);
		foreach ($res as $key => $val) {
			$sampleData[$val['name']][$val['ckey']] = $val['value'];
		}
		$f = $this->writeIniFile($sampleData, APP_ROOT . 'flexymvc/configs/' . SITE_USED . '/config.ini.php');
	}

}

;
