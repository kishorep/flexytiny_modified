<?php

//For development server 'On' and for Production server 'Off'
error_reporting(E_ALL);
session_start();
ini_set('display_errors', 'Off');
ini_set("session.cookie_domain", trim($_SERVER['SERVER_NAME'], "www."));

//echo "<pre>";print_r($_SERVER);exit;
    define("SUB_DIR", "flexytiny_new/");
if (SUB_DIR)
    session_name(trim(SUB_DIR, '/'));

// date_default_timezone_set(@date_default_timezone_get()); // remove the comment if u get date() error message

/* $ccp = session_get_cookie_params();
  $rootDomain = trim($_SERVER['SERVER_NAME'], "www.");
  session_set_cookie_params($ccp["lifetime"], $ccp["path"], $rootDomain, $ccp["secure"], $ccp["httponly"]) ; */

ob_start();
ini_set("memory_limit", "128M");
$report = array('stats' => array());

define("THROUGH_CONTROLLER", 1);
define("AJAX", 0);
define("AFIXI_ROOT", $_SERVER['DOCUMENT_ROOT'] . "/" . SUB_DIR . "flexymvc/");
define("AFIXI_CORE", $_SERVER['DOCUMENT_ROOT'] . '/flexymvc_core/');

if(trim($_SERVER['SERVER_NAME'], "www.")=="manoranjan.afixiindia.com"||"192.168.1.58"){
    $_SESSION['multi_language'] = "default"; 
}else{
    $_SESSION['multi_language'] = "default";
}

include_once(AFIXI_CORE . "common.php");

// Code for set multilanguage
if ($GLOBALS['conf']['MULTI_LANG']['islang']) {
    $_SESSION['multi_language'] = isset($_SESSION['multi_language']) ? $_SESSION['multi_language'] : "default";
} else {
    $_SESSION['multi_language'] = "default";
}


// End code for set multilanguage
include_once($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_DIR . "common_files.php");

if (defined("SITE_USED") && file_exists(AFIXI_ROOT . '../' . SITE_USED . '.php')) {
    include_once(AFIXI_ROOT . '../' . SITE_USED . '.php');
} else {
    include_once(AFIXI_ROOT . '../site.php');
}

$ce = isset($_input['ce']) ? $_input['ce'] : 1;
$site = new site($ce);
$smarty = getSmarty();

$site->smarty = &$smarty;
$site->cache_id = &$cache_id;
if (isset($_input['mod'])) {
    $result = user_templates::get_mod($_input, '', "ACTION");
    unset($_input['choice']);
    if ($result) {
	if (is_array($result)) {
	    redirect($result['redirect_url']);
	} else {
	    $mgr = $_input['mgr'];
	    if ($mgr) {
		$_input[$mgr . '_choice'] = str_replace('choice_', '', $_input['success']);
	    } else {
		$_input['choice'] = str_replace('choice_', '', $_input['success']);
	    }
	}
    } else {
	$mgr = $_input['mgr'];
	$_input['choice'] = str_replace('choice_', '', $_input['fail']);
    }
}
$actual_link ='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$chkurl=  recordExists("redirecturl", "old_url='{$actual_link}'");
if(!empty($chkurl)){
	$dt=date("Y-m-d H:i:s");
	$occurance=$chkurl['occurance']+1;
$upsql="UPDATE ".TABLE_PREFIX."redirecturl SET occurance={$occurance},last_ip='{$_SERVER['REMOTE_ADDR']}',last_time='{$dt}' WHERE id={$chkurl['id']}";
execute($upsql);
header("HTTP/1.1 301 Moved Permanently"); 
header('Location: '.$chkurl['new_url']);
exit;
}
$page = isset($_input['page']) ? $_input['page'] : 'common';
$site->handle_page($page);

/* if(isset($GLOBALS['conf']['PAGE_HEADERS'])){
  $page_title=$GLOBALS['conf']['PAGE_HEADERS'][$page.'_page_title'];
  $smarty->assign('page_title', $page_title);
  }else{
  $smarty->assign('page_title', "::. User Site .::");
  } */

/* if (isset($_input['ce'])) {
  $site->set_container_enabled($_input['ce']);
  } */
//Added By Parwesh For Error Handling When Page is not found
$file_test = TEMPLATE_DIR . $_SESSION['multi_language'] . "/" . $site->default_tpl . TEMPLATE_EXTENSION;

if ($page == 'user' || file_exists($file_test)) {
    if ($site->is_container_enabled) {
	//$smarty->assign('content', $smarty->add_theme_to_template($site->default_tpl));
	$content = $smarty->add_theme_to_template($site->default_tpl);
	$smarty->assign('content_fetch', $smarty->fetch($content));
	$tpl = isset($_GET['c_tpl']) ? $_GET['c_tpl'] : $site->container_tpl;
    } else {
	$byPassArray = array("register", "loginForm", "setLogin", "checkUsername", "insert", "fconnect", "checkUsername", "checkEmail", "commonSocialNetwork", "showMap", "linkedin", "edit", "facebook", "twitterEmail", "updateEmail", "checkValidUser", "twitt_connect", "twittDetails","list_user");
	if (empty($_SESSION['id_user']) && !in_array($_REQUEST['choice'], $byPassArray)) {
	    header("HTTP/1.0 401 Not Found");
	    exit;
	}
	$tpl = $site->default_tpl;
    }
    if (!empty($_SESSION['CACHE_OUTPUT'])) {
	ob_start();
    }
    restore_error_handler();
    $report['debug'] = 0;
    $report['end_code'] = getmicrotime();
    $smarty->assign_by_ref('report', $report);
    $smarty->debugging_ctrl = 'URL';

    $title = '::. FLEXYMVC TINY : User Site .::';
    $meta_keywords = "key1,key2,key3";
    $meta_description = "This is a flexymvc site";
    $smarty->assign_by_ref('title', $title);
    $smarty->assign_by_ref('meta_keywords', $meta_keywords);
    $smarty->assign_by_ref('meta_description', $meta_description);


    $smarty->display($tpl, $cache_id);
    $time_end = getmicrotime();
    unset($_SESSION['raise_message']);
    if (!empty($_SESSION['CACHE_OUTPUT'])) {
	ob_end_flush();
	unset($_SESSION['CACHE_OUTPUT']);
    }
} else {
    if (!($page == 'templates' || $page == 'image')) {
	$_SESSION['raise_message']['global'] = "<h2>The requested page is not available.<h2>";
	redirect(LBL_SITE_URL);
    } else {
	return;
    }
}
db_close();
showdebug();
ob_end_flush();
?>
