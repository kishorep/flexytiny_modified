<?php

//print_r($_REQUEST);exit;
/*
 * Created on Mar 31, 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class site {

    var $is_container_enabled = 1;

    function init() {
	//include_once (AFIXI_ROOT."configs/".SITE_USED.'/permission.php');
    }

    function handle_page($page) {
	$this->smarty->caching = 1;
	$this->smarty->cache_lifetime = 2000;
	switch ($page) {
	    case 'admin':
		$this->cache_id = "admin_login";
		$this->default_tpl = 'admin/userlogin';
		break;
	    default :
		$this->cache_id = $page;
		$this->default_tpl = 'admin/' . $page . '/home';
		break;
	}
    }

    function is_container_enabled($ce) {
	return $ce;
    }

    function get_container_tpl() {
	if (!empty($_SESSION['id_admin'])) {
	    return "admin/home";
	}else if(!empty($_SESSION['id_user'])) {
	    redirect(LBL_SITE_URL);
	}else {
	    if(isset($_SERVER[PARSE_FOR_COLLECT_INPUT])){
		$nextRed = preg_replace("/\/([a-zA-Z0-9_]*)\/index.php([\/]*)/", "", $_SERVER[PARSE_FOR_COLLECT_INPUT]);
		if ($nextRed)
		    $_SESSION['nextredirect'] = $nextRed;
	    }
	    return "admin/userlogin";
	}
    }

};