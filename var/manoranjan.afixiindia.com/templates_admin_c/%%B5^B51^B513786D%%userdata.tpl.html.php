<?php /* Smarty version 2.6.7, created on 2017-07-20 10:13:55
         compiled from admin/csvuser/userdata.tpl.html */ ?>

<!-- Template: admin/csvuser/userdata.tpl.html Start 20/07/2017 10:13:55 --> 
  <?php echo '
<script>
    var code;
    function checkValidate() {
var validator = $("#userdata").validate({
//    ignore: "input[type=\'text\']:hidden",
rules: {
"post": {
required: true,
 },
"name": {
required: true,
 },
"status": {
required: true,
 },
 },
messages: {
"post": {
required: "<br>" + flexymsg.required,
 },
"name": {
required: "<br>" + flexymsg.required,
 },
"status": {
required: "<br>" + flexymsg.required,
 },
 }
 });
var x= validator.form();
var y=imagevalidation();
var z = $(\'#div_err2\').text().length;
if (x && y && !z) {
$("#csvsubmit").prop("disabled",true);
return x;
 } else {
$(\'#csvsubmit\').removeAttr(\'disabled\');
return false;
 }
 }
function imagevalidation(){
  filename = $("#csvfilebtn").val();
  extention= filename.split(\'.\').pop();
  if(extention != \'csv\'){
 $(\'#err_msg\').html(\'<font color="red">Please Choose CSV File</font>\');
 $("#csvfilebtn").val("");
return false;
   }else{
    $(\'#err_msg\').html("");
    
   }
if( $("#csvfilebtn").val() == ""){
$(\'#err_msg\').html(\'<font color="red">This field required</font>\');
return false;
 }else{
$(\'#err_msg\').html("");
return true;
 }
 }

function checkuniquename(id){
    var value = $("#csvnm").val();
            $.post(siteurl,{"page" : "csvuser", "choice" : "checkuniquename", "ce" : "0", "value" : value,"id":id },function(res){//alert(res);//return false;
            if(res == 1){
               $("#div_err2").html(\'\'); 
             }else{
                $("#div_err2").html(\'<font color="red">Name already Exist.</font>\');
             }
         });
 }

        function downlodecsvfile(){
            window.location.href="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/csvuser/samplecsvfile/ce/0/";
          }
</script>

'; ?>



<div style="width: 1000px;">
  <div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget green">
                           <div class="widget-title">
                                <h4><i class="icon-reorder"></i><?php if ($this->_tpl_vars['sm']['data']): ?>Edit<?php else: ?>Upload<?php endif; ?>CSV:</h4>
                                <span class="fltrht"><a href="javascript:void(0);" onclick="downlodecsvfile();" class="buton btn_csv">Sample CSV</a></span>
                            </div>
      <div id="error" align="center" style="display:none"></div>
      <div class="widget-body">
	<form name="userdata" id="userdata" action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/csvuser/uploadcsvfile" onsubmit="return checkValidate();" method="post" enctype="multipart/form-data">
	    <input type="hidden" name="tblid" value='<?php echo $this->_tpl_vars['sm']['data']['id']; ?>
' />
            <input type="hidden"  name="user[code]" value="<?php echo $this->_tpl_vars['sm']['code']; ?>
">
            <table class="table table-striped">
                 <?php if ($this->_tpl_vars['sm']['post']): ?>
		<tr>
		    <td width="20%">Choose Post :</td>
		    <td>
			<select name="post" id="postdata" onchange="checkpost();">
			    <option value="">--Select--</option>
			    <?php if (count($_from = (array)$this->_tpl_vars['sm']['post'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			    <option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
			    <?php endforeach; endif; unset($_from); ?>
			    </select>
		    </td>
		</tr>
                <?php endif; ?>
		<tr>
		    <td width="20%">Name:</td>
		    <td>
                        <input id="csvnm" type="input" name="name" class="form-control" value="<?php echo $this->_tpl_vars['sm']['data']['name']; ?>
" onchange="checkuniquename('<?php echo $this->_tpl_vars['sm']['data']['id']; ?>
');"/> 
		    </td>
		</tr>
		<tr>
		    <td width="20%">Upload CSV:</td>
		    <td>
                        <input id="csvfilebtn" type="file" name="csvfile" class="upload" onchange="imagevalidation();"/>
		    </td>
		</tr>
		<tr <?php if (! $this->_tpl_vars['sm']['data']): ?>style="display:none;"<?php endif; ?>>
		    <td width="20%">Choose yourr option:</td>
		    <td>
                        <input  type="radio" name="status"  value="1"/>Replace And Insert
			<input  type="radio" name="status"  value="2"/>Replace Without Duplicate<br/>
                        <label class="error" for="status" generated="true"></label>
		    </td>
		</tr>
	    </table>
        <div class="form-actions" align="center">
     <input type="submit" name="sub"  id="csvsubmit" value="<?php if ($this->_tpl_vars['sm']['code']): ?>Next<?php else: ?>Submit<?php endif; ?>" class="btn btn-success" />
        </div>
	</form>
      </div>
    </div>
  </div>
    </div>
</div>

<!-- Template: admin/csvuser/userdata.tpl.html End --> 