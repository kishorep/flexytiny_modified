<?php /* Smarty version 2.6.7, created on 2017-06-27 15:36:57
         compiled from admin/setting/msg_list.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', 'admin/setting/msg_list.tpl.html', 21, false),array('function', 'eval', 'admin/setting/msg_list.tpl.html', 91, false),)), $this); ?>

<!-- Template: admin/setting/msg_list.tpl.html Start 27/06/2017 15:36:57 --> 
 <?php $this->assign('lang_code', $this->_tpl_vars['util']->get_values_from_config_reverse('LANGUAGE')); ?>
<?php $this->assign('qstart', $this->_tpl_vars['sm']['next_prev']->start); ?>
<?php $this->assign('m', $this->_tpl_vars['sm']['next_prev']->module); ?>
<?php $this->assign('total', $this->_tpl_vars['sm']['next_prev']->total); ?>
<!--done by gayatree starts-->
<?php echo '
<style type="text/css">
  .msglisting_admin{display:none; }
</style>
'; ?>

<div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i> <?php if ($this->_tpl_vars['sm']['pg_header']): ?>
        <?php echo $this->_tpl_vars['sm']['pg_header']; ?>
&nbsp;(<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
)
        <?php else: ?>
        <?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['next_prev']->module)) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
 Lists
        <?php endif; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                              <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
        <div  class="fltrht">
        <?php if ($_SESSION['id_developer']): ?>
        <input type="button" value="Add New" onclick="addMsg();" class="btn" />
        <?php endif; ?>
        </div>
        <?php endif; ?>
                            </div>
                          
                           <?php if ($_SESSION['id_developer']): ?>
    <div class="hdng" align="center">Please does not remove Message Key starting with SETTING_</div><br />
    <?php endif; ?>
                            <div class="widget-body">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                      <?php if ($this->_tpl_vars['sm']['special']): ?>
                                        <th><?php echo $this->_tpl_vars['sm']['special']['prompt']; ?>
</th>
                                        <?php endif; ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['field'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
<?php if ($this->_tpl_vars['key'] == $_SESSION[$this->_tpl_vars['m']]['sort_by']): ?>
<?php if ($_SESSION[$this->_tpl_vars['m']]['sort_order'] == 'ASC'): ?>
<?php $this->assign('cls', 'asc'); ?>
<?php else: ?>
<?php $this->assign('cls', 'desc'); ?>
<?php endif; ?>
<?php else: ?>
<?php $this->assign('cls', ""); ?>
<?php endif; ?>
                                        <th class="<?php echo $this->_tpl_vars['cls']; ?>
"><?php if ($this->_tpl_vars['item']['1'] != 0): ?>
<?php if ($this->_tpl_vars['sm']['ajax']): ?>
<a href="javascript:get_next_page('http://manoranjan.afixiindia.com/flexytiny_new/<?php echo $this->_tpl_vars['sm']['uri']; ?>
/sort_by/<?php echo $this->_tpl_vars['key']; ?>
',0,0,'<?php echo $_REQUEST['page']; ?>
_<?php echo $_REQUEST['choice']; ?>
')"><?php if ($this->_tpl_vars['item'] != ""):  echo $this->_tpl_vars['item']['0'];  else:  echo $this->_tpl_vars['key'];  endif; ?></a>
<?php else: ?>
<a href="http://manoranjan.afixiindia.com/flexytiny_new/<?php echo $this->_tpl_vars['sm']['uri']; ?>
/sort_by/<?php echo $this->_tpl_vars['key']; ?>
"><?php if ($this->_tpl_vars['item'] != ""):  echo $this->_tpl_vars['item']['0'];  else:  echo $this->_tpl_vars['key'];  endif; ?></a>
<?php endif; ?>
<?php else: ?>
<?php if ($this->_tpl_vars['item'] != ""):  echo $this->_tpl_vars['item']['0'];  else:  echo $this->_tpl_vars['key'];  endif; ?>
<?php endif; ?></th>
                                        <?php endforeach; endif; unset($_from); ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['links'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                        <th><?php echo $this->_tpl_vars['item']['0']; ?>
</th>
                                        <?php endforeach; endif; unset($_from); ?>
<!--                                        <th>Username</th>-->
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
<?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
                                    <tr <?php if ($this->_sections['cur']['iteration'] == 1): ?>class="first"<?php endif; ?>>
                                      <?php if ($this->_tpl_vars['sm']['special']): ?>
                                        <td><?php $this->assign('id', $this->_tpl_vars['sm']['special']['id']); ?>
<input type="checkbox" class="chkbox" value="<?php echo $this->_tpl_vars['x'][$this->_tpl_vars['id']]; ?>
"/></td>
                                      <?php endif; ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['field'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                        <td><?php if ($this->_tpl_vars['item']['anchor']): ?>
<a href="<?php echo $this->_tpl_vars['item']['anchor'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
"><?php echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
</a>
<?php elseif ($this->_tpl_vars['item']['condition']): ?>
<?php $this->assign('cond', $this->_tpl_vars['x'][$this->_tpl_vars['key']]); ?>
<?php echo $this->_tpl_vars['item']['condition'][$this->_tpl_vars['cond']]; ?>

<?php else: ?>
<?php if ($this->_tpl_vars['item']['format'] != ""): ?>
<?php ob_start(); ?>
{"<?php echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
"|<?php echo $this->_tpl_vars['item']['format']; ?>
}
<?php $this->_smarty_vars['capture']['k'] = ob_get_contents(); ob_end_clean(); ?>
<?php echo smarty_function_eval(array('var' => $this->_smarty_vars['capture']['k']), $this);?>

<?php elseif ($this->_tpl_vars['item']['image'] != ""): ?>
<img src="<?php echo $this->_tpl_vars['item']['image'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
" />
<?php else: ?>
<?php if ($this->_tpl_vars['key'] == 'lang_code'): ?>
<?php $this->assign('language', $this->_tpl_vars['lang_code'][$this->_tpl_vars['x']['lang_code']]); ?>
<?php echo $this->_tpl_vars['language']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>

<?php endif; ?>
<?php endif; ?>
<?php endif; ?></td>
                                        <?php endforeach; endif; unset($_from); ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['links'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                        <td><a <?php if ($this->_tpl_vars['item']['function']): ?>href="javascript:void(0);"onclick='<?php echo $this->_tpl_vars['item']['function']; ?>
("<?php echo $this->_tpl_vars['item']['1'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['item']['2']]; ?>
",<?php echo $this->_tpl_vars['qstart']; ?>
);'<?php else: ?>href="<?php echo $this->_tpl_vars['item']['1'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['item']['2']]; ?>
"<?php endif; ?>><?php if ($this->_tpl_vars['item']['3']): ?><button class="btn btn-success"><i class="icon-edit"</i></button><?php else:  echo $this->_tpl_vars['item']['0'];  endif; ?></a></td>
                                        <!--<td>@mdo</td>-->
                                        <?php endforeach; endif; unset($_from); ?>
                                    </tr>
                                    <?php endfor; endif; ?>
                                    
                                    </tbody>
                                </table>
                              <?php if ($this->_tpl_vars['total'] > $this->_tpl_vars['sm']['limit']): ?>
<?php if ($this->_tpl_vars['sm']['type'] == 'advance'): ?>
<div class="pagination_adv">
<?php echo $this->_tpl_vars['sm']['next_prev']->generateadv(); ?>

</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'box'): ?>
<div class="pagination_box">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'normal'): ?>
<div class="pagination">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'nextprev'): ?>
<div class="pagination">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->onlynextprev(); ?>
</div>
</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'extra'): ?>
<div class="pagination_box">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generateextra(); ?>
</div>
</div>
<?php else: ?>
<?php if ($this->_tpl_vars['sm']['type'] != 'no'): ?>
<div>
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
</div>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
  </div>
<!--done by gayatree ends-->










<div class="makebox center msglisting_admin">
<div class="headprt settheme">
    <div class="mdl">
        <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
        <div  class="fltrht">
        <?php if ($_SESSION['id_developer']): ?>
        <input type="button" value="Add New" onclick="addMsg();" class="buton" />
        <?php endif; ?>
        </div>
        <?php endif; ?>
        <span> <?php if ($this->_tpl_vars['sm']['pg_header']): ?>
        <?php echo $this->_tpl_vars['sm']['pg_header']; ?>
&nbsp;(<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
)
        <?php else: ?>
        <?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['next_prev']->module)) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
 Lists
        <?php endif; ?></span>
        <div class="clear"></div>
    </div>
</div>
    <?php if ($_SESSION['id_developer']): ?>
    <div class="hdng" align="center">Please does not remove Message Key starting with SETTING_</div><br />
    <?php endif; ?>
<div class="bodyprt">
<table cellspacing="0" class="tbl_listing">
<thead>
<tr>
<?php if ($this->_tpl_vars['sm']['special']): ?>
<th><?php echo $this->_tpl_vars['sm']['special']['prompt']; ?>
</th>
<?php endif; ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['field'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
<?php if ($this->_tpl_vars['key'] == $_SESSION[$this->_tpl_vars['m']]['sort_by']): ?>
<?php if ($_SESSION[$this->_tpl_vars['m']]['sort_order'] == 'ASC'): ?>
<?php $this->assign('cls', 'asc'); ?>
<?php else: ?>
<?php $this->assign('cls', 'desc'); ?>
<?php endif; ?>
<?php else: ?>
<?php $this->assign('cls', ""); ?>
<?php endif; ?>
<th class="<?php echo $this->_tpl_vars['cls']; ?>
">
<?php if ($this->_tpl_vars['item']['1'] != 0): ?>
<?php if ($this->_tpl_vars['sm']['ajax']): ?>
<a href="javascript:get_next_page('http://manoranjan.afixiindia.com/flexytiny_new/<?php echo $this->_tpl_vars['sm']['uri']; ?>
/sort_by/<?php echo $this->_tpl_vars['key']; ?>
',0,0,'<?php echo $_REQUEST['page']; ?>
_<?php echo $_REQUEST['choice']; ?>
')"><?php if ($this->_tpl_vars['item'] != ""):  echo $this->_tpl_vars['item']['0'];  else:  echo $this->_tpl_vars['key'];  endif; ?></a>
<?php else: ?>
<a href="http://manoranjan.afixiindia.com/flexytiny_new/<?php echo $this->_tpl_vars['sm']['uri']; ?>
/sort_by/<?php echo $this->_tpl_vars['key']; ?>
"><?php if ($this->_tpl_vars['item'] != ""):  echo $this->_tpl_vars['item']['0'];  else:  echo $this->_tpl_vars['key'];  endif; ?></a>
<?php endif; ?>
<?php else: ?>
<?php if ($this->_tpl_vars['item'] != ""):  echo $this->_tpl_vars['item']['0'];  else:  echo $this->_tpl_vars['key'];  endif; ?>
<?php endif; ?>
</th>
<?php endforeach; endif; unset($_from); ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['links'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
<th>
<?php echo $this->_tpl_vars['item']['0']; ?>

</th>
<?php endforeach; endif; unset($_from); ?>
</tr>
</thead>
<tbody>
<?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
<?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>						
<tr <?php if ($this->_sections['cur']['iteration'] == 1): ?>class="first"<?php endif; ?>>
<?php if ($this->_tpl_vars['sm']['special']): ?>
<td>
<?php $this->assign('id', $this->_tpl_vars['sm']['special']['id']); ?>
<input type="checkbox" class="chkbox" value="<?php echo $this->_tpl_vars['x'][$this->_tpl_vars['id']]; ?>
"/>
</td>
<?php endif; ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['field'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
<td>
<?php if ($this->_tpl_vars['item']['anchor']): ?>
<a href="<?php echo $this->_tpl_vars['item']['anchor'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
"><?php echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
</a>
<?php elseif ($this->_tpl_vars['item']['condition']): ?>
<?php $this->assign('cond', $this->_tpl_vars['x'][$this->_tpl_vars['key']]); ?>
<?php echo $this->_tpl_vars['item']['condition'][$this->_tpl_vars['cond']]; ?>

<?php else: ?>
<?php if ($this->_tpl_vars['item']['format'] != ""): ?>
<?php ob_start(); ?>
{"<?php echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
"|<?php echo $this->_tpl_vars['item']['format']; ?>
}
<?php $this->_smarty_vars['capture']['k'] = ob_get_contents(); ob_end_clean(); ?>
<?php echo smarty_function_eval(array('var' => $this->_smarty_vars['capture']['k']), $this);?>

<?php elseif ($this->_tpl_vars['item']['image'] != ""): ?>
<img src="<?php echo $this->_tpl_vars['item']['image'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>
" />
<?php else: ?>
<?php if ($this->_tpl_vars['key'] == 'lang_code'): ?>
<?php $this->assign('language', $this->_tpl_vars['lang_code'][$this->_tpl_vars['x']['lang_code']]); ?>
<?php echo $this->_tpl_vars['language']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['x'][$this->_tpl_vars['key']]; ?>

<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
</td>
<?php endforeach; endif; unset($_from); ?>
<?php if (count($_from = (array)$this->_tpl_vars['sm']['links'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
<td>
<a <?php if ($this->_tpl_vars['item']['function']): ?>href="javascript:void(0);"onclick='<?php echo $this->_tpl_vars['item']['function']; ?>
("<?php echo $this->_tpl_vars['item']['1'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['item']['2']]; ?>
",<?php echo $this->_tpl_vars['qstart']; ?>
);'<?php else: ?>href="<?php echo $this->_tpl_vars['item']['1'];  echo $this->_tpl_vars['x'][$this->_tpl_vars['item']['2']]; ?>
"<?php endif; ?>><?php if ($this->_tpl_vars['item']['3']): ?><img src="<?php echo $this->_tpl_vars['item']['3']; ?>
" title="<?php echo $this->_tpl_vars['item']['0']; ?>
"/><?php else:  echo $this->_tpl_vars['item']['0'];  endif; ?></a>									
</td>
<?php endforeach; endif; unset($_from); ?>
</tr>
<?php endfor; endif; ?>
</tbody>
</table>
<br />
<?php if ($this->_tpl_vars['total'] > $this->_tpl_vars['sm']['limit']): ?>
<?php if ($this->_tpl_vars['sm']['type'] == 'advance'): ?>
<div class="pagination_adv">
<?php echo $this->_tpl_vars['sm']['next_prev']->generateadv(); ?>

</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'box'): ?>
<div class="pagination_box">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'normal'): ?>
<div class="pagination">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'nextprev'): ?>
<div class="pagination">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->onlynextprev(); ?>
</div>
</div>
<?php elseif ($this->_tpl_vars['sm']['type'] == 'extra'): ?>
<div class="pagination_box">
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generateextra(); ?>
</div>
</div>
<?php else: ?>
<?php if ($this->_tpl_vars['sm']['type'] != 'no'): ?>
<div>
<div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
</div>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
</div>
</div>
<?php echo '
<script type="text/javascript">
function addMsg() {
    var url = "http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
    $.fancybox.showActivity();
    $.post(url,{"page":"setting","choice":"addMsg",ce : 0 },function(res){
    show_fancybox(res);
    css_even_odd();
     });
 }

function editMsg(id_msg) {
    var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
    $.fancybox.showActivity();
    $.post(url,{"page":"setting","choice":"editMsg","id_msg":id_msg,ce:0 },function(res) {
    show_fancybox(res);
    css_even_odd();
     });
 }

function deleteMsg(key_name) {
    var ser_msg_name=$(\'#ser_msg_name\').val()?$(\'#ser_msg_name\').val():\'\';
    var msg = '; ?>
"<?php echo @SETTING_CONFIRM_DEL_EN; ?>
"<?php echo ';
    if(confirm(msg)) {
        var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
        $.post(url,{"page":"setting","choice":"deleteMsg","key_name":key_name,"ser_msg_name":ser_msg_name,ce:0 },function(res) {
        var msg_del = '; ?>
"<?php echo @SETTING_DELETE_SUC_EN; ?>
"<?php echo ';
        alert(msg_del);
        $(\'#setting_msgList\').html(res);
         });
     }else {
        return false;
     }
 }
</script>
'; ?>


<!-- Template: admin/setting/msg_list.tpl.html End --> 