<?php /* Smarty version 2.6.7, created on 2017-08-19 10:30:44
         compiled from admin/dutil/database_show.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', 'admin/dutil/database_show.tpl.html', 26, false),)), $this); ?>
<div class="makebox wid60 center" id="dv1">
	<div class="headprt settheme">
		<div class="mdl">
			<div  class="fltrht"> <input type="button" class="buton" id="bt_check" name="check" value="UncheckAll" onclick="return check_all();" /></div>
			<span>Table List</span>
			<div class="clear"></div>
		</div>
	</div>
	<div class="bodyprt">
		<form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/dutil/dump" name="table_dump" method="post" onsubmit="return check_table_data();" >
			<table width="100%">
				<tr>
					<td>
												<table width="100%" border="0" class="formtbl">
							<thead>
								<tr>
									<th>Table Name</th>
									<th>Rows</th>
									<th>Space Used</th>
								</tr>
							</thead>
							<?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['arr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
							<tr>
								<td><input type="checkbox" name="table_db[]" value="<?php echo $this->_tpl_vars['sm']['arr'][$this->_sections['row']['index']]['Name']; ?>
" checked>&nbsp;&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['arr'][$this->_sections['row']['index']]['Name'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
</td>
								<td><?php echo $this->_tpl_vars['sm']['arr'][$this->_sections['row']['index']]['Rows']; ?>
</td>
								<td><?php echo $this->_tpl_vars['sm']['arr'][$this->_sections['row']['index']]['Space_used']; ?>
&nbsp;KiB</td>
							</tr>
							<?php endfor; endif; ?>
						</table>
					</td>
				</tr>
				<tr>
					<td><br>
						<div class="headprt settheme">
							<div class="mdl">
								<span>Structure and Data Format</span>
							</div>
						</div>
						<table class="formtbl" width="100%">
							<tr>
								<td colspan="2">
									<div id="div_sql">
										<label><input type="checkbox" name="sql_structure" value="structure" checked />Structure </label>&nbsp;&nbsp;
										<label><input type="checkbox" name="sql_data" value="data" checked />Data</label>
									</div>
									<div id="div_csv" style="display:none;">
										Fields terminated by&nbsp;:&nbsp;&nbsp;<b>;</b><br>
										Fields enclosed by&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<b>"</b><br>
										Fields escaped by&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<b>\</b> <br>
										Lines terminated by&nbsp;&nbsp;:&nbsp;&nbsp;<b>AUTO</b><br>
										Fields terminated by&nbsp;:&nbsp;&nbsp;<b>NULL</b><br>
										Put fields names in the first row&nbsp;&nbsp;<input type="checkbox" name="first_row" value="first_row" checked>
									</div>
									<div id="div_xml" style="display:none;">
										This format has no options.
									</div>
									<div id="div_xls" style="display:none;">
										This format has no options.
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></br>
						<div class="headprt settheme">
							<div class="mdl">
								<span>File Type</span>
							</div>
						</div>
						<table class="formtbl" width="100%">
							<tr>
								<td colspan="2">
									<input type="radio" name="file_type_export" value="sql" checked>.sql &nbsp;&nbsp;
									<input type="radio" name="file_type_export" value="csv">.csv &nbsp;&nbsp;
									<input type="radio" name="file_type_export" value="xml">.xml &nbsp;&nbsp;
									<input type="radio" name="file_type_export" value="xls">.xls
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><br>
						<div class="headprt settheme">
							<div class="mdl">
								<span>Save as file</span>
							</div>
						</div>
						<table class="formtbl" width="100%">
							<tr>
								<td colspan="2">
									<input type="radio" name="save" value="no" checked>None &nbsp;&nbsp;
									<span id="span_save">
										<input type="radio" name="save" value="zip">Zipped &nbsp;&nbsp;
										<input type="radio" name="save" value="tar">Tar &nbsp;&nbsp;
										<input type="radio" name="save" value="bzip">Bzipped &nbsp;&nbsp;
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						Download Filename:-<input type="text"  name="filename" value="">
					</td>
				</tr>
				<tr>
					<td>
						<div class="fltrht settheme">
							<input type="submit" class="buton" name="Dump" value="Dump" />
						</div>
						<div class="clear"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
</br></br>
<?php echo '
<script type="text/javascript">
	$(document).ready(function(){
		$("[name^=\'table_db\']").click(function(){
			//alert($(":checkbox:checked").length);
			if($("[name^=\'table_db\']:checked").length == $("[name^=\'table_db\']").length){
				$("#bt_check").val(\'UncheckAll\');
			 }else{
				$("#bt_check").val(\'CheckAll\');
			 }
		 });
		$("[name=\'file_type_export\']").click(function(){
			$(\'[id^="div"]\').hide();
			$("#span_save:hidden").show();
			var ext= $(this).val();
			if( ext == \'sql\'){
				$(\'#div_sql\').show();
			 }else if(ext == \'csv\'){
				$(\'#div_csv\').show();
			 }else if(ext == \'xls\'){
				$("#span_save").hide();
				$("[value=\'no\']").attr("checked",true);
				$(\'#div_xls\').show();
			 }else{
				$(\'#div_xml\').show();
			 }
		 });

	 });
	function check_all(){
		if($("#bt_check").val() == "UncheckAll"){
			$("[name^=\'table_db\']").attr("checked",false);
			$("#bt_check").val(\'CheckAll\');
		 }else{
			$("[name^=\'table_db\']").attr("checked",true);
			$("#bt_check").val(\'UncheckAll\');
		 }
	 }
	function check_table_data(){
		if($("[name^=\'table_db\']:checked").length >= 1){
			if($("[name=\'file_type_export\']:checked").val() == \'sql\'){
				if($("[name^=\'sql_\']:checked").length < 1){
					alert("PLease Check Structure and Data Format.");
					return false;
				 }
			 }
			return true;
		 }else{
			alert("Please check one table for Dump.");
			return false;
		 }
	 }
</script>
'; ?>