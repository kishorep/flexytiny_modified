<?php /* Smarty version 2.6.7, created on 2017-05-03 10:41:38
         compiled from admin/product/product_detail.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/product/product_detail.tpl.html', 34, false),)), $this); ?>

<!-- Template: admin/product/product_detail.tpl.html Start 03/05/2017 10:41:38 --> 
 <?php echo '
<style typde="text/css">
  .productdetailfancy_admin{display:none; }
  </style>
'; ?>

<!--done by gayatree starts-->
<div style="width:600px;">
  <div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget green">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>  Product Detail</h4>
<!--                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>-->
                            </div>
                            <div class="widget-body">
                                <table class="table table-striped">
<!--                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>-->
                                    <tbody>
                                   <tr>
        <td width="100">Name :</td>
        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
        </tr>
                                    <tr>
        <td>Description :</td>
        <td><div class="wid100"><?php echo $this->_tpl_vars['sm']['res']['description']; ?>
</div></td>
        </tr>
                                    <tr>
        <td>Code :</td>
        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['code'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
        </tr>
        <?php if ($this->_tpl_vars['sm']['res']['video']): ?>
        <tr>
        <td>Video:</td>
        <td>
            <div>
            <embed type="application/x-shockwave-flash" src="http://manoranjan.afixiindia.com/flexytiny_new/templates/video/player.swf" style="" id="ply" name="ply" bgcolor="#ffffff" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="file=http://manoranjan.afixiindia.com/flexytiny_new/video/<?php echo $this->_tpl_vars['sm']['res']['video']; ?>
&autostart=false" width="330" height="235"></embed>
            </div>			    
        </td>
        </tr>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['sm']['imgType'] == 2): ?>
         <tr valign="top">
        <td>Image :</td>
        <td style="vertical-align:top;">
            <?php if ($this->_tpl_vars['sm']['imgs']): ?>
            <div id="gallery" align="center">
                <ul style="margin-top:0px;margin-left:0px!important;">
                <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['imgs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
                <?php $this->assign('x', $this->_tpl_vars['sm']['imgs'][$this->_sections['cur']['index']]); ?>
                <?php if ($this->_sections['cur']['iteration'] > 1): ?>
                <?php endif; ?>
                <li style="width:100px;height:auto;">
										<?php if ($this->_tpl_vars['x']['image_name']): ?>
                    <a href="http://manoranjan.afixiindia.com/flexytiny_new/image/orig/product/<?php echo $this->_tpl_vars['x']['id_image']; ?>
_<?php echo $this->_tpl_vars['x']['image_name']; ?>
" >
                    <img src="http://manoranjan.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['x']['id_image']; ?>
_<?php echo $this->_tpl_vars['x']['image_name']; ?>
" />
                    </a>
										<?php else: ?>
										<img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
										<?php endif; ?>
                </li>
                <?php endfor; endif; ?>
                </ul>
            </div>
            <?php else: ?>
           <!--				    No image-->
						<img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
            <?php endif; ?>
        </td>
        </tr>
        <?php elseif ($this->_tpl_vars['sm']['imgType'] == 1): ?>
        <tr>
        <td>Image :</td>
        <td>
						<?php if ($this->_tpl_vars['sm']['res']['image']): ?>
										<a id="" href="http://manoranjan.afixiindia.com/flexytiny_new/image/orig/product/<?php echo $this->_tpl_vars['sm']['res']['id_product']; ?>
_<?php echo $this->_tpl_vars['sm']['res']['image']; ?>
" target="_blank" >
										<img src="http://manoranjan.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['sm']['res']['id_product']; ?>
_<?php echo $this->_tpl_vars['sm']['res']['image']; ?>
"  alt="no image" />
										</a>
							<?php else: ?>
						<!--				    No image-->
						<img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
						<?php endif; ?>
        </td>
        </tr>
        <?php endif; ?>	
        
        
        
        
        
        
        
        
        
        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
    </div>
  </div>
<!--done by gayatree ends-->









<div style="width:600px;" class="productdetailfancy_admin">
	<div class="makebox center" style="width:550px;">
<div class="headprt settheme">
    <div class="mdl">
    	<span> Product Detail</span>
    </div>
</div>
<div class="bodyprt">
    <table align="center" class="formtbl" border="0">
        <tr>
        <td width="100">Name :</td>
        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
        </tr>
        <tr>
        <td>Description :</td>
        <td><div class="wid100"><?php echo $this->_tpl_vars['sm']['res']['description']; ?>
</div></td>
        </tr>
        <tr>
        <td>Code :</td>
        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['code'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
        </tr>
        <?php if ($this->_tpl_vars['sm']['res']['video']): ?>
        <tr>
        <td>Video:</td>
        <td>
            <div>
            <embed type="application/x-shockwave-flash" src="http://manoranjan.afixiindia.com/flexytiny_new/templates/video/player.swf" style="" id="ply" name="ply" bgcolor="#ffffff" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="file=http://manoranjan.afixiindia.com/flexytiny_new/video/<?php echo $this->_tpl_vars['sm']['res']['video']; ?>
&autostart=false" width="330" height="235"></embed>
            </div>			    
        </td>
        </tr>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['sm']['imgType'] == 2): ?>
        <tr valign="top">
        <td>Image :</td>
        <td style="vertical-align:top;">
            <?php if ($this->_tpl_vars['sm']['imgs']): ?>
            <div id="gallery" align="center">
                <ul style="margin-top:0px;margin-left:0px!important;">
                <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['imgs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
                <?php $this->assign('x', $this->_tpl_vars['sm']['imgs'][$this->_sections['cur']['index']]); ?>
                <?php if ($this->_sections['cur']['iteration'] > 1): ?>
                <?php endif; ?>
                <li style="width:100px;height:auto;">
										<?php if ($this->_tpl_vars['x']['image_name']): ?>
                    <a href="http://manoranjan.afixiindia.com/flexytiny_new/image/orig/product/<?php echo $this->_tpl_vars['x']['id_image']; ?>
_<?php echo $this->_tpl_vars['x']['image_name']; ?>
" >
                    <img src="http://manoranjan.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['x']['id_image']; ?>
_<?php echo $this->_tpl_vars['x']['image_name']; ?>
" />
                    </a>
										<?php else: ?>
										<img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
										<?php endif; ?>
                </li>
                <?php endfor; endif; ?>
                </ul>
            </div>
            <?php else: ?>
           <!--				    No image-->
						<img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
            <?php endif; ?>
        </td>
        </tr>
        <?php elseif ($this->_tpl_vars['sm']['imgType'] == 1): ?>
        <tr>
        <td>Image :</td>
        <td>
						<?php if ($this->_tpl_vars['sm']['res']['image']): ?>
										<a id="" href="http://manoranjan.afixiindia.com/flexytiny_new/image/orig/product/<?php echo $this->_tpl_vars['sm']['res']['id_product']; ?>
_<?php echo $this->_tpl_vars['sm']['res']['image']; ?>
" target="_blank" >
										<img src="http://manoranjan.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['sm']['res']['id_product']; ?>
_<?php echo $this->_tpl_vars['sm']['res']['image']; ?>
"  alt="no image" />
										</a>
							<?php else: ?>
						<!--				    No image-->
						<img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
						<?php endif; ?>
        </td>
        </tr>
        <?php endif; ?>		    
    </table>
</div>
</div> 
</div>

<?php echo '
<script type="text/javascript">
$(function() {
$(\'#gallery a\').lightBox();
css_even_odd();
 });
</script>
<style>
#gallery ul li a{
padding: 0px;
margin: 0;
 }   
</style>
'; ?>


<!-- Template: admin/product/product_detail.tpl.html End --> 