<?php /* Smarty version 2.6.7, created on 2017-06-27 15:36:57
         compiled from admin/setting/msg_search.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'admin/setting/msg_search.tpl.html', 120, false),)), $this); ?>

<!-- Template: admin/setting/msg_search.tpl.html Start 27/06/2017 15:36:57 --> 
 <!--done by gayatree starts-->
<?php echo '
<style type="text/css">
  .msgsearch_admin{display:none; }
</style>
'; ?>

<div class="center">
<div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Search</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                            </div>
                            <div class="widget-body">
                              <form name="search" id="search" method="post" action="javascript:void(0);" class="form-horizontal">
                                 
                            <div class="control-group">
                                <label class="control-label">Message Key Or Value  </label>
                                <div class="controls">
                                 <input type="text" name="ser_msg_name" id="ser_msg_name"  value="" class="span6"/>
                                    <!--<input type="text" class="span6 " />-->
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
                            </div>
                                
                            <div class="form-actions">
                               
<button class="btn btn-success" type="button" name="search" value="Search" onclick="msgSearchList();">Search</button>
<button class="btn btn-success" type="reset" name="search" value="Reset" onclick="msgReset();">Reset</button>


                            </div>
                                  
                            </form>
                              
                              
                              
<!--                              <form name="search" id="search" class="basic" method="post" action="javascript:void(0);">
                                <table class="table table-striped tblbdr">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      
                                    <tr>
                                        <td class="algnrht pdtop">Message Key Or Value :</td>
                                        <td class="algnlft pdtop"><input type="text" name="ser_msg_name" id="ser_msg_name"  value="" class="txt"/></td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                         <td>
                                           <div class="form-actions" align="center">
<button class="btn btn-success" type="button" name="search" value="Search" onclick="msgSearchList();">Search</button>
<button class="btn" type="reset" name="search" value="Reset" onclick="msgReset();">Reset</button>

</div>
                                           </td>
                                    </tr>
                                    
                                    
                                    </tbody>
                                </table>
                                <div class="form-actions" align="center">
<button class="btn btn-success" type="button" name="search" value="Search" onclick="msgSearchList();">Search</button>
<button class="btn" type="reset" name="search" value="Reset" onclick="msgReset();">Reset</button>

</div>
                                </form>-->
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
             
                </div>
  </div>
<!--done by gayatree ends-->





<div class="wid30 fltlft mrglft5 msgsearch_admin">
    <div class="makebox center">
    	<div class="headprt settheme">
            <div class="mdl">
            	<span>Search</span>
            </div>
        </div>
        <div class="bodyprt">
		   <form name="search" id="search" method="post" action="javascript:void(0);">
				 <table border="0" width="92%" align="center" class="formtbl">
				 <tr>
					 <td style="vertical-align:middle;">Message Key Or Value :</td>
					 <td><input type="text" name="ser_msg_name" id="ser_msg_name"  value="" class="txt"/></td>
				 </tr>
				 <tr>
					 <td>&nbsp;</td>
					 <td>
                     	<table>
                        	<tr>
                            	<td><div class="settheme fltlft"><input type="button" name="btm" class="buton" value="Search" onclick="msgSearchList();"/></div></td>
                                <td><div class="settheme fltlft"><input type="button" name="btm" class="buton" value="Reset" onclick="msgReset();"/></div></td>
                            </tr>
                        </table>
					 </td>
				 </tr>
				 </table>
			 </form>
			 <input type="hidden" name="imagecnt" id="imagecnt" value="<?php echo count($this->_tpl_vars['sm']['imgs']); ?>
"/>
        </div>
    </div>
</div>
<div id="setting_msgList" class=""><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/setting/msg_list.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
<?php echo '
<script type="text/javascript">
    $(function(){
        $(\'#search\').bind(\'keypress\', function(e) {//"search" is the id of from field and pass the parameter to searchByEnter function like wise  
            searchByEnter(e,"msgSearchList");// searchList(1) is ypur function name and also you can pass 3rd argument as 1 example is in product search;
         });
     });
    function msgSearchList(){
	var url = "http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
	var ser_msg_name=$(\'#ser_msg_name\').val();
	if(ser_msg_name){
	    $.post(url,{"page":"setting","choice":"msgList","ser_msg_name":ser_msg_name,ce:0 },function(res){
		$(\'#setting_msgList\').html(res);
	     });
	 }else{
	    alert("Please enter message key or value");
	    return false;
	 }
     }

    function msgReset(){
	var url = "http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
	$(\'#ser_msg_name\').val(\'\')
	$.post(url,{"page":"setting","choice":"msgList","flag":1,ce:0 },function(res){
	    $(\'#setting_msgList\').html(res).slow(1000);
	 });
     }
</script>
'; ?>


<!-- Template: admin/setting/msg_search.tpl.html End --> 