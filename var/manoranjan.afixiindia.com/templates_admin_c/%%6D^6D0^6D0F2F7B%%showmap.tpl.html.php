<?php /* Smarty version 2.6.7, created on 2017-08-25 12:41:55
         compiled from admin/practice/showmap.tpl.html */ ?>

<!-- Template: admin/practice/showmap.tpl.html Start 25/08/2017 12:41:55 --> 
 <!DOCTYPE html>
<html>
  <head>
      <?php echo '
    <style>
      #map {
        width: 100%;
        height: 400px;
        background-color: grey;
       }
    </style>
    '; ?>

  </head>
  <body>
    <h3>My Google Maps Demo</h3>
    <div id="map"></div>
    <?php echo '
    <script>
      function initMap() {
        var uluru = {lat: 20.358760, lng: 85.816597 };
        var map = new google.maps.Map(document.getElementById(\'map\'), {
          zoom: 4,
          center: uluru
         });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
         });
       }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBB6VF87lPOOf8tJ_az8MqYMKQ0OMQmSdU&callback=initMap">
    </script>
    '; ?>

  </body>
</html>
<!-- Template: admin/practice/showmap.tpl.html End --> 