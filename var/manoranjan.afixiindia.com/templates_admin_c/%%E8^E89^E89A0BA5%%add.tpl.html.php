<?php /* Smarty version 2.6.7, created on 2017-05-29 14:54:24
         compiled from admin/redirect/add.tpl.html */ ?>

<!-- Template: admin/redirect/add.tpl.html Start 29/05/2017 14:54:24 --> 
 <?php echo '
<style type="text/css">
	 label.error{
		  font-size:12px;
		  display:block;
	  }
</style>
<script type="text/javascript">

    function validateExp() {
	var validator=$("#adminaddexperiences").validate({
            ignore: "input[type=\'text\']:hidden",
	    rules: {
		"exp[old_url]": {
		    required:true
		 },
		"exp[new_url]":{
		    required: true
		 }
	     },
	    messages: {
		"exp[old_url]":{
		    required:"<br>"+flexymsg.required
		 },
		"exp[new_url]":{
		    required:"<br>"+flexymsg.required
		 },
	     }
	 });
var x = validator.form();
if (x) {
$(\'#sbmt\').attr("disabled",true);
return x;
 } else {
$(\'#sbmt\').removeAttr("disabled");
return false;
 }
     }

function callbackFun(response) {
var msg = "';  echo $this->_tpl_vars['sm']['res'];  echo '"?"Experience updated Successfully":"Experience added successfully";
$(\'#redirect_listing\').html(response);
$.fancybox.close();
//$("#img_tbl tbody").sortable({
//update: function(event, tr) {
//var all_exam = $("#img_tbl tbody").sortable(\'toArray\', \'id\');//alert(all_class)
//$.post(siteurl, {"page": "attribute", "choice": "update_exp_seq", "all_list": all_exam, "tbl": "experiences", "seq_field": "exp_seq", "cond_field": "id_exp", "ce": 0 });
// },handle: \'.handler\'
// });
messageShow(msg);
 }

$(function(){
if(\'';  echo $this->_tpl_vars['sm']['res'];  echo '\'){
$("#adminaddexperiences input,#adminaddexperiences textarea").attr("disabled", true).attr(\'style\',\'border:0px !important;background:#FFFFFF;\');
$(".hid").attr(\'style\',"display:none");
 }
 });

function rmvstyles(){
 $("#adminaddexperiences input").attr("disabled", false).removeAttr(\'style\');
 $("#adminaddexperiences textarea").attr("disabled", false).removeAttr(\'style\');
 $(".hid").removeAttr(\'style\');
 $(".achr_img").css(\'display\' , \'none\');
 }
</script>
<style>
  textarea{min-height: 160px !important; }
</style>
'; ?>

<div id="dv2">
    <div style="width:500px;">
        <div class="headprt settheme">
            <div class="mdl">
                <span><?php if ($this->_tpl_vars['sm']['res']): ?>Edit<?php else: ?>Add<?php endif; ?> URL Data</span>
                <span class="fltrht"><a class="achr_img" href="javascript:void(0);"><?php if ($this->_tpl_vars['sm']['res']): ?><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/edit(1).png" alt="img" title="Edit" onclick="rmvstyles();"><?php endif; ?></a></span>
                <div class="clear"></div>
            </div>
        </div>
        <div class="bodyprt">
            <form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/redirect/insertredirecturl/ce/0/" name="adminaddexperiences" id="adminaddexperiences" enctype="multipart/form-data" method="post" onsubmit="return AsyncUpload.submitForm(this, validateExp, callbackFun);">
            <!--<form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/redirect/insertredirecturl/ce/0/" name="adminaddexperiences" id="adminaddexperiences" enctype="multipart/form-data" method="post" onsubmit="return validateExp();">-->
                <input type="hidden" name="qstart" value="<?php if ($this->_tpl_vars['sm']['qstart']):  echo $this->_tpl_vars['sm']['qstart'];  else: ?>0<?php endif; ?>" />   
                <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['sm']['res']['id']; ?>
" />   
                <table border="0" class="formtbl">
		    <tr>
			<td>Old URL <font color="red" class="hid">*</font>:</td>
			<td>
                            <input type="text" class="txt" name="exp[old_url]" value="<?php echo $this->_tpl_vars['sm']['res']['old_url']; ?>
"/>
			</td>
		    </tr>
		    <tr>
			<td>New URL <font color="red" class="hid">*</font>:</td>
			<td>
                            <input type="text" class="txt" name="exp[new_url]" value="<?php echo $this->_tpl_vars['sm']['res']['new_url']; ?>
"/>
			</td>
		    </tr>
            <tr>
                      <td></td>    
                           <td>
                           	<table>
                            	<tr>
                                	<td><div class="settheme fltlft"><input type="submit" class="buton hid" id="sbmt"   name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?>" /></div></td>
                                    <td><div class="settheme fltlft"><input type="button" class="buton hid"   value="Cancel" onclick="$.fancybox.close();" /></div></td>
                                </tr>
                            </table>
                           
                               </td>
                      </tr>
            </table>
            </form>
        </div>
    </div>
</div>



<!-- Template: admin/redirect/add.tpl.html End --> 