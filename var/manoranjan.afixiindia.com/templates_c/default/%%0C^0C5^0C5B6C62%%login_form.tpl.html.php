<?php /* Smarty version 2.6.7, created on 2017-09-25 11:46:57
         compiled from user/login_form.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'get_mod', 'user/login_form.tpl.html', 292, false),)), $this); ?>
<?php $this->_cache_serials['/var/www/html/flexytiny_new/flexymvc/../var/manoranjan.afixiindia.com/templates_c/default/%%0C^0C5^0C5B6C62%%login_form.tpl.html.inc'] = 'db20accad3a3868518a81acaa1e2b9ca'; ?>
<!-- Template: user/login_form.tpl.html Start 25/09/2017 11:46:57 --> 
 <?php $this->assign('check_captcha', $this->_tpl_vars['util']->get_values_from_config('CAPTCHA')); ?>
<?php $this->assign('login_type', $this->_tpl_vars['util']->get_values_from_config('LOGIN')); ?>
<?php $this->assign('facebook', $this->_tpl_vars['util']->get_values_from_config('FACEBOOK')); ?>
<?php $this->assign('twitter', $this->_tpl_vars['util']->get_values_from_config('TWITTER')); ?>
<?php $this->assign('linkedin', $this->_tpl_vars['util']->get_values_from_config('LINKEDIN')); ?>
<?php $this->assign('google', $this->_tpl_vars['util']->get_values_from_config('GOOGLE')); ?>
<?php echo '
<style type="text/css">
  .userlogin_admin{display:none; }
  .metro1 a:hover{color:#4A8BC2; }
  #logintable{margin:0 auto; }
</style>
<script type="text/javascript">
    function validateLogin(){
		validator=$("#loginform").validate({
			rules: {
				username: {
					required:true
				 },
				password:{
					required: true,
					minlength: 6
				 }
			 },
			messages: {
				username:{
					required:flexymsg.required
				
				 },
				password:{
					required:flexymsg.required,
					minlength:flexymsg.minlength
				 }
			 }
		 });
		x=validator.form();
		return x;
     }
    
    function setLogin(){
		if(validateLogin()){
			var loginInput = $("#loginform").serialize();
			var url = siteurl + "user/setLogin/ce/0/?"+loginInput;
			$.post(url,function(res){
				var err = res.split("::");
				if(err[0] == \'suc\'){
					window.location.href = err[1];
				 }else if(err[0] == \'err\'){
					messageShow(err[1]);
					//$("#err_login").html("<div style=\'color:#FF0000;margin-top:25px;\'>"+err[1]+"</div>").show();
					$("#uname").val(\'\');
					$("#pass").val(\'\');
					return false;
				 }else{
					$("#logintable").html(res);
				 }
			 });
		 }else{
			return false;
		 }
		return false;
     }
	$(document).ready(function(){
		var targethash = window.location.hash;
		if(targethash !=\'\'){
			if(targethash == "#members"){
				$("#guests").removeAttr(\'class\');
				$("#members").attr(\'class\',\'selected\');
				$(\'#logintable\').find(\'.tabsectorcontent\').eq(\'0\').removeClass(\'selected\');
				$(\'#logintable\').find(\'.tabsectorcontent\').eq(\'1\').addClass(\'selected\');
			 }
		 }
	 });
</script>

<!--  <script src="https://cdn.firebase.com/libs/firebaseui/2.3.0/firebaseui.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/2.3.0/firebaseui.css" />
<script src="https://www.gstatic.com/firebasejs/4.3.0/firebase.js"></script>
</script>
<script>
// Initialize Firebase
var config = {
apiKey: "AIzaSyBSV0ceuajbpj-XRvLiSLQZ7QRW_0QLjag",
authDomain: "flexytinynew-5340e.firebaseapp.com",
databaseURL: "https://flexytinynew-5340e.firebaseio.com",
projectId: "flexytinynew-5340e",
storageBucket: "flexytinynew-5340e.appspot.com",
messagingSenderId: "473152198904"
 };
firebase.initializeApp(config);


//set options
    var googleid = "1";
    var facebookid = "1";
    var twitterid = "1";
    var githubid = "1";
    var emailid = "0";
    var phoneid = "1";
    //var  googleopt;
    if(googleid==1){
     var  googleopt = firebase.auth.GoogleAuthProvider.PROVIDER_ID;
      }
    if(facebookid==1){
     var facebookopt= firebase.auth.FacebookAuthProvider.PROVIDER_ID;
      }
    if(twitterid==1){
     var  twitteropt= firebase.auth.TwitterAuthProvider.PROVIDER_ID;
      }
    if(githubid==1){
     var githubopt= firebase.auth.GithubAuthProvider.PROVIDER_ID;
      }
    if(emailid==1){
     var emailopt= firebase.auth.EmailAuthProvider.PROVIDER_ID;
      }
    if(phoneid==1){
      var phoneopt= firebase.auth.PhoneAuthProvider.PROVIDER_ID;
      }
    // FirebaseUI config.
    var uiConfig = {
        //        signInSuccessUrl: getuserdata,
        signInFlow: \'popup\',
        signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.
        googleopt,
        facebookopt,
        twitteropt,
        githubopt,
        emailopt,
        phoneopt
        ],
        // Terms of service url.
        tosUrl: \'<toservice-url>\',
        callbacks: {
        signInSuccess: function(currentUser, credential) {
//            alert(JSON.stringify(currentUser));
//            alert(JSON.stringify(credential));
            getuserdata(currentUser,credential);
          }
      }
      };

    // Initialize the FirebaseUI Widget using Firebase.
    var ui = new firebaseui.auth.AuthUI(firebase.auth());
    // The start method will wait until the DOM is loaded.
    ui.start(\'#firebaseui-auth-container\', uiConfig);
    
    function getuserdata(currentUser,credential){
        var providerIdphone = currentUser.providerData[0].providerId; 
        var userID ="";
        var providerId =""; 
        var displayName ="";
        var first_name ="";
        var last_name ="";
        var email ="";
        var photoURL="";
        if(currentUser.phoneNumber){
            providerId = providerIdphone;
            userID = currentUser.uid;
            email = currentUser.phoneNumber;
            first_name = "phone";
            last_name = "login";
          }else{
            providerId = credential.providerId;
            userID = currentUser.uid;
            displayName =currentUser.displayName;
            displayName = displayName.split(" ");
            first_name = displayName[0];
            last_name = displayName[1];
            email = currentUser.email;
            photoURL= currentUser.photoURL;
          }
        $.post(siteurl,{\'page\':\'socialNetwork\',\'choice\':\'firebaselogin\',\'userID\':userID,"first_name":first_name,"last_name":last_name,"email":email,\'providerId\':providerId,"ce":0  },function(res){ //alert(res);return false;
            window.location.href=siteurl+\'user/userHome\';					
            $.fancybox.hideActivity();
	  });
      }
</script>-->
'; ?>

<!--done by gayatree starts-->
<div id="logintable" class="center wid60" style="margin-top:62px;">
<div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i> Log In</h4>
<!--                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>-->
                            </div>
                          <div class="tabhd">
            <a id='guests' href="#guests">Guests</a>
            <a id='members' href="#members" class="selected">Members</a>
        </div>
                          <div class="clear" style="height: 2px;"></div>
                           <div class="tabsector" style="background:white" id="test">
                          <div class="tabsectorcontent">
                <div class="hdng">Welcome to Flexymvc Release</div>
                <div align="right">
                    <div class="fltlft">Feel free to register. Its free.</div>
                    <?php if ($this->_tpl_vars['login_type']['register'] == 1): ?>
                    <a href="javascript:void(0);" onclick="ajaxRegister();" class="userbuton">Create An Account</a>
                    <?php else: ?>
					<a href="http://manoranjan.afixiindia.com/flexytiny_new/register" class="userbuton">Create An Account</a>
                    <?php endif; ?>
                    <div class="clear"></div>
                </div>
            </div>
                            <div class="widget-body">
                              
                              <div class="tabsectorcontent selected">
                              <div class="hdng hdng_login">Registered Users</div>
                If you have an account with us, please log in.
                <?php if ($this->_tpl_vars['login_type']['type'] == 1): ?>
				<form id="loginform" name="loginform" action="javascript:void(0);" method="post" onsubmit="return setLogin()">
					<?php else: ?>
                    <form id="loginform" name="loginform" action="http://manoranjan.afixiindia.com/flexytiny_new/user/setLogin" method="post" onsubmit="return validateLogin()">
						<?php endif; ?>
						<input type="hidden"  value="1" name="userui" />
						<div align="center" id="err_login" style="display: none"></div>
                                <table class="table table-striped">
<!--                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>-->
                                    <tbody>
                                    <tr>
								<td width="120" class="algnrht">Username :</td>
								<td><input type="text" name="username" id="uname" class="fields secndtd" value="<?php echo $this->_tpl_vars['sm']['uname']; ?>
" /></td>
							</tr>
							<tr>
								<td class="algnrht">Password :</td>
								<td><input type="password" name="password" id="pass" class="fields secndtd" value="<?php echo $this->_tpl_vars['sm']['pwd']; ?>
" /></td>
							</tr>
							
							<tr>
								<td width="32%" class="algnrht">Facebook account:</td>
								<td align="right">
                                  <div class="metro metro1 double-size navy-blue ">
<a class="social-link" href="javascript:void(0);" onclick="callFBlogin();">
<i class="icon-facebook-sign"></i>
<span>Facebook Login</span>
</a>
</div>
									<!--<div><a href="javascript:void(0);" onclick="callFBlogin();"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/facebook_button.jpeg"/></a></div>-->
								</td>
							</tr>
							
							<?php if ($this->_tpl_vars['linkedin']['is_linkedin'] == 1): ?>
							<tr>
								<td>Linkedin Account:</td>
								<td align="right">
									<div><a href="javascript:void(0);" onclick="onLinkedinLogin();"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/button_linkedin.jpg"/></a></div>
								</td>
							</tr>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['twitter']['is_twitter'] == 1): ?>
							<tr>
								<td>Twitter Account:</td>
								<td align="right">
									<a href="http://manoranjan.afixiindia.com/flexytiny_new/socialNetwork/twitter"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/sign-in-with-twitter-l.png" width="151" height="24" border="0" /></a>		
								</td>
							</tr>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['google']['is_google'] == 1): ?>
							<tr>
								<td>Google Account:</td>
								<td align="right">
									<div><a href="<?php echo $this->_tpl_vars['sm']['gauth']; ?>
"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/google-login-button.png"/></a></div>
								</td>
							</tr>
							<?php endif; ?>
<!--                                                        <tr>
                                                            <td></td><td id="firebaseui-auth-container"></td>
                                                        </tr>-->
							<tr>
								<td><input type="checkbox" name="rem" id="rem" value="1"/>Remember Me</td>
								<td><a href="http://manoranjan.afixiindia.com/flexytiny_new/user/forgotPwd">Forgot Your Password?</a></td>
							</tr>
							<?php if ($this->_tpl_vars['check_captcha']['login']): ?>
							<?php if ($_SESSION['login_count']): ?>
							<tr>
								<td colspan="2">
									<div id="load_captcha" align="center"><?php if ($this->caching && !$this->_cache_including) { echo '{nocache:db20accad3a3868518a81acaa1e2b9ca#0}';}echo $this->_plugins['function']['get_mod'][0][0]->get_mod(array('mod' => 'user','mgr' => 'user','choice' => 'getCaptchaDb','type' => 'login'), $this);if ($this->caching && !$this->_cache_including) { echo '{/nocache:db20accad3a3868518a81acaa1e2b9ca#0}';}?>
</div>
									<?php if ($this->_tpl_vars['sm']['captcha_msg']): ?>
									<span id="cap_err_msg"><?php echo $this->_tpl_vars['sm']['captcha_msg']; ?>
</span>
									<?php endif; ?>
								</td>
							</tr>
							<?php endif; ?>
							<?php endif; ?>
<!--							<tr>
								<td colspan="2"></td>
							</tr>-->
							<tr>
								<td valign="middle"></td>
								<td valign="middle" class="tr"><input type="submit" value="Login" class="userbuton"/></td>
							</tr>
                                    </tbody>
                                </table>
                        </form>
                    </div>
                              </div>
                             </div>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
  </div>
  </div>
<div class="clear"></div>
<!--done by gayatree ends-->











<div id="logintable" class="center userlogin_admin">
    <div class="tabbg gradient">
        <div class="tabhd">
            <a id='guests' href="#guests">Guests</a>
            <a id='members' href="#members" class="selected">Members</a>
        </div>
        <div class="clear" style="height: 2px;"></div>
        <div class="tabsector" style="background:white" id="test">
            <div class="tabsectorcontent">
                <div class="hdng">Welcome to Flexymvc Release</div>
                <div align="right">
                    <div class="fltlft">Feel free to register. Its free.</div>
                    <?php if ($this->_tpl_vars['login_type']['register'] == 1): ?>
                    <a href="javascript:void(0);" onclick="ajaxRegister();" class="userbuton">Create An Account</a>
                    <?php else: ?>
					<a href="http://manoranjan.afixiindia.com/flexytiny_new/register" class="userbuton">Create An Account</a>
                    <?php endif; ?>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="tabsectorcontent selected">
                <div class="hdng">Registered Users</div>
                If you have an account with us, please log in.
                <?php if ($this->_tpl_vars['login_type']['type'] == 1): ?>
				<form id="loginform" name="loginform" action="javascript:void(0);" method="post" onsubmit="return setLogin()">
					<?php else: ?>
                    <form id="loginform" name="loginform" action="http://manoranjan.afixiindia.com/flexytiny_new/user/setLogin" method="post" onsubmit="return validateLogin()">
						<?php endif; ?>
						<input type="hidden"  value="1" name="userui" />
						<div align="center" id="err_login" style="display: none"></div>
						<table class="formtbl" width="100%">
							<tr>
								<td width="120">Username :</td>
								<td><input type="text" name="username" id="uname" class="fields" value="<?php echo $this->_tpl_vars['sm']['uname']; ?>
" /></td>
							</tr>
							<tr>
								<td>Password :</td>
								<td><input type="password" name="password" id="pass" class="fields" value="<?php echo $this->_tpl_vars['sm']['pwd']; ?>
" /></td>
							</tr>
							<?php if ($this->_tpl_vars['facebook']['fconnect'] == 1): ?>
							<tr>
								<td>Facebook account:</td>
								<td align="right">
									<div><a href="javascript:void(0);" onclick="callFBlogin();"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/facebook_button.jpeg"/></a></div>
								</td>
							</tr>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['linkedin']['is_linkedin'] == 1): ?>
							<tr>
								<td>Linkedin Account:</td>
								<td align="right">
									<div><a href="javascript:void(0);" onclick="onLinkedinLogin();"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/button_linkedin.jpg"/></a></div>
								</td>
							</tr>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['twitter']['is_twitter'] == 1): ?>
							<tr>
								<td>Twitter Account:</td>
								<td align="right">
									<a href="http://manoranjan.afixiindia.com/flexytiny_new/socialNetwork/twitter"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/sign-in-with-twitter-l.png" width="151" height="24" border="0" /></a>		
								</td>
							</tr>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['google']['is_google'] == 1): ?>
							<tr>
								<td>Google Account:</td>
								<td align="right">
									<div><a href="<?php echo $this->_tpl_vars['sm']['gauth']; ?>
"><img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/google-login-button.png"/></a></div>
								</td>
							</tr>
							<?php endif; ?>
							<tr>
								<td><input type="checkbox" name="rem" id="rem" value="1"/>Remember Me</td>
								<td><a href="http://manoranjan.afixiindia.com/flexytiny_new/user/forgotPwd">Forgot Your Password?</a></td>
							</tr>
							<?php if ($this->_tpl_vars['check_captcha']['login']): ?>
							<?php if ($_SESSION['login_count']): ?>
							<tr>
								<td colspan="2">
									<div id="load_captcha" align="center"><?php if ($this->caching && !$this->_cache_including) { echo '{nocache:db20accad3a3868518a81acaa1e2b9ca#1}';}echo $this->_plugins['function']['get_mod'][0][0]->get_mod(array('mod' => 'user','mgr' => 'user','choice' => 'getCaptchaDb','type' => 'login'), $this);if ($this->caching && !$this->_cache_including) { echo '{/nocache:db20accad3a3868518a81acaa1e2b9ca#1}';}?>
</div>
									<?php if ($this->_tpl_vars['sm']['captcha_msg']): ?>
									<span id="cap_err_msg"><?php echo $this->_tpl_vars['sm']['captcha_msg']; ?>
</span>
									<?php endif; ?>
								</td>
							</tr>
							<?php endif; ?>
							<?php endif; ?>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td valign="middle"></td>
								<td valign="middle" class="tr"><input type="submit" value="Login" class="userbuton"/></td>
							</tr>
						</table>
					</form>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<?php echo '
<script type="text/javascript">
	function callFBlogin(){
		FB.login(function(response) {
			if (response.status == \'connected\') {
				$.fancybox.showActivity();
				FB.api(\'/me\',  function(response) {
					var first_name = response.first_name;
					var last_name = response.last_name;
					var gender = response.gender;
					var email = response.email;
					var userID = response.id;
					window.location.href=siteurl+\'socialNetwork/fb_loginurl/userID/\'+userID+\'/first_name/\'+first_name+\'/last_name/\'+last_name+\'/gender/\'+gender+\'/email/\'+email;
				 });
             }
		 } , {perms:\'email\' }); 
	 }
	
	function onLinkedinLogin() {
		IN.ENV.js.scope = new Array();
		IN.ENV.js.scope[0] = \'r_emailaddress\';
		IN.User.authorize();
		if(IN.User.isAuthorized()){
			onLinkedInAuth();
		 }else{
			IN.Event.on(IN, "auth", onLinkedInAuth);
		 }
	 }
	
	function onLinkedInAuth() {
		$.fancybox.showActivity();
		IN.API.Profile("me").fields("id,firstName,lastName,emailAddress")
		.result( function(me) {
			var response = me.values[0];
			var first_name = response.firstName;
			var last_name = response.lastName;
			var email = response.emailAddress;
			var userID = response.id;
			
			$.post(siteurl,{\'page\':\'socialNetwork\',\'choice\':\'linkedin\',\'userID\':userID,"first_name":first_name,"last_name":last_name,"email":email,"ce":0 },function(res){
				//alert(res);
				window.location.href=siteurl+\'user/userHome\';					
				$.fancybox.hideActivity();
			 });
		 });
	 }
        
       
</script>
'; ?>

<!-- Template: user/login_form.tpl.html End --> 