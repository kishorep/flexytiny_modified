<?php /* Smarty version 2.6.7, created on 2017-06-27 15:37:05
         compiled from product/list.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'product/list.tpl.html', 17, false),array('modifier', 'escape', 'product/list.tpl.html', 18, false),array('modifier', 'truncate', 'product/list.tpl.html', 19, false),)), $this); ?>

<!-- Template: product/list.tpl.html Start 27/06/2017 15:37:05 --> 
 <div id="product_listing">
<div id="dv5" >
    <div class="wid70 center">
	<div class="makebox">
	    <h3 class="gradient">Product List</h3>
	    <table class="tbl_listing">
		<tr>
		    <th>Product Name</th>
		    <th>Description</th>
		    <th width="90">No. of Image</th>
		    <th>Image</th>
		</tr>
		<?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
	    	<?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
		<tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
">
		    <td><a href="javascript:void(0);" style="color: #1A28AC;" onclick="show_detail('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');"><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></td>
		    <td><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30, "...", true) : smarty_modifier_truncate($_tmp, 30, "...", true)); ?>
</td>
				<td align='center'><?php if ($this->_tpl_vars['x']['total'] != 0): ?><a href="javascript:void(0);" style="color: #1A28AC;" onclick="show_images('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');" title="Click to see all images"><?php echo $this->_tpl_vars['x']['total']; ?>
</a><?php else: ?>0<?php endif; ?></td>
		    <td><?php if ($this->_tpl_vars['x']['image_name']): ?><img src="http://manoranjan.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['x']['id_image']; ?>
_<?php echo $this->_tpl_vars['x']['image_name']; ?>
" style="max-width: 60px; max-height: 60px;" /><?php else: ?><img src='http://manoranjan.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg' style="max-width: 60px; max-height: 60px;" /><?php endif; ?></td>
		</tr>
		<?php endfor; else: ?>
		<tr>
		    <td colspan="4" align="center"><b>No Records Found</b></td>
		</tr>
		<?php endif; ?>
	    </table>
	    <div class="pagination_box" align="center">
		<?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>

	    </div>
	</div>
    </div>	
		</div>
<?php echo '
<script type="text/javascript" >
	var code=\'\';
function showfullimage(img){
	var mywindow=window.open("http://manoranjan.afixiindia.com/flexytiny_new/image/orig/product/"+img);
 }
function show_images(id){
	 $(\'#succ_msg\').html(\'\');
	 $(\'#target\').html(\'\');
	 var url="http://manoranjan.afixiindia.com/flexytiny_new/";//function writen in admin part
	 $.fancybox.showActivity();
		$.post(url,{"page":"product","choice":"showGallery",ce:0,\'id_product\':id },function(response){
			show_fancybox(response);
		 });
 }
function show_detail(id){
	 var url="http://manoranjan.afixiindia.com/flexytiny_new/";
	 $.fancybox.showActivity();
		$.post(url,{"page":"product","choice":"productDetail",ce:0,"id_product":id },function(response){
			show_fancybox(response);
		 });
	 }
</script>
'; ?>

</div>

<!-- Template: product/list.tpl.html End --> 