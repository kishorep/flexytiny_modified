<?php /* Smarty version 2.6.7, created on 2017-08-19 16:22:42
         compiled from user/forgot_pwd.tpl.html */ ?>

<!-- Template: user/forgot_pwd.tpl.html Start 19/08/2017 16:22:42 --> 
 <?php $this->assign('check_field', $this->_tpl_vars['util']->get_values_from_config('LOGIN')); ?>
<div class="wid50 center">
    <div class="makebox">
        <div class="hdng gradient">Forgot Password ?</div>
        <div class="message">Do not worry, please provide your username below, we will help you.</div><br />
        <form action="http://manoranjan.afixiindia.com/flexytiny_new/user/forgotPwd/s/1" method="post" name="forgotform" id="forgotform" onsubmit="return validate();">
            <table class="formtbl">
                <tr>
                    <td><label for="email"><?php if ($this->_tpl_vars['check_field']['name'] == 1): ?>Username<?php else: ?>Email Address<?php endif; ?> :</label></td>
                    <td><input type="text" name="email" id="email"/></td>
                </tr>	
                <tr>
                    <td></td>
                    <td><input type="submit" value="Submit" class="userbuton"/></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<?php echo '
<script type="text/javascript">
		var usernamecheck;
        var emailchk;
		$(function(){
		    var c="';  echo $this->_tpl_vars['check_field']['name'];  echo '";
		    if(c==1){
			usernamecheck=true;
            emailchk=false;
		     }else{
			usernamecheck=false;
            emailchk=true;
		     }
		 });
		$.validator.addMethod("username", function(value) {
			return /^[a-z0-9]([a-z0-9_.]+)?$/i.test(value);
			 },\'<br>Please enter a valid username\'
		);
function validate(){

	var validator=$("#forgotform").validate({
	   rules: {
	   		"email":{
				required:true,
				email:emailchk,
        username:usernamecheck
			 }
		 },
		messages: {
			"email":{
				required:"<br>"+flexymsg.required,
				email:"<br>"+flexymsg.email
			 }
		 }
	 });
	var x=validator.form();
	return x;
 }
</script>
'; ?>

<!-- Template: user/forgot_pwd.tpl.html End --> 