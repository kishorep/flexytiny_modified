<?php /* Smarty version 2.6.7, created on 2017-09-25 11:46:57
         compiled from common/header.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'ucFirst', 'common/header.tpl.html', 43, false),)), $this); ?>

<!-- Template: common/header.tpl.html Start 25/09/2017 11:46:57 --> 
 <?php echo '
<script>
  (function(i, s, o, g, r, a, m) {
        i[\'GoogleAnalyticsObject\'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
         }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
     })(window, document, \'script\', \'https://www.google-analytics.com/analytics.js\', \'ga\');

    ga(\'create\', \'UA-103810194-1\', \'auto\');
    ga(\'send\', \'pageview\');

</script>
'; ?>

<?php $this->assign('login_type', $this->_tpl_vars['util']->get_values_from_config('LOGIN')); ?>
<?php $this->assign('facebook', $this->_tpl_vars['util']->get_values_from_config('FACEBOOK')); ?>
<div id="header" class="navbar navbar-inverse navbar-fixed-top">
<div class="navbar-inner">
<div class="container-fluid">
  <div class="fltlft">
                <a href="http://manoranjan.afixiindia.com/flexytiny_new/" class="userlogotxt user_log">WELCOME </a>
            </div>
  <div class="fltrht" style="margin-top:15px; font-weight:bold;">
				<?php if (! $_SESSION['id_user']): ?>
                <?php if ($this->_tpl_vars['login_type']['type'] == 1): ?>
				<a href="javascript:void(0);" onclick="ajaxLogin();">Log in</a>&nbsp;|&nbsp;
                <?php else: ?>
				<a class="user_log" href="http://manoranjan.afixiindia.com/flexytiny_new/loginForm">Log in</a>&nbsp;|&nbsp;
                <?php endif; ?>
                <?php if ($this->_tpl_vars['login_type']['register'] == 1): ?>
				<a href="javascript:void(0);" onclick="ajaxRegister();">Register</a>
                <?php else: ?>
				<a class="user_log" href="http://manoranjan.afixiindia.com/flexytiny_new/register">Register</a>
                <?php endif; ?>
				<?php else: ?>
				<a style="color:#fff;">Welcome <?php echo ((is_array($_tmp=$_SESSION['username'])) ? $this->_run_mod_handler('ucFirst', true, $_tmp) : ucFirst($_tmp)); ?>
</a> |
				<a style="color:#fff;" href ="<?php if ($_SESSION['fconnect_flag'] || $_SESSION['linkedin_flag']): ?>javascript:void(0);<?php else: ?>http://manoranjan.afixiindia.com/flexytiny_new/user/logout<?php endif; ?>" <?php if ($_SESSION['fconnect_flag']): ?>onclick="logout_fb();"<?php else: ?>onclick="logout_linkedin();"<?php endif; ?>>Logout</a>
				<?php endif; ?>
            </div>
            <div class="clear"></div>
  
  </div>
  </div>
  </div>







<!--<div class="gradient">
	<div class="header">
		<div class="wid80 mrgtopbtm0 center">-->
<!--            <div class="fltlft">
                <a href="http://manoranjan.afixiindia.com/flexytiny_new/" class="userlogotxt">FLEXYMVC TINY </a>
            </div>-->
<!--            <div class="fltrht" style="margin-top:15px;">
				<?php if (! $_SESSION['id_user']): ?>
                <?php if ($this->_tpl_vars['login_type']['type'] == 1): ?>
				<a href="javascript:void(0);" onclick="ajaxLogin();">Log in</a>&nbsp;|&nbsp;
                <?php else: ?>
				<a href="http://manoranjan.afixiindia.com/flexytiny_new/loginForm">Log in</a>&nbsp;|&nbsp;
                <?php endif; ?>
                <?php if ($this->_tpl_vars['login_type']['register'] == 1): ?>
				<a href="javascript:void(0);" onclick="ajaxRegister();">Register</a>
                <?php else: ?>
				<a href="http://manoranjan.afixiindia.com/flexytiny_new/register">Register</a>
                <?php endif; ?>
				<?php else: ?>
				<a>Welcome <?php echo ((is_array($_tmp=$_SESSION['username'])) ? $this->_run_mod_handler('ucFirst', true, $_tmp) : ucFirst($_tmp)); ?>
</a> |
				<a href ="<?php if ($_SESSION['fconnect_flag'] || $_SESSION['linkedin_flag']): ?>javascript:void(0);<?php else: ?>http://manoranjan.afixiindia.com/flexytiny_new/user/logout<?php endif; ?>" <?php if ($_SESSION['fconnect_flag']): ?>onclick="logout_fb();"<?php else: ?>onclick="logout_linkedin();"<?php endif; ?>>Logout</a>
				<?php endif; ?>
            </div>
            <div class="clear"></div>-->
<!--        </div>
    </div>
</div>-->
<!-- Template: common/header.tpl.html End --> 