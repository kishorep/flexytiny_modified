<?php /* Smarty version 2.6.7, created on 2017-04-06 09:55:10
         compiled from user/register.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_radios', 'user/register.tpl.html', 410, false),array('function', 'html_select_date', 'user/register.tpl.html', 452, false),array('function', 'get_mod', 'user/register.tpl.html', 467, false),)), $this); ?>
<?php $this->_cache_serials['/var/www/vhosts/divyanshu.afixiindia.com/flexytiny_new/flexymvc/../var/divyanshu.afixiindia.com/templates_c/default//%%B5^B54^B54A40B8%%register.tpl.html.inc'] = '31f393480522f136bc4a8868219cfb06'; ?>
<!-- Template: user/register.tpl.html Start 06/04/2017 09:55:10 --> 
 <?php $this->assign('check_field', $this->_tpl_vars['util']->get_values_from_config('LOGIN')); ?>
<?php $this->assign('check_captcha', $this->_tpl_vars['util']->get_values_from_config('CAPTCHA')); ?>
<?php $this->assign('show_map', $this->_tpl_vars['util']->get_values_from_config('ADDR_GMAP')); ?>
<?php $this->assign('hobby', $this->_tpl_vars['util']->get_values_from_config('HOBBIES')); ?>
<?php $this->assign('type', $_SESSION['login_type']); ?>
<?php if ($this->_tpl_vars['show_map']['gmap_status'] == 'true'): ?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>
<script type="text/javascript" src="http://divyanshu.afixiindia.com/flexytiny_new/templates/flexyjs/flexymap.js"></script>
<?php endif; ?>
<?php echo '
<script type="text/javascript">
  var emailcheck;
  var usernamecheck;
  var mxlnth;
  $(function() {
	showdate();
	var c = "';  echo $this->_tpl_vars['check_field']['name'];  echo '";
	if (c == 1) {
	  usernamecheck = true;
	  mxlnth = 20;
	  emailcheck = false;
	 } else {
	  usernamecheck = false;
	  mxlnth = false;
	  emailcheck = true;
	 }
   });
  $.validator.addMethod("username", function(value) {
	return /^[a-z0-9]([a-z0-9_.]+)?$/i.test(value);
   }, \'<br>Please enter a valid username\'
		  );

  function showdate() {
	var x = document.getElementById(\'dtdropdown\').options.length;
	var mon = $(\'#mondropdown\').val();
	var yr = $(\'#yrdropdown\').val();
	var lpyr = checkleap(yr);
	if (mon == \'02\') {
	  if (!lpyr) {
		$("#dtdropdown option[value=\'29\']").remove();
	   } else {
		$("#dtdropdown option[value=29]").html() == null ? $("#dtdropdown").append(\'<option value="29">29</option>\') : \'\';
	   }
	  $("#dtdropdown option[value=\'30\']").remove();
	  $("#dtdropdown option[value=\'31\']").remove();
	 } else if (mon == \'01\' || mon == \'03\' || mon == \'05\' || mon == \'07\' || mon == \'08\' || mon == \'10\' || mon == \'12\') {
	  $("#dtdropdown option[value=29]").html() == null ? $("#dtdropdown").append(\'<option value="29">29</option>\') : \'\';
	  $("#dtdropdown option[value=30]").html() == null ? $("#dtdropdown").append(\'<option value="30">30</option>\') : \'\';
	  $("#dtdropdown option[value=31]").html() == null ? $("#dtdropdown").append(\'<option value="31">31</option>\') : \'\';
	 } else {
	  $("#dtdropdown option[value=29]").html() == null ? $("#dtdropdown").append(\'<option value="29">29</option>\') : \'\';
	  $("#dtdropdown option[value=30]").html() == null ? $("#dtdropdown").append(\'<option value="30">30</option>\') : \'\';
	  $("#dtdropdown option[value=\'31\']").remove();
	 }
   }

  function checkleap(yr) {
	if ((yr % 100 == 0 && yr % 400 == 0) || (yr % 100 != 0 && yr % 4 == 0))
	  return 1
	else
	  return 0;
   }

  function checkValidate() {
	$(\'#add_user_msg\').html(\'\');
	$(\'#add_email_msg\').html(\'\');
	var validator = $("#signupform").validate({
	  rules: {
		"user[first_name]": {
		  required: true,
		  maxlength: 50
		 },
		"user[last_name]": {
		  required: true,
		  maxlength: 50
		 },
		"user[username]": {
		  required: true,
//		  maxlength: mxlnth,
//		  username: usernamecheck,
		  email: emailcheck
		 },
		"user[email]": {
		  required: true,
		  email: true
		 },
		"user[password]": {
		  required: true,
		  minlength: 6,
		  maxlength: 20
		 },
		"cpwd": {
		  required: true,
		  equalTo: \'#pwd\'
		 },
		"gender": {
		  required: true
		 },
		"user[address]": {
		  required: true
		 },
		"dob_Month": {
		  required: true
		 },
		"dob_Year": {
		  required: true
		 },
		"dob_Day": {
		  required: true
		 },
		"hobbies[]": {
		  required: true
		 }
	   },
	  messages: {
		"user[first_name]": {
		  required:flexymsg.required,
		  maxlength: "<br>" + flexymsg.maxlength
		 },
		"user[last_name]": {
		  required:flexymsg.required,
		  maxlength: "<br>" + flexymsg.maxlength
		 },
		"user[username]": {
		  required:flexymsg.required,
		  maxlength: "<br>" + flexymsg.maxlength,
		  email: "<br>" + flexymsg.email
		 },
		"user[email]": {
		  required:flexymsg.required,
		  email: "<br>" + flexymsg.email
		 },
		"user[password]": {
		  required:flexymsg.required,
		  minlength: "<br>" + flexymsg.minlength,
		  maxlength: "<br>" + flexymsg.maxlength
		 },
		"cpwd": {
		  required:flexymsg.required,
		  equalTo: "<br>" + flexymsg.equalTo
		 },
		"gender": {
		  required:flexymsg.required
		 },
		"user[address]": {
		  required: flexymsg.required
		 },
		"dob_Month": {
		  required: "Month field required"
		 },
		"dob_Year": {
		  required: "Year field required"
		 },
		"dob_Day": {
		  required: "Date field required"
		 },
		"hobbies[]": {
		  required:flexymsg.required
		 }
	   }
	 });
//		var y = $("[type^=\'checkbox\']").is(":checked");
	var x = validator.form();
//		var y= showUsernameExist();
//		if(!y){
//			$("#err6").html("<font color=\'red\'><b>This field required<b></font>");
//			return false;
//		 }else{
//			$("#err6").html("");
//		 }
//alert(x+"==="+userexist+"==="+emailexist)
	if (x && userexist && emailexist) {
	  return x;
	 } else {
	  if (!userexist) {
		$(\'#add_user_msg\').html("<font color=red>This Username Already Exist.</font>");
	   }
	  if (!emailexist) {
		$(\'#add_email_msg\').html("<font color=red>This email id already is in use.</font>");
	   }
	  return false;
	 }
   }
  var userexist = true;
  var emailexist = true;
  function showUsernameExist() {
	var username = $(\'#username\').val();
	if (username != \'\') {
	  $.post(siteurl, {"page": "user", "choice": "checkUsername", ce: 0, "username": username }, function(res) {
		if (res == 1 && username != "") {
		  $(\'#add_user_msg\').html("<font color=red>This Username Already Exist.</font>");
		  userexist = false;
//					return false;
		 } else {
		  $(\'#add_user_msg\').html(\'\');
		  userexist = true;
//					return true;
		 }
	   });
	 } else {
	  $(\'#add_user_msg\').html(\'\');
	  $(\'#username\').focus();
	  userexist = false;
//			return false;
	 }
   }
  function showEmailExist() {
	var email = $(\'#email\').val();
	var id_user = \'';  echo $this->_tpl_vars['sm']['res']['id_user'];  echo '\';
	if (email != \'\') {
	  $.post(siteurl, {"page": "user", "choice": "checkEmail", \'ce\': \'0\', "email": email, \'id_user\': id_user }, function(res) {
		if (res == 1 && email != "") {
		  $(\'#add_email_msg\').html("<font color=red>This email id already is in use.</font>");
		  emailexist = false;
		 } else {
		  $(\'#add_email_msg\').html(\'\');
		  emailexist = true;
		 }
	   });
	 } else {
	  $(\'#add_email_msg\').html(\'\');
	  $(\'#email\').focus();
	  emailexist = false;
	 }
   }


  function checkPassword() {
	var text = $(\'#pwd\').val();
	var i, s, color, width;
	var n_o_small_char = 0;
	var n_o_cap_char = 0;
	var n_o_spe_char = 0;
	var n_o_dig = 0;
	var point = 0;
	for (i = 0; i < text.length; i++) {
	  if (97 <= text.charCodeAt(i) && text.charCodeAt(i) <= 122) {
		point++;
		n_o_small_char = n_o_small_char + 1;
	   } else if (65 <= text.charCodeAt(i) && text.charCodeAt(i) <= 90) {
		point = point + 2;
		n_o_cap_char++;
	   } else if (48 <= text.charCodeAt(i) && text.charCodeAt(i) <= 57) {
		point = point + 2;
		n_o_dig++;
	   } else if (text.charCodeAt(i) != 32) {
		n_o_spe_char++;
		point = point + 3;
	   }
	 }
	if (n_o_small_char > 0 && n_o_cap_char > 0 && n_o_spe_char > 0 && n_o_dig > 0) {
	  point = point + 4;
	 }
	if (n_o_spe_char > 2) {
	  point = point + 3;
	 }
	if (point < 10) {
	  if (!point) {
		s = "";
		width = 0;
	   } else {
		s = "poor";
		color = "#FFFFCC";
		width = 30;
	   }
	 } else if (10 <= point && point <= 15) {
	  s = "good";
	  color = "#CCFFFF";
	  width = 60;
	 } else if (point >= 15) {
	  s = "best";
	  width = 100;
	  color = "#00FF00";
	 }
	document.getElementById(\'status\').innerHTML = s;
	document.getElementById(\'subpassprogressbar\').style.backgroundColor = color;
	document.getElementById(\'subpassprogressbar\').style.width = width + \'%\';
   }

  function insertData() {
	if (checkValidate()) {
	  var loginInput = $("#signupform").serialize();
	  var url = siteurl + "user/insert/ce/0/?" + loginInput;
	  $.post(url, function(res) {
		var err = res.split("::");
		if (err[0] == \'suc\') {
		  window.location.href = err[1];
		 } else if (err[0] == \'fcon\') {
		  $.post(siteurl, {"page": "user", "choice": "fconnect", "ce": 0, "fid": err[1] }, function(fres) {
			show_fancybox(fres);
		   })
		 } else if (err[0] == \'err\') {
		  $("#error").html("<div style=\'color:#FF0000;margin-top:25px;\'>" + err[1] + "</div>").show();
		  return false;
		 } else {
		  $("#logintable").html(res);
		 }
	   });
	 } else {
	  return false;
	 }
	return false;
   }
</script>
<style type="text/css">
  h3.gradient a{color:white; text-decoration: underline }
  h3.gradient a:hover{text-decoration: none; }
  td.sml_select select{width:20% }
  .left label{float: left; margin-left:0px; }
  .counttext img{display:none;right:5px; top: 2px; }
  .counttext:hover img{display:block; }
  .gradient1{background:#4A8BC2 !important; font-size:15.5px !important; text-align: center; padding:0px !important; }
  .secndtd{width:95%; }
  .register_admin{display:none; }
  .reg .table tr td{border-top:none !important; }
  .reg .table tr td:nth-child(1){padding-top: 12px !important; }
</style>
'; ?>

<!--done by gayatree starts-->
<div class="wid70 center reg" style="margin:0 auto;">
<div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Registration Form</h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                                                      
                            <div class="widget-body">
                              <div id="error" align="center" style="display:none"></div>
<?php if ($this->_tpl_vars['check_field']['register'] == 1 && ! $this->_tpl_vars['sm']['flag']): ?>
<form id="signupform" name="signupform" action="javascript:void(0);" method="post" onSubmit="return insertData();" autocomplete="off">
  <?php else: ?>
  <form id="signupform" name="signupform" action="http://divyanshu.afixiindia.com/flexytiny_new/user/<?php if ($this->_tpl_vars['sm']['flag'] || $_SESSION['regd_type']): ?>updateProfile<?php else: ?>insert<?php endif; ?>" method="post" onSubmit="return checkValidate();">
	<input type="hidden" name="<?php echo $_SESSION['login_type']; ?>
" id="<?php echo $_SESSION['login_type']; ?>
" value="<?php echo $_SESSION[$this->_tpl_vars['type']]; ?>
" />
	<input type="hidden" name="id_user" id="id_user" value="<?php echo $this->_tpl_vars['sm']['res']['id_user']; ?>
" />
	<input type="hidden" name="social_regd_type" id="social_regd_type" value="<?php echo $_SESSION['social_regd_type']; ?>
" />
	<?php endif; ?>
    <h3 class="gradient gradient1"><?php if (! $this->_tpl_vars['sm']['res']['username']): ?>You can join here<?php else: ?>Edit Profile <a href="http://divyanshu.afixiindia.com/flexytiny_new/user/changePassword" class="fltrht mrgrht1">Change Password</a><div class="clear"></div><?php endif; ?></h3>
                                <table class="table table-striped">
<!--                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>-->
                                    <tbody>
                                    <tr>
			<td width="15%"><label for="fname">First Name  </label></td>
			<td><input type="text" name="user[first_name]" id="fname" value="<?php echo $this->_tpl_vars['sm']['res']['first_name']; ?>
" class="secndtd" />
			  <span id="err"><?php echo $this->_tpl_vars['sm']['err']['first_name']; ?>
</span>
			</td>
		  </tr>
		  <tr>
			<td><label for="lname">Last Name  </label></td>
			<td align="left"><input type="text" name="user[last_name]" id="lname" value="<?php echo $this->_tpl_vars['sm']['res']['last_name']; ?>
" class="secndtd" />
			  <span id="err1"><?php echo $this->_tpl_vars['sm']['err']['last_name']; ?>
</span>
			</td>
		  </tr>
		  <tr>
			<td><label for="username">Username </label></td>
			<td><input type="text" name="user[username]" class="secndtd" id="username" value="<?php echo $this->_tpl_vars['sm']['res']['username']; ?>
" <?php if ($this->_tpl_vars['sm']['flag']): ?>readonly="readonly"<?php endif; ?> <?php if (! $this->_tpl_vars['sm']['flag']): ?>onkeyup="return showUsernameExist();" onblur="showUsernameExist();"<?php endif; ?> />
					   <span id="add_user_msg"><b></b></span>
			  <span id="err2"><?php if ($this->_tpl_vars['sm']['d_name']):  echo $this->_tpl_vars['sm']['d_name'];  else:  echo $this->_tpl_vars['sm']['err']['username'];  endif; ?></span>
			</td>
		  </tr>
		  <tr>
			<td><label for="email">Email   </label></td>
			<td><input type="text" name="user[email]" id="email" value="<?php echo $this->_tpl_vars['sm']['res']['email']; ?>
" class="secndtd" <?php if ($_SESSION['regd_type']): ?>readonly="readonly"<?php endif; ?> onkeyup="return showEmailExist();"/>
					   <span id="add_email_msg"><label for="email" generated="true" class="error"></label></span>
			  <span id="err3"><?php echo $this->_tpl_vars['sm']['err']['email']; ?>
</span>
			</td>
		  </tr>
		  <?php if (! $this->_tpl_vars['sm']['flag']): ?>
		  <tr>
			<td><label for="pwd">Password  </label></td>
			<td><input type="password" name="user[password]" id="pwd" value="" onkeyup="checkPassword();" class="secndtd"/>
			  <span id="err4"><?php echo $this->_tpl_vars['sm']['err']['password']; ?>
</span>
			</td>
		  </tr>
		  <tr>
			<td colspan="3">
			  <div id="passprogressbar">
				<div id="subpassprogressbar">
				  <span id="status">&nbsp;</span>
				</div>
			  </div>
			</td>
		  </tr>
		  <tr>
			<td><label for="cpwd">Confirm Password  </label></td>
			<td><input type="password" name="cpwd" id="cpwd" value="<?php echo $this->_tpl_vars['sm']['conf_pwd']; ?>
" class="secndtd" />
			  <span id="err9"><?php echo $this->_tpl_vars['sm']['err']['conf_pwd']; ?>
</span>
			</td>
		  </tr>
		  <?php endif; ?>
		  <tr>
			<td><label>Gender  </label></td>
			<td class="left">
			  <?php echo smarty_function_html_radios(array('name' => 'gender','options' => $this->_tpl_vars['sm']['gender'],'selected' => $this->_tpl_vars['sm']['res']['gender']), $this);?>

			  <label for="gender" class="error" style="display:none;" class="secndtd">Please choose one option.</label>
			  <span id="err5"><?php echo $this->_tpl_vars['sm']['err']['gender']; ?>
</span>
			</td>
		  </tr>
		  <?php if ($this->_tpl_vars['hobby']['status'] == 'true'): ?>
		  <tr>
			<td><label>Hobbies  </label> </td>
			<td class="left">
			  <?php $this->assign('u_hob', $this->_tpl_vars['sm']['res']['hobbies']); ?>
			  <?php if (count($_from = (array)$this->_tpl_vars['sm']['hobbies'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['val']):
?>
			  <input class="secndtd" type="checkbox" name="hobbies[]" value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['u_hob'] & $this->_tpl_vars['key']): ?>checked="checked"<?php endif; ?>>&nbsp;&nbsp;<?php echo $this->_tpl_vars['val']; ?>
&nbsp;&nbsp;
					 <?php endforeach; endif; unset($_from); ?>
					 <label class="error" for="hobbies[]" generated="true" style="display:none;"></label>
			  <span id="err6"><?php echo $this->_tpl_vars['sm']['err']['hobbies']; ?>
</span>
			</td>
		  </tr>
		  <?php endif; ?>
		  <tr>
			<td valign="top"><label for="address">Address  </label><!-- <br />To add more address fields hover the textarea --></td>
			<td>
			  <div id="extratxt">
				<div class="posrel counttext1" id="address0">
				  <textarea name="user[address]" rows="3" style="width:93%" id="addr" <?php if ($this->_tpl_vars['show_map']['gmap_status']): ?>class="put_map_address1"<?php endif; ?> ><?php echo $this->_tpl_vars['sm']['res']['address']; ?>
</textarea>
				  <?php if ($this->_tpl_vars['show_map']['gmap_status'] == 'true'): ?>
				  <a href="javascript:void(0);" onclick="return display_map();"><img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/question_mark_icon.gif" alt="Address in Map" title="Address in Map"/></a>
				  <?php endif; ?>
				  <!--			<img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/add.png" onclick="addmore();" class="posabs cursor_pointer" />-->
				</div>
				<label class="error" for="addr" id="erraddr" generated="true"></label>
				<span class="err7"><?php echo $this->_tpl_vars['sm']['err']['address']; ?>
</span>
			  </div>
			</td>
		  </tr>
		  <tr>
			<td><label>Date of Birth  </label> </td>
			<td class="sml_select">
			  <?php if ($this->_tpl_vars['sm']['res']['dob']): ?>
			  <?php $this->assign('date', $this->_tpl_vars['sm']['res']['dob']); ?>
			  <?php else: ?>
			  <?php $this->assign('date', "--"); ?>
			  <?php endif; ?>
			  <?php echo smarty_function_html_select_date(array('prefix' => 'dob_','start_year' => "-100",'year_empty' => 'Year','month_empty' => 'Month','day_empty' => 'Date','reverse_years' => true,'day_extra' => "id='dtdropdown'",'month_extra' => "id='mondropdown' onchange='showdate();'",'year_extra' => "id='yrdropdown' onchange='showdate();'",'time' => $this->_tpl_vars['date']), $this);?>

			  <span id="err8"><?php echo $this->_tpl_vars['sm']['err']['dob']; ?>
</span>
			  <table width="70%">
				<tr>
				  <td width="30%"><label class="error" generated="true" for="mondropdown" style="display:none;margin-left:0px;"></label> </td>
				  <td width="30%"><label class="error" generated="true" for="dtdropdown" style="display:none; margin-left: -12px;"></label></td>
				  <td width="33%"><label class="error" generated="true" for="yrdropdown" style="display:none;margin-left: -22px;"></label></td>
				</tr>
			  </table>
			</td>
		  </tr>
		  <?php if ($this->_tpl_vars['check_captcha']['register'] && ! $this->_tpl_vars['sm']['flag']): ?>
		  <tr>
			<td>&nbsp;</td>
			<td>
			  <div id="load_captcha"><?php if ($this->caching && !$this->_cache_including) { echo '{nocache:31f393480522f136bc4a8868219cfb06#0}';}echo $this->_plugins['function']['get_mod'][0][0]->get_mod(array('mod' => 'user','mgr' => 'user','choice' => 'getCaptchaDb','type' => 'register'), $this);if ($this->caching && !$this->_cache_including) { echo '{/nocache:31f393480522f136bc4a8868219cfb06#0}';}?>
</div>
			  <?php if ($this->_tpl_vars['sm']['captcha_msg']): ?>
			  <span id="cap_err_msg" style="margin:0px auto;"> <font color="red"><?php echo $this->_tpl_vars['sm']['captcha_msg']; ?>
</font></span>
			  <?php endif; ?>
			</td>
		  </tr>
		  <?php endif; ?>
		  <tr>
			<td>&nbsp;</td>
			<td><input type="submit" value="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>Register<?php endif; ?>" class="btn btn-success" /></td>
		  </tr>
                                    </tbody>
                                </table>
    </form>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
  </div>
  </div>
<!--done by gayatree ends-->













<div id="error" align="center" style="display:none"></div>
<?php if ($this->_tpl_vars['check_field']['register'] == 1 && ! $this->_tpl_vars['sm']['flag']): ?>
<form id="signupform" name="signupform" action="javascript:void(0);" method="post" onSubmit="return insertData();" autocomplete="off">
  <?php else: ?>
  <form id="signupform" name="signupform" action="http://divyanshu.afixiindia.com/flexytiny_new/user/<?php if ($this->_tpl_vars['sm']['flag'] || $_SESSION['regd_type']): ?>updateProfile<?php else: ?>insert<?php endif; ?>" method="post" onSubmit="return checkValidate();">
	<input type="hidden" name="<?php echo $_SESSION['login_type']; ?>
" id="<?php echo $_SESSION['login_type']; ?>
" value="<?php echo $_SESSION[$this->_tpl_vars['type']]; ?>
" />
	<input type="hidden" name="id_user" id="id_user" value="<?php echo $this->_tpl_vars['sm']['res']['id_user']; ?>
" />
	<input type="hidden" name="social_regd_type" id="social_regd_type" value="<?php echo $_SESSION['social_regd_type']; ?>
" />
	<?php endif; ?>
	<div class="wid70 center register_admin">
	  <div class="makebox">
		<h3 class="gradient"><?php if (! $this->_tpl_vars['sm']['res']['username']): ?>You can join here<?php else: ?>Edit Profile <a href="http://divyanshu.afixiindia.com/flexytiny_new/user/changePassword" class="fltrht mrgrht1">Change Password</a><div class="clear"></div><?php endif; ?></h3>
		<table class="formtbl">
		  <tr>
			<td width="155"><label for="fname">First Name :</label></td>
			<td><input type="text" name="user[first_name]" id="fname" value="<?php echo $this->_tpl_vars['sm']['res']['first_name']; ?>
" />
			  <span id="err"><?php echo $this->_tpl_vars['sm']['err']['first_name']; ?>
</span>
			</td>
		  </tr>
		  <tr>
			<td><label for="lname">Last Name :</label></td>
			<td align="left"><input type="text" name="user[last_name]" id="lname" value="<?php echo $this->_tpl_vars['sm']['res']['last_name']; ?>
" />
			  <span id="err1"><?php echo $this->_tpl_vars['sm']['err']['last_name']; ?>
</span>
			</td>
		  </tr>
		  <tr>
			<td><label for="username">Username:</label></td>
			<td><input type="text" name="user[username]" id="username" value="<?php echo $this->_tpl_vars['sm']['res']['username']; ?>
" <?php if ($this->_tpl_vars['sm']['flag']): ?>readonly="readonly"<?php endif; ?> <?php if (! $this->_tpl_vars['sm']['flag']): ?>onkeyup="return showUsernameExist();" onblur="showUsernameExist();"<?php endif; ?> />
					   <span id="add_user_msg"><b></b></span>
			  <span id="err2"><?php if ($this->_tpl_vars['sm']['d_name']):  echo $this->_tpl_vars['sm']['d_name'];  else:  echo $this->_tpl_vars['sm']['err']['username'];  endif; ?></span>
			</td>
		  </tr>
		  <tr>
			<td><label for="email">Email : </label></td>
			<td><input type="text" name="user[email]" id="email" value="<?php echo $this->_tpl_vars['sm']['res']['email']; ?>
" <?php if ($_SESSION['regd_type']): ?>readonly="readonly"<?php endif; ?> onkeyup="return showEmailExist();"/>
					   <span id="add_email_msg"><label for="email" generated="true" class="error"></label></span>
			  <span id="err3"><?php echo $this->_tpl_vars['sm']['err']['email']; ?>
</span>
			</td>
		  </tr>
		  <?php if (! $this->_tpl_vars['sm']['flag']): ?>
		  <tr>
			<td><label for="pwd">Password :</label></td>
			<td><input type="password" name="user[password]" id="pwd" value="" onkeyup="checkPassword();"/>
			  <span id="err4"><?php echo $this->_tpl_vars['sm']['err']['password']; ?>
</span>
			</td>
		  </tr>
		  <tr>
			<td colspan="3">
			  <div id="passprogressbar">
				<div id="subpassprogressbar">
				  <span id="status">&nbsp;</span>
				</div>
			  </div>
			</td>
		  </tr>
		  <tr>
			<td><label for="cpwd">Confirm Password :</label></td>
			<td><input type="password" name="cpwd" id="cpwd" value="<?php echo $this->_tpl_vars['sm']['conf_pwd']; ?>
" />
			  <span id="err9"><?php echo $this->_tpl_vars['sm']['err']['conf_pwd']; ?>
</span>
			</td>
		  </tr>
		  <?php endif; ?>
		  <tr>
			<td><label>Gender :</label></td>
			<td class="left">
			  <?php echo smarty_function_html_radios(array('name' => 'gender','options' => $this->_tpl_vars['sm']['gender'],'selected' => $this->_tpl_vars['sm']['res']['gender']), $this);?>
<br />
			  <label for="gender" class="error" style="display:none;">Please choose one option.</label>
			  <span id="err5"><?php echo $this->_tpl_vars['sm']['err']['gender']; ?>
</span>
			</td>
		  </tr>
		  <?php if ($this->_tpl_vars['hobby']['status'] == 'true'): ?>
		  <tr>
			<td><label>Hobbies :</label> </td>
			<td class="left">
			  <?php $this->assign('u_hob', $this->_tpl_vars['sm']['res']['hobbies']); ?>
			  <?php if (count($_from = (array)$this->_tpl_vars['sm']['hobbies'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['val']):
?>
			  <input type="checkbox" name="hobbies[]" value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['u_hob'] & $this->_tpl_vars['key']): ?>checked="checked"<?php endif; ?>><?php echo $this->_tpl_vars['val']; ?>

					 <?php endforeach; endif; unset($_from); ?>
					 <label class="error" for="hobbies[]" generated="true" style="display:none;"></label>
			  <span id="err6"><?php echo $this->_tpl_vars['sm']['err']['hobbies']; ?>
</span>
			</td>
		  </tr>
		  <?php endif; ?>
		  <tr>
			<td valign="top"><label for="address">Address :</label><!-- <br />To add more address fields hover the textarea --></td>
			<td>
			  <div id="extratxt">
				<div class="posrel counttext1" id="address0">
				  <textarea name="user[address]" rows="3" style="width:93%" id="addr" <?php if ($this->_tpl_vars['show_map']['gmap_status']): ?>class="put_map_address1"<?php endif; ?> ><?php echo $this->_tpl_vars['sm']['res']['address']; ?>
</textarea>
				  <?php if ($this->_tpl_vars['show_map']['gmap_status'] == 'true'): ?>
				  <a href="javascript:void(0);" onclick="return display_map();"><img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/question_mark_icon.gif" alt="Address in Map" title="Address in Map"/></a>
				  <?php endif; ?>
				  <!--			<img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/add.png" onclick="addmore();" class="posabs cursor_pointer" />-->
				</div>
				<label class="error" for="addr" id="erraddr" generated="true"></label>
				<span class="err7"><?php echo $this->_tpl_vars['sm']['err']['address']; ?>
</span>
			  </div>
			</td>
		  </tr>
		  <tr>
			<td><label>Date of Birth :</label> </td>
			<td class="sml_select">
			  <?php if ($this->_tpl_vars['sm']['res']['dob']): ?>
			  <?php $this->assign('date', $this->_tpl_vars['sm']['res']['dob']); ?>
			  <?php else: ?>
			  <?php $this->assign('date', "--"); ?>
			  <?php endif; ?>
			  <?php echo smarty_function_html_select_date(array('prefix' => 'dob_','start_year' => "-100",'year_empty' => 'Year','month_empty' => 'Month','day_empty' => 'Date','reverse_years' => true,'day_extra' => "id='dtdropdown'",'month_extra' => "id='mondropdown' onchange='showdate();'",'year_extra' => "id='yrdropdown' onchange='showdate();'",'time' => $this->_tpl_vars['date']), $this);?>

			  <span id="err8"><?php echo $this->_tpl_vars['sm']['err']['dob']; ?>
</span>
			  <table width="70%">
				<tr>
				  <td width="30%"><label class="error" generated="true" for="mondropdown" style="display:none;margin-left:0px;"></label> </td>
				  <td width="30%"><label class="error" generated="true" for="dtdropdown" style="display:none; margin-left: -12px;"></label></td>
				  <td width="33%"><label class="error" generated="true" for="yrdropdown" style="display:none;margin-left: -22px;"></label></td>
				</tr>
			  </table>
			</td>
		  </tr>
		  <?php if ($this->_tpl_vars['check_captcha']['register'] && ! $this->_tpl_vars['sm']['flag']): ?>
		  <tr>
			<td>&nbsp;</td>
			<td>
			  <div id="load_captcha"><?php if ($this->caching && !$this->_cache_including) { echo '{nocache:31f393480522f136bc4a8868219cfb06#1}';}echo $this->_plugins['function']['get_mod'][0][0]->get_mod(array('mod' => 'user','mgr' => 'user','choice' => 'getCaptchaDb','type' => 'register'), $this);if ($this->caching && !$this->_cache_including) { echo '{/nocache:31f393480522f136bc4a8868219cfb06#1}';}?>
</div>
			  <?php if ($this->_tpl_vars['sm']['captcha_msg']): ?>
			  <span id="cap_err_msg" style="margin:0px auto;"> <font color="red"><?php echo $this->_tpl_vars['sm']['captcha_msg']; ?>
</font></span>
			  <?php endif; ?>
			</td>
		  </tr>
		  <?php endif; ?>
		  <tr>
			<td>&nbsp;</td>
			<td><input type="submit" value="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>Register<?php endif; ?>" class="userbuton" /></td>
		  </tr>
		</table>
	  </div>
	</div>
  </form>

  <?php echo '
  <style type="text/css">
	.left label {
	  float: none ;
	 }
  </style>
  <script type="text/javascript">
	var apnd = \'\';
	function addmore() {
	  var id = $(\'.counttext\').length;
	  apnd = \'<div class="posrel counttext" id="address\' + id + \'"><textarea name="user[address][]" rows="3" style="width:93%" ></textarea><img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/delete.png" onclick="delthis(\' + id + \')" class="posabs cursor_pointer" /></div>\';
	  $(\'#extratxt\').append(apnd);
	 }
	function delthis(id) {
	  $(\'#address\' + id).remove();
	 }
  </script>
  '; ?>





<!-- Template: user/register.tpl.html End --> 