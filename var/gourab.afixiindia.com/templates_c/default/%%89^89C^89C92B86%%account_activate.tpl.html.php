<?php /* Smarty version 2.6.7, created on 2016-11-08 17:03:21
         compiled from user/account_activate.tpl.html */ ?>

<!-- Template: user/account_activate.tpl.html Start 08/11/2016 17:03:21 --> 
 <b>Dear <?php echo $this->_tpl_vars['sm']['first_name']; ?>
,</b><br /><br />
Thank You For registering in <b>http://gourab.afixiindia.com/flexytiny_new/</b><br /><br />

<?php if (! $this->_tpl_vars['sm']['social_flag']): ?>
<a href="<?php echo $this->_tpl_vars['sm']['activate_link']; ?>
">Click Here</a> To activate your account. <br /><br />

<i>If the above link does not work, you can copy and paste the link below into your Web browser (please do not add extra spaces).</i>
<br /><br />
<a href="<?php echo $this->_tpl_vars['sm']['activate_link']; ?>
"><?php echo $this->_tpl_vars['sm']['activate_link']; ?>
</a><br /><br />
<?php else: ?>

You can login with your <?php echo $this->_tpl_vars['sm']['social_flag']; ?>
 credentials. If you want to Login with our site credentials, Here is your Login Details:<br /><br />

username: <?php echo $this->_tpl_vars['sm']['username']; ?>
<br />
password: <?php echo $this->_tpl_vars['sm']['password']; ?>

<?php endif; ?>
<!-- Template: user/account_activate.tpl.html End --> 