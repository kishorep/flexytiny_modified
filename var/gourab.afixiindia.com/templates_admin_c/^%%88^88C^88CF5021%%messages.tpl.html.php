<?php /* Smarty version 2.6.7, created on 2017-04-04 13:01:59
         compiled from common/messages.tpl.html */ ?>

<!-- Template: common/messages.tpl.html Start 04/04/2017 13:01:59 --> 
 <!-- messages set by all modules shown here -->

<!-- Done by manab-->
<?php $this->assign('type_display', $this->_tpl_vars['util']->get_values_from_config('MESSAGE_DISPLAY')); ?>

	<?php if ($this->_tpl_vars['type_display']['type'] == 1): ?>     
	<script type="text/javascript" src="http://gourab.afixiindia.com/flexytiny_new/templates/flexyjs/jquery.ui.notification.js"></script>
	<link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/jquery.ui.notification.css"/>
	<?php elseif ($this->_tpl_vars['type_display']['type'] == 2): ?>
	 <script type="text/javascript" src="http://gourab.afixiindia.com/flexytiny_new/templates/flexyjs/jquery.notice.js"></script>
	 <link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/jquery.notice.css">
	<?php elseif ($this->_tpl_vars['type_display']['type'] == 3): ?>
	 <script type="text/javascript" src="http://gourab.afixiindia.com/flexytiny_new/templates/flexyjs/sNotify.js"></script>
	 <link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/sNotify.css"/>
	<?php endif; ?>
<!--end-->

    <div class="alert" id="raise" align="center"></div>
<?php if ($this->_tpl_vars['sm']['message']): ?>
<?php unset($this->_sections['cur_msg']);
$this->_sections['cur_msg']['name'] = 'cur_msg';
$this->_sections['cur_msg']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['message']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur_msg']['show'] = true;
$this->_sections['cur_msg']['max'] = $this->_sections['cur_msg']['loop'];
$this->_sections['cur_msg']['step'] = 1;
$this->_sections['cur_msg']['start'] = $this->_sections['cur_msg']['step'] > 0 ? 0 : $this->_sections['cur_msg']['loop']-1;
if ($this->_sections['cur_msg']['show']) {
    $this->_sections['cur_msg']['total'] = $this->_sections['cur_msg']['loop'];
    if ($this->_sections['cur_msg']['total'] == 0)
        $this->_sections['cur_msg']['show'] = false;
} else
    $this->_sections['cur_msg']['total'] = 0;
if ($this->_sections['cur_msg']['show']):

            for ($this->_sections['cur_msg']['index'] = $this->_sections['cur_msg']['start'], $this->_sections['cur_msg']['iteration'] = 1;
                 $this->_sections['cur_msg']['iteration'] <= $this->_sections['cur_msg']['total'];
                 $this->_sections['cur_msg']['index'] += $this->_sections['cur_msg']['step'], $this->_sections['cur_msg']['iteration']++):
$this->_sections['cur_msg']['rownum'] = $this->_sections['cur_msg']['iteration'];
$this->_sections['cur_msg']['index_prev'] = $this->_sections['cur_msg']['index'] - $this->_sections['cur_msg']['step'];
$this->_sections['cur_msg']['index_next'] = $this->_sections['cur_msg']['index'] + $this->_sections['cur_msg']['step'];
$this->_sections['cur_msg']['first']      = ($this->_sections['cur_msg']['iteration'] == 1);
$this->_sections['cur_msg']['last']       = ($this->_sections['cur_msg']['iteration'] == $this->_sections['cur_msg']['total']);
?>
     <div class="alert" align="center"><?php echo $this->_tpl_vars['sm']['message'][$this->_sections['cur_msg']['index']]; ?>
</div>
<?php endfor; else: ?>
     <div class="alert" align="center"><?php echo $this->_tpl_vars['sm']['message']; ?>
</div>
<?php endif; ?>
<?php endif; ?>

<!---  Done by manab --->
<?php echo '
<script type="text/javascript">
$(document).ready(function(){
 //ui-notification-container
 var msg_display="';  echo $_SESSION['raise_message'][$this->_tpl_vars['module']];  echo '";
  messageShow(msg_display);
 });
function messageShow(msg_display){
   if(msg_display){
	var msgDisplay=\'<font color="red">\'+msg_display+\'</font>\';
   if(!$("div").is("#raise")){
	var prepend=1;
	var css=\'\';
	'; ?>

	  <?php $this->assign('type_display', $this->_tpl_vars['util']->get_values_from_config('MESSAGE_DISPLAY')); ?>
	  <?php if ($this->_tpl_vars['type_display']['type'] == 1): ?>
	   css='<link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexymvc_core/libsext/jqueryui/1.8.16/jquery-ui-1.8.16.custom.css"/><link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/jquery.ui.notification.css"/>';
	  <?php elseif ($this->_tpl_vars['type_display']['type'] == 2): ?>
	   css='<link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/jquery.notice.css">';
	  <?php elseif ($this->_tpl_vars['type_display']['type'] == 3): ?> 
	   css='<link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/sNotify.css"/>';
	<?php endif; ?>
	<?php echo '
	$(\'head\').append(css);
	$(\'<div class="alert" id="raise"  align="center"></div>\').appendTo(\'body\');
    }
   $(\'#raise\').show();	 
   if(\'';  echo $this->_tpl_vars['type_display']['type'];  echo '\' == 1){
	$(\'#raise\').attr(\'class\',\'ui-notification-container\');
	 $(".ui-notification-container").notification();
		   // bind creation of notification to button
			   $(".ui-notification-container").notification("create", {
				   title: msgDisplay,
				   content: " "
			    },{
				show: {
					effect: \'bounce\',
					options: { },
					speed: 250,
					callback: function(){ }
				 },
				hide: {
					effect: \'clip\',
					options: { },
					speed: 250,
					callback: function(){ }
				 }
			    });
	  return false;
    }else if(\'';  echo $this->_tpl_vars['type_display']['type'];  echo '\' == 2){
	jQuery.noticeAdd({
	   text: msgDisplay,
	   stay: false
	 });
    }else if(\'';  echo $this->_tpl_vars['type_display']['type'];  echo '\' == 3){
	 sNotify.addToQueue(msgDisplay);
    }else{
		$(\'#raise\').html(msgDisplay).css({\'position\':\'fixed\',\'top\':\'120px\',\'left\':\'450px\',\'zIndex\':\'10055\',\'background-color\': \'white\',\'padding\': \'10px 10px 10px 10px\',\'border\': \'1px solid #000\' }).show(\'slow\');
		setTimeout("hide_message(\'raise\')",5000);
	 //$(\'#raise\').html(msg_display);
    }
   if(prepend == 1){
	 $(\'<link rel="stylesheet" type="text/css" href="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/sNotify.css"/>\').remove(\'head\');
	 $(css).remove(\'body\');
    }
  }
 }
function hide_message(id){
	$(\'#\'+id).hide(\'slow\');
 }
</script>
'; ?>

<!-- END -->
<!-- Template: common/messages.tpl.html End --> 