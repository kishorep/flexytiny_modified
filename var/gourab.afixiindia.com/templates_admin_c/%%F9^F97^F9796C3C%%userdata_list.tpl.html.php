<?php /* Smarty version 2.6.7, created on 2017-02-21 02:52:13
         compiled from admin/csvuser/userdata_list.tpl.html */ ?>

<!-- Template: admin/csvuser/userdata_list.tpl.html Start 21/02/2017 02:52:13 --> 
 <?php echo '
<script>
    function add(id,qstart,name){
	$.post(siteurl,{"page" : "csvuser", "choice" : "adduser","id":id,"qstart":qstart,"name":name,"ce" : 0 },function(res){
        show_fancybox(res);
     });
     }
        function downlode(id){
//            var id = ';  echo $this->_tpl_vars['sm']['id_voters'];  echo ';
            window.location.href = siteurl+"csvuser/downlodefile/id/';  echo $this->_tpl_vars['sm']['id_csvtbl'];  echo '";
         }

function deletuser(qstart,name){
var searchval = $("#search").serialize();
var id=$(\'input:checkbox:checked.chkbox\').map(function(){
return this.value;  }).get().join(",");
var conf=confirm(\'Are you sure to delete ?\');
if(conf && id){
$.post(siteurl,{"page" : "csvuser", "choice" : "deleteusers", "ce" : 0, "id" : id, "qstart" : qstart,"voterslist":name, "searchval":searchval },function(res){//alert(res);return false;
messageShow("Voters deleted successfully.");
$("#csvuser_userdata_list").html(res);
 });   
 }else{
return false;
 }
 }

$(document).ready(function() {

    $(\'#checkAll\').click(function () {
        $(\'input:checkbox\').not(this).prop(\'checked\', this.checked);
     });

    $("[id*=chkbox]").change(function () {
        if ($(\'input[id*=chkbox][type=checkbox]:checked\').length == $(\'input[id*=chkbox][type=checkbox]\').length) {
            $(\'#checkAll\').prop(\'checked\', true);
         } else {
            $(\'#checkAll\').prop(\'checked\', false);
         }
     });

 });
</script>
'; ?>

<div class="row-fluid">
<div class="span12">

<!-- BEGIN EXAMPLE TABLE widget-->
<div class="widget red">
    <div class="widget-title">

        <h4><i class="icon-reorder"></i>User Listing (<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
)</h4>
            <span class="tools">
                <a href="javascript:;" class="icon-chevron-down"></a>
                <a href="javascript:;" class="icon-remove"></a>
            </span>
        <div  class="fltrht pdngin block_ip">
  <a href="javascript:void(0);" onclick="add('<?php echo $this->_tpl_vars['sm']['namedata']['id']; ?>
','<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
','<?php echo $this->_tpl_vars['sm']['namedata']['name']; ?>
');" class="btn btn-success">Add</a>
  <a href="javascript:void(0);" onclick="deletuser('<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
','<?php echo $this->_tpl_vars['sm']['namedata']['name']; ?>
');" class="btn btn-success">Delete</a>
  <a href="javascript:void(0);" onclick="downlode('<?php echo $this->_tpl_vars['sm']['id_voters']; ?>
');" class="btn btn-success">Download List</a>
 </div>
    </div>
    <div class="widget-body">
     <form>
       <?php if ($this->_tpl_vars['sm']['list']): ?>
        <table cellspacing="0" class="table table-striped table-bordered" id="attr_tbl">
                   <thead>
                        <thead>
                    <tr>
                         <th><input type="checkbox" class="checkAll" id="checkAll">Check All</th>
			<?php if (count($_from = (array)$this->_tpl_vars['sm']['clmnm'])):
    foreach ($_from as $this->_tpl_vars['kyh'] => $this->_tpl_vars['ith']):
?>
			 <th><?php echo $this->_tpl_vars['ith']; ?>
</th>
			<?php endforeach; endif; unset($_from); ?>
                    </tr>
                </thead>
                <tbody>
                      <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
		      <?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
                    <tr>
                          <td><input type="checkbox" class="chkbox" value="<?php echo $this->_tpl_vars['x']['id_csvtbl']; ?>
" id="chkbox<?php echo $this->_tpl_vars['x']['id_csvtbl']; ?>
"></td>
			<?php if (count($_from = (array)$this->_tpl_vars['sm']['clmnm'])):
    foreach ($_from as $this->_tpl_vars['kyv'] => $this->_tpl_vars['itv']):
?>
                        <?php if ($this->_tpl_vars['itv'] == 'Password'): ?>
                        <td><?php echo $this->_tpl_vars['x']['login_pass']; ?>
</td>
                        <?php elseif ($this->_tpl_vars['itv'] == 'Username'): ?>
			<td><?php echo $this->_tpl_vars['x']['login_data']; ?>
</td>
                        <?php else: ?>
			<td><?php echo $this->_tpl_vars['x'][$this->_tpl_vars['itv']]; ?>
</td>
                        <?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
                    </tr>
                    <?php endfor; endif; ?>
                    </tbody>
        </table>
       <?php else: ?>
       <div>No records found....</div>
       <?php endif; ?>
       </form>
    </div>
 <div class="pagination_box">
<div align="center"><?php if (! $this->_tpl_vars['sm']['id_cat']):  echo $this->_tpl_vars['sm']['next_prev']->generate();  endif; ?></div>
</div>
</div>
</div>
<!-- Template: admin/csvuser/userdata_list.tpl.html End --> 