<div id="chklist">
    <?php
    define("DB_HOST", '192.168.1.53'); // MySql host
    define("DB_USER", 'parwesh'); // MySql User name
    define("DB_PASS", 'p455w0rd'); // MySql password
    define("DB_DB", 'FLEXYTEST'); // MySql Database Name
    define("TABLE_PREFIX", 'flexyrelease__');
    define("LIBSEXT_PATH", 'http://' . $_SERVER['HTTP_HOST'] . "/flexymvc_core/libsext/");
    define("JS_PATH_EXT", LIBSEXT_PATH . "jquery/");
    define("SUB_DIR", "FLEXYMVC/");
    define("IP", $_SERVER['REMOTE_ADDR']);
    
    $_SESSION['site_used'] = !empty($_SESSION['site_used']) ? $_SESSION['site_used'] : $_SERVER["HTTP_HOST"];
    $_SESSION['site_used'] = preg_replace("/www./", "", $_SESSION['site_used']);

    if (!defined("SITE_USED") || $_SESSION['site_used'] != SITE_USED) {
	    define("SITE_USED", $_SESSION['site_used']);
    }

    db_connect_mysql5();
    global $conf;
    $conf = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . "/FLEXYMVC/flexymvc/configs/".SITE_USED."/config.ini.php", true);

    function db_connect_mysql5() {
	global $link;
	$link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DB);
	/* check connection */
	if (mysqli_connect_errno()) {
	    //echo "Connect failed: ".mysqli_connect_error();
	    exit();
	}
	//echo "Host information:".$link->host_info."<br/>";
    }

    $choice = isset($_REQUEST['choice']) ? $_REQUEST['choice'] : "listing";
    if ($choice)
	$choice();

    function listing() {
	//$conf=parse_ini_file($_SERVER['DOCUMENT_ROOT']."/FLEXYMVC/flexymvc/configs/prabhu.afixiindia.com/config.ini.php",true);
	global $link;
	global $conf;
	$sql = "SELECT * FROM " . TABLE_PREFIX . "checkList ORDER BY category";
	$res = mysqli_query($link, $sql);
	$str = "<div><input type='button' name='Addnew' value='Addnew' onClick='addNewCL()'><input type='button' name='unchk' value='Uncheck' onClick='uncheckCL()'></div><table cellpadding='5px' cellspacing='5px'><tr><th>Action</th><th>Category</th><th>Check Value</th></tr>";
	$i = 1;
	$cond = "";
	while ($rec = mysqli_fetch_assoc($res)) {
	    if ($rec['status'] == 1) {
		$cond = "checked=checked";
	    } else {
		$cond = "";
	    }
	    $str = $str . "<tr><td><input type='checkbox' name='ckh$i' id='ckh$i' value=" . $rec['id_checklist'] . " $cond ></td>
			 <td>" . $conf['CHECK_LIST'][$rec['category']] . "</td><td>" . $rec['checkvalue'] . "</td></td></tr>";
	    $i++;
	}
	$str = $str . "<tr><td colspan='3' align='center'><input type='button' name='save'
		  value='Save' onclick='save()'></td></tr></table>";
	print $str;
    }

    function insertChk() {
	global $link;
	$ids = trim($_REQUEST['ids'], ",");
	if ($ids == "") {
	    $sql = "UPDATE " . TABLE_PREFIX . "checkList SET status = '0'";
	    $res = mysqli_query($link, $sql);
	} else {
	    $sql = "UPDATE " . TABLE_PREFIX . "checkList SET status = '1' WHERE id_checklist IN($ids)";
	    $res = mysqli_query($link, $sql);

	    $sql1 = "UPDATE " . TABLE_PREFIX . "checkList SET status = '0' WHERE id_checklist  NOT IN($ids)";
	    $res = mysqli_query($link, $sql1);
	}
	header("Location:checklist.php");
    }

    function insertChkList() {
	global $link;
	$checklist = $_REQUEST['checklist'];
	$checklist['status'] = '1';
	$sql = "INSERT INTO " . TABLE_PREFIX . "checkList (category,checkvalue,status,add_date,ip) VALUES ('" . $checklist['category'] . "','" . $checklist['checkvalue'] . "','1', 'now()', '" . IP . "')";
	$res = mysqli_query($link, $sql);
	//listing();
	header("Location:checklist.php");
    }

    function addNewCL() {
	global $conf;
	$res = $conf['CHECK_LIST'];
	$str = '<form action="checklist.php?choice=insertChkList" name="checklist1" id="checklist1" enctype="multipart/form-data" method="post" onSubmit="return validateCheckList();">
			<table width=""  align="center">
			    <tr><td align="right">Category :</td><td><select name="checklist[category]"><option value="">Select</option>';
	foreach ($res as $k => $v) {
	    $str.='<option label="' . $k . '" value="' . $k . '">' . $v . '</option>';
	}
	$str.='</td></tr>
			    <tr><td align="right">Checklist Data :</td><td><textarea name="checklist[checkvalue]" value="" style="width:400px;height:50px;"></textarea></td></tr>
			    <tr><td>&nbsp;</td><td><input type="submit" class="login_btn" name="submit" value="Add" /></td></tr>
				</table>
				</form>';
	print_r($str);
    }

    function uncheckCL() {
	global $link;
	$sql = "UPDATE " . TABLE_PREFIX . "checkList SET status=0 WHERE 1";
	$res = mysqli_query($link, $sql);
	header("Location:checklist.php");
    }
    ?>
    <script src="/flexymvc_core/libsext/jquery/1.3.2/jquery.js"> </script>
    <script type="text/javascript" src="/flexymvc_core/libsext/jquery/js/jquery.validate.js"></script>
    <script type="text/javascript" src="/flexymvc_core/libsext/jquery/fancybox/jquery.fancybox-1.3.2.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="/FLEXYMVC/templates/css_theme/fancybox/jquery.fancybox-1.3.2.css"/>
    <script type="text/javascript" language="javascript">
	var ids="";
	function save(){
	    var n = $("input:checked").each(function(){
		ids += $(this).val()+",";
	    });
	    $.post('checklist.php?choice=insertchk',{'ids':ids},function (res){
		window.location.href='checklist.php?choice=listing';
	    });
	}
	function addNewCL(){
	    $.fancybox.showActivity();
	    $.post('checklist.php?choice=addNewCL',{},function (res){
		$.fancybox(res,{
		    centerOnScroll:true,
		    hideOnOverlayClick:false,
		    'onClosed'		: function() {
			window.location.href='checklist.php?choice=listing';
		    }
		});
	    });
	}
	function validateCheckList() {
	    var validator=$("#checklist1").validate({
		rules: {
		    "checklist[category]":{
			required: true
		    },
		    "checklist[checkvalue]":{
			required: true
		    }
		}
	    });
	    var x=validator.form();
	    return x;
	}
	function uncheckCL(){
	    window.location.href='checklist.php?choice=uncheckCL';
	}
    </script>
</div>
