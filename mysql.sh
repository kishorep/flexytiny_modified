#!/bin/bash
  # sh scriptname 1-prefix 2-username 3-password 4-database  5-filename where you want store data 6- host 7-moving to which database

  port=3306
DATE=`date '+%d-%m-%Y %H:%M:%S'`
  if test -n "$6"; then
  if test -n "$7"; then
  files_list=`echo "show tables like '$1%__';" | mysql    -u $2 -P $7 -h$6 -p$3 $4 | grep -v 'Tables_in_' |xargs `
  mysqldump  -u $2 -P $7 -h$6 -p$3 $4 $files_list >$5$DATE
  else
  files_list=`echo "show tables like '$1%__';" | mysql    -u $2 -P $port -h$6 -p$3 $4 | grep -v 'Tables_in_' |xargs `
mysqldump  -u $2 -P $port -h$6 -p$3 $4 $files_list >$5$DATE
fi
  else
  host="localhost"
  files_list=`echo "show tables like '$1__%';" | mysql    -u $2 -p$3 $4  -h $host | grep -v 'Tables_in_' |xargs `
  fi
  if test -n "$files_list"; then
  echo $files_list
  else
  echo "No data found"
  fi
